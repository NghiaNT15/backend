<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeveloperItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developer_items', function (Blueprint $table) {
            $table->id();
            $table->string('title',191)->nullable();
            $table->longText('contentDescription')->nullable();
            $table->mediumText('imageShare')->nullable();
            $table->integer('priorites')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developer_items');
    }
}
