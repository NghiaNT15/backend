<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();

            $table->string('title',191)->nullable();
			$table->longText('content')->nullable();
			$table->string('slug')->unique();
            $table->integer('priorites')->default(1);
			$table->boolean('isActive')->default(1);
            $table->boolean('isSpecial')->default(0);
			$table->string('keyWord')->nullable();
			$table->mediumText('linkShare')->nullable();
			$table->mediumText('shortDescription')->nullable();
            $table->mediumText('backgroundImage')->nullable();
            $table->string('titleSeo',191)->nullable();
            $table->string('imageShare',191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
