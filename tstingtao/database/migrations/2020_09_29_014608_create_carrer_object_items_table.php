<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarrerObjectItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrer_object_items', function (Blueprint $table) {
            $table->id();
            $table->string('positionName', 250)->nullable();
            $table->integer('step')->nullable();
            $table->string('locationName', 250)->nullable();
            $table->integer('priority')->nullable();

            $table->longText('content')->nullable();
            $table->string('imageShare', 250)->nullable();
            $table->string('slug', 191)->nullable();
            $table->mediumText('shortDescription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrer_object_items');
    }
}
