<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfomationCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infomation_companies', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191)->nullable();

            $table->string('address', 250)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('hotline', 10)->nullable();
            $table->string('facebookLink', 251)->nullable();
            $table->string('twitterLink', 251)->nullable();
            $table->string('instagramLink', 251)->nullable();
            $table->string('email', 20)->nullable();
            $table->mediumText('IntroductionContent')->nullable();
            $table->mediumText('MIssionContent')->nullable();
            $table->mediumText('TypeofBussiness')->nullable();
            $table->mediumText('aboutContent')->nullable();
            $table->mediumText('techologyContent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infomation_companies');
    }
}
