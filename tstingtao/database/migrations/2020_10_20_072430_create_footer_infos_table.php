<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFooterInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footer_infos', function (Blueprint $table) {
            $table->id();

            $table->string('shortName')->default("TSINGTAO VIETNAM")->nullable();
            $table->string('phone1')->default("(+84) 908 109 789")->nullable();
            $table->string('email')->default("+852 2851 3029")->nullable();
            $table->string('footerTextFind')->default("Find Tsingtao on Social Media")->nullable();
            $table->string('facebooklink')->nullable();
            $table->string('youtubeLink')->nullable();
            $table->string('instagramlink')->nullable();
            $table->string('footerText')->default("Tsingtao © Copyright 2020. All Rights Reserved. Powered by Maskcodex")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footer_infos');
    }
}
