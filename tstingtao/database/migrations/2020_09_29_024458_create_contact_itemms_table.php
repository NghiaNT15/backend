<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactItemmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_itemms', function (Blueprint $table) {
            $table->id();
            $table->string('title', 250)->nullable();
            $table->string('address', 250)->nullable();
            $table->string('phoneNumber', 20)->nullable();
            $table->string('contactAddress', 100)->nullable();
            $table->mediumText('contactAddressMap')->nullable();
            $table->integer('priority')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_itemms');
    }
}
