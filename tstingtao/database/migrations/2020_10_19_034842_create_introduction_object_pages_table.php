<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntroductionObjectPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('introduction_object_pages', function (Blueprint $table) {
            $table->id();
            $table->string('titleHOmePage', 191)->nullable();
            $table->mediumText('descriptionContentHomePage')->nullable();
            $table->mediumText('imageHomePage')->nullable();

            $table->mediumText('bannerImage')->nullable();

            $table->string('titleIntroduction', 191)->nullable();

            $table->mediumText('introductionContent')->nullable();
            $table->string('tsingTaoVietNamTitle', 191)->nullable();

            $table->mediumText('tsingTaoVietNamContent')->nullable();

            $table->string('tsingTaoVietNamMisionTitle', 191)->nullable();

            $table->mediumText('tsingTaoVietNamMisionContent')->nullable();

            $table->string('titlePage', 191)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('introduction_object_pages');
    }
}
