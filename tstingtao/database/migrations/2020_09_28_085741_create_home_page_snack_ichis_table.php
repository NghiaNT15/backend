<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomePageSnackIchisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_page_snack_ichis', function (Blueprint $table) {
            $table->id();
            $table->string('image1', 250)->nullable();
            $table->string('image2', 250)->nullable();
            $table->string('image', 250)->nullable();
            $table->string('title', 50)->nullable();
            $table->string('shortDescription', 191)->nullable();
             $table->string('slug', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_page_snack_ichis');
    }
}
