<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_infos', function (Blueprint $table) {
            $table->id();
            $table->mediumText('Address')->default("Lầu 5, Tòa nhà An Phát, Số 1074, Đường Võ Văn Kiệt, Phường 6, Quận 5, Thành phố Hồ Chí Minh, Việt Nam")->nullable();
            $table->string('phone1')->default("(+84) 28 2210 4346")->nullable();
            $table->string('phone2')->default("(+84) 908 109 789")->nullable();
            $table->string('phone3')->default("+852 2851 3029")->nullable();
            $table->string('email')->default("tai.nguyen@tsingtaovietnam.com")->nullable();
            $table->string('website')->default("www.tsingtaovietnam.com")->nullable();
            $table->string('fullName')->default("CÔNG TY TNHH BIA TSINGTAO VIỆT NAM")->nullable();

            $table->longText('googlemap')->default("")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_infos');
    }
}
