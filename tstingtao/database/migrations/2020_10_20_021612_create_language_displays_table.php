<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguageDisplaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_displays', function (Blueprint $table) {
            $table->id();
            $table->string('Homepage_productsingtaoTile',191)->default("CÁC SẢN PHẨM CỦA TSINGTAO")->nullable();
            $table->string('Homepage_videoSpecial',191)->default("VIDEO NỔI BẬT")->nullable();
            $table->string('Homepage_newEvent',191)->default("TIN TỨC & SỰ KIỆN")->nullable();

            $table->string('btnSeeMore',191)->default("Xem thêm")->nullable();
            $table->string('btnSeeDetail',191)->default("xem chi tiết")->nullable();


            $table->string('carrer_PositionName',191)->default("VỊ TRÍ TUYỂN DỤNG")->nullable();
            $table->string('carrer_Quatinity',191)->default("Số lượng")->nullable();
            $table->string('carrer_WorkLocation',191)->default("Nơi làm việc")->nullable();
            $table->string('carrer_ApplicationDealine',191)->default("Hạn nộp")->nullable();


            $table->string('new_lasted',191)->default("Tin mới nhất")->nullable();
            $table->string('new_Special',191)->default("Tin nổi bật")->nullable();
            $table->string('new_backtoNew',191)->default("Trở về tin tức")->nullable();
            $table->string('new_other',191)->default("Tin tức khác")->nullable();

            $table->string('contactinfo',191)->default("Gửi liên hệ")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_displays');
    }
}
