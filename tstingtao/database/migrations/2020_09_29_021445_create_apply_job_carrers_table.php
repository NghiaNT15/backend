<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplyJobCarrersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apply_job_carrers', function (Blueprint $table) {
            $table->id();
            $table->string('fullName', 250)->nullable();
            $table->string('ref',20)->nullable();
            $table->boolean('isread')->nullable();
            $table->string('phoneNumber', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('linkFile', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apply_job_carrers');
    }
}
