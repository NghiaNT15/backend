<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerHomePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_home_pages', function (Blueprint $table) {
            $table->id();
            $table->string('image1', 250)->nullable();
            $table->string('image2', 250)->nullable();
            $table->string('image3', 250)->nullable();
            $table->string('image4', 250)->nullable();
            $table->string('title', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_home_pages');
    }
}
