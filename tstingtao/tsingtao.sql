-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2020 at 10:29 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tsingtao`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus_pages`
--

CREATE TABLE `aboutus_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `QuatinityText` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footerText` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aboutus_pages`
--

INSERT INTO `aboutus_pages` (`id`, `title`, `content`, `QuatinityText`, `footerText`, `created_at`, `updated_at`) VALUES
(1, 'VỀ CHÚNG TÔI', '<p>B&aacute;nh gạo Nhật ICHI l&agrave; sản phẩm độc quyền thuộc C&ocirc;ng Ty Cổ Phần Thi&ecirc;n H&agrave; Kameda. Thi&ecirc;n H&agrave; Kameda được th&agrave;nh lập ng&agrave;y 9/1/2013 bởi sự hợp t&aacute;c, li&ecirc;n doanh giữa doanh nghiệp Việt Nam l&agrave; C&ocirc;ng Ty Cổ Phần Thi&ecirc;n H&agrave; Corp v&agrave; C&ocirc;ng Ty Nhật Bản Kameda Seika Co., LTD (thương hiệu b&aacute;nh gạo c&oacute; doanh thu số 1 Nhật Bản).</p>\n<p>Với số lượng CBCNV 300 người c&ugrave;ng đội ngũ l&atilde;nh đạo chuy&ecirc;n nghiệp, sau một khoảng thời gian h&igrave;nh th&agrave;nh v&agrave; ph&aacute;t triển, Thi&ecirc;n H&agrave; Kameda đ&atilde; tạo được tiếng vang lớn tr&ecirc;n thị trường Việt Nam với sản phẩm b&aacute;nh gạo gi&ograve;n ngon chuẩn Nhật, được người ti&ecirc;u d&ugrave;ng đ&oacute;n nhận v&agrave; y&ecirc;u th&iacute;ch. Từ một nh&agrave; m&aacute;y đầu ti&ecirc;n tại miền Bắc (Hưng Y&ecirc;n), Thi&ecirc;n H&agrave; Kameda tiếp tục mở rộng quy m&ocirc; sản xuất tại miền Trung v&agrave; miền Nam bằng việc th&agrave;nh lập 2 nh&agrave; m&aacute;y tại Thừa Thi&ecirc;n Huế (năm 2015) v&agrave; Đồng Th&aacute;p (năm 2016).</p>\n<p>Với mong muốn mang đến cho người Việt sản phẩm b&aacute;nh gạo chất lượng chuẩn Nhật Bản nhưng vẫn ph&ugrave; hợp với khẩu vị Việt Nam, đội ngũ Thi&ecirc;n H&agrave; Kameda lu&ocirc;n ch&uacute; trọng từng kh&acirc;u trong quy tr&igrave;nh sản xuất, kh&ocirc;ng ngừng cải tiến để mang đến cho người d&ugrave;ng những hương vị độc đ&aacute;o trong từng chiếc b&aacute;nh gi&ograve;n tan.</p>\n<p>Hiện tại, ICHI l&agrave; một trong những thương hiệu b&aacute;nh gạo b&aacute;n chạy nhất tại thị trường Việt Nam. Trong đ&oacute;, sản phẩm ghi dấu ấn v&agrave; được y&ecirc;u th&iacute;ch nhất l&agrave; b&aacute;nh gạo Nhật vị nước tương Shouyu mật ong. Với sự k&ecirc;́t hợp của những nguy&ecirc;n liệu thượng hạng: gạo Japonica thơm lừng, nước tương Shouyu đặc trưng của Nhật Bản v&agrave; vị ngọt dịu của m&acirc;̣t ong, từng chiếc b&aacute;nh cho người d&ugrave;ng trải nghiệm ẩm thực độc đ&aacute;o, thơm ngon, gi&ograve;n tan bất ngờ.</p>\n<p>Giữ trọn chất lượng Nhật Bản trong từng chiếc b&aacute;nh, kh&ocirc;ng ngừng mang đến những hương vị v&agrave; trải nghiệm mới lạ, b&aacute;nh gạo Nhật ICHI (c&oacute; nghĩa l&agrave; số 1 trong tiếng Nhật), đ&uacute;ng như t&ecirc;n gọi, tiếp tục sẽ l&agrave; c&aacute;i t&ecirc;n người d&ugrave;ng nhớ đến đầu ti&ecirc;n khi nghĩ đến m&oacute;n b&aacute;nh gạo Nhật thơm ngon.</p>', 'Suốt quá trình hình thành và phát triển, Thiên Hà Kameda đã vinh dự nhận được các chứng nhận 								về HT an toàn thực phẩm 22000, giấy chứng nhận HACCP 2003, ISO 22000:205.', 'Hàng triệu người dùng đã lựa chọn & yêu thích', '2020-09-28 02:45:00', '2020-10-05 02:48:41');

-- --------------------------------------------------------

--
-- Table structure for table `apply_job_carrers`
--

CREATE TABLE `apply_job_carrers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullName` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isread` tinyint(1) DEFAULT NULL,
  `phoneNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkFile` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apply_job_carrers`
--

INSERT INTO `apply_job_carrers` (`id`, `fullName`, `ref`, `isread`, `phoneNumber`, `email`, `linkFile`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Trường Nghĩa', '1', 0, '0384441120', 'nguyentruongnghiacntt@gmail.com', NULL, '2020-09-28 19:37:46', '2020-09-28 19:37:46'),
(2, 'Nguyễn Trường Nghĩa', NULL, 0, '0383338840', 'your@email.com', NULL, '2020-09-30 17:22:33', '2020-09-30 17:22:33');

-- --------------------------------------------------------

--
-- Table structure for table `banner_home_pages`
--

CREATE TABLE `banner_home_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_home_pages`
--

INSERT INTO `banner_home_pages` (`id`, `image1`, `image2`, `image3`, `image4`, `title`, `created_at`, `updated_at`) VALUES
(1, 'banner-home-pages\\September2020\\YYSQq1TdslsfbskmXnu5.jpg', 'banner-home-pages\\September2020\\ubpTu6qsoqsXhJ7qfJW3.png', 'banner-home-pages\\September2020\\6hwLo8mMNOU1bivibzkw.jpg', 'banner-home-pages\\September2020\\akKtMrGFNp9Eq8gBCM3O.png', 'BÁNH GẠO NHẬT ', '2020-09-27 20:48:00', '2020-09-30 19:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `banner_items`
--

CREATE TABLE `banner_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyScreen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_items`
--

INSERT INTO `banner_items` (`id`, `image`, `title`, `keyScreen`, `created_at`, `updated_at`) VALUES
(1, 'banner-items\\October2020\\GHl0pDkRxEZvi9U91ZdO.jpg', NULL, 'aboutus', '2020-10-19 20:39:00', '2020-10-19 20:39:41'),
(2, 'banner-items\\October2020\\xbMROku3nopmry5vFvya.jpg', 'TUYỂN DỤNG', 'career', '2020-10-19 20:40:38', '2020-10-19 20:40:38'),
(3, 'banner-items\\October2020\\UyKdTe5C00xhYTzbDDUE.jpg', 'LIÊN HỆ', 'contact', '2020-10-19 20:45:10', '2020-10-19 20:45:10');

-- --------------------------------------------------------

--
-- Table structure for table `banner_pages`
--

CREATE TABLE `banner_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `priorites` int(11) NOT NULL DEFAULT 1,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyScreen` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carrer_object_items`
--

CREATE TABLE `carrer_object_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `positionName` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `locationName` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageShare` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `quatinity` int(11) DEFAULT NULL,
  `applicationDealine` date DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkImageShare` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleSeo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descriptionSEO` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carrer_object_items`
--

INSERT INTO `carrer_object_items` (`id`, `positionName`, `step`, `locationName`, `priority`, `content`, `imageShare`, `slug`, `shortDescription`, `created_at`, `quatinity`, `applicationDealine`, `updated_at`, `keyword`, `linkImageShare`, `titleSeo`, `descriptionSEO`, `author`) VALUES
(2, 'Giám Sát Bán Hàng (Sales Supervisor)', 1, 'Tp. Hồ Chí Minh', 1, '<p class=\"font-weight-bold\">M&ocirc; tả c&ocirc;ng việc</p>\n<p>- Quản l&yacute; trực tiếp về nh&acirc;n sự (S.R, PGs), Doanh số của khu vực được chỉ định từ RSM (nếu chưa c&oacute; ASM).</p>\n<p>- Sẵn s&agrave;ng nhận chỉ ti&ecirc;u doanh số, độ phủ, chi ph&iacute;, từ cấp quản l&yacute; trực tiếp (ASM, RSM) v&agrave; c&ocirc;ng ty theo từng th&aacute;ng, từng qu&yacute;, v&agrave; năm.</p>\n<p>- Gi&aacute;m s&aacute;t to&agrave;n bộ c&ocirc;ng việc b&aacute;n h&agrave;ng v&agrave; viếng thăm kh&aacute;ch của showroom</p>\n<p>- Đ&ocirc;n đốc, th&uacute;c đẩy, động vi&ecirc;n vi&ecirc;n tất cả nh&acirc;n vi&ecirc;n (S.R, PGs) của m&igrave;nh quản l&yacute;,</p>\n<p>- Đi thị trường với S.R để huấn luyện, đ&agrave;o tạo tại khu vực, hỗ trợ giải quyết kh&oacute; khăn m&agrave; S.R gặp phải tr&ecirc;n thị trường.</p>\n<p>- L&ecirc;n kế hoạch ph&aacute;t triển thị trường cho khu vực m&igrave;nh quản l&yacute;, rồi đưa ra chương tr&igrave;nh khuyến m&atilde;i cho hợp l&yacute; v&agrave; đạt hiệu quả cao. Đồng thời cũng quản l&yacute; chặt chẽ từng chương tr&igrave;nh khuyến m&atilde;i về quy tr&igrave;nh thanh to&aacute;n v&agrave; kiểm so&aacute;t lượng h&agrave;ng b&aacute;n v&agrave; tồn kho một c&aacute;ch ch&iacute;nh x&aacute;c.</p>\n<p>- Nắm th&ocirc;ng tin về kh&aacute;ch h&agrave;ng để tư vấn cho ASM hoặc RSM về vấn đề l&agrave;m promotion cho outlet, t&agrave;i trợ cho outlet.</p>\n<p>- Quản l&yacute; NPP một c&aacute;ch chặt chẽ, về chỉ ti&ecirc;u h&agrave;ng th&aacute;ng, đ&ocirc;n đốc nhắc nhở npp nhập h&agrave;ng v&agrave; b&aacute;n h&agrave;ng theo tiến độ được giao, - Quản l&yacute; được date bia tại Npp, kiểm tra tồn kho.</p>\n<p>- Phối hợp với PGs leader vấn đề giao chỉ ti&ecirc;u v&agrave; chấm c&ocirc;ng của PG cho hợp l&yacute;.</p>\n<p class=\"font-weight-bold\">Quyền lợi được hưởng</p>\n<p>- Thu nhập cạnh tranh từ 15-20 Triệu</p>\n<p>- Lương th&aacute;ng 13</p>\n<p>- Thưởng th&agrave;nh t&iacute;ch theo KPIs</p>\n<p>- C&aacute;c chế độ đ&atilde;i ngộ theo quy định của Nh&agrave; nước; đ&oacute;ng BHXH, BHYT, BHTN theo quy định của C&ocirc;ng ty</p>\n<p class=\"font-weight-bold\">Y&ecirc;u cầu c&ocirc;ng việc</p>\n<p>- Từng l&agrave;m vị tr&iacute; quản l&yacute; ở c&aacute;c vị tr&iacute; tương đương</p>\n<p>- Kinh nghiệm tr&ecirc;n 1 năm</p>\n<p>Từng quen thuộc với thị trường bia địa phương, si&ecirc;u thị, k&ecirc;nh b&aacute;n bu&ocirc;n đại l&yacute;, phục vụ, qu&aacute;n bar v&agrave; mạng lưới b&aacute;n h&agrave;ng v&agrave; c&aacute;c k&ecirc;nh kh&aacute;c</p>\n<p>* Sử dụng th&agrave;nh thạo vi t&iacute;nh v&agrave; c&aacute;c phần mềm văn ph&ograve;ng kh&aacute;c để b&aacute;o c&aacute;o.</p>\n<p class=\"font-weight-bold\">Y&ecirc;u cầu hồ sơ</p>\n<p>- Sơ yếu l&iacute; lịch</p>\n<p>- Bằng cấp li&ecirc;n quan</p>\n<p>- CV c&aacute; nh&acirc;n</p>', NULL, 'giam-sat-ban-hang-1', '- Quản lý trực tiếp về nhân sự (S.R, PGs), Doanh số của khu vực được chỉ định từ RSM (nếu chưa có ASM)', '2020-10-19 00:27:00', 2, '2020-10-20', '2020-10-19 00:27:00', '- Quản lý trực tiếp về nhân sự (S.R, PGs), Doanh số của khu vực được chỉ định từ RSM (nếu chưa có ASM)', NULL, '- Quản lý trực tiếp về nhân sự (S.R, PGs), Doanh số của khu vực được chỉ định từ RSM (nếu chưa có ASM)', '- Quản lý trực tiếp về nhân sự (S.R, PGs), Doanh số của khu vực được chỉ định từ RSM (nếu chưa có ASM)', '- Quản lý trực tiếp về nhân sự (S.R, PGs), Doanh số của khu vực được chỉ định từ RSM (nếu chưa có ASM)');

-- --------------------------------------------------------

--
-- Table structure for table `carrer_object_pages`
--

CREATE TABLE `carrer_object_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contentDescription` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageShare` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carrer_object_pages`
--

INSERT INTO `carrer_object_pages` (`id`, `title`, `contentDescription`, `imageShare`, `created_at`, `updated_at`) VALUES
(1, 'VĂN HOÁ & CON NGƯỜI TẠI TSINGTAO', '<p>Yếu tố con người đ&oacute;ng vai tr&ograve; quan trọng nhất trong mọi hoạt động của TSINGTAO. Việt Nam. Đ&oacute; l&agrave; l&yacute; do TSINGTAO. Việt Nam mang đến một m&ocirc;i trường l&agrave;m việc với chế độ đ&atilde;i ngộ v&agrave; những cơ hội ph&aacute;t triển nghề nghiệp hấp dẫn.</p>\r\n<p>C&aacute;c th&agrave;nh vi&ecirc;n của ch&uacute;ng t&ocirc;i đều cởi mở, sẵn s&agrave;ng đ&oacute;n nhận những điều mới v&agrave; lu&ocirc;n quan t&acirc;m đến mọi người xung quanh. B&ecirc;n cạnh đ&oacute;, bạn sẽ được tạo điều kiện để tham gia v&agrave;o c&aacute;c dự &aacute;n phối hợp giữa nhiều ph&ograve;ng ban cũng như l&agrave;m việc với c&aacute;c đồng nghiệp tr&ecirc;n khắp thế giới.</p>\r\n<p>Bạn sẽ được ph&aacute;t triển v&agrave; thử th&aacute;ch bản th&acirc;n qua rất nhiều cơ hội kh&aacute;c nhau. Ch&uacute;ng t&ocirc;i lu&ocirc;n nỗ lực hết m&igrave;nh để kh&ocirc;ng ngừng gắn kết những th&agrave;nh vi&ecirc;n của TSINGTAO.</p>', 'carrer-object-pages\\October2020\\Co9TfTkCtQ1u9Nad62kd.jpg', '2020-10-19 00:24:15', '2020-10-19 00:24:15');

-- --------------------------------------------------------

--
-- Table structure for table `contact_infos`
--

CREATE TABLE `contact_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '(+84) 28 2210 4346',
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '(+84) 908 109 789',
  `phone3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '+852 2851 3029',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'tai.nguyen@tsingtaovietnam.com',
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'www.tsingtaovietnam.com',
  `fullName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'CÔNG TY TNHH BIA TSINGTAO VIỆT NAM',
  `googlemap` longtext COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_infos`
--

INSERT INTO `contact_infos` (`id`, `Address`, `phone1`, `phone2`, `phone3`, `email`, `website`, `fullName`, `googlemap`, `created_at`, `updated_at`) VALUES
(1, 'Lầu 5, Tòa nhà An Phát, Số 1074, Đường Võ Văn Kiệt, Phường 6, Quận 5, Thành phố Hồ Chí Minh, Việt Nam<', '(+84) 28 2210 4346', '(+84) 908 109 789', '+852 2851 3029', 'tai.nguyen@tsingtaovietnam.com', 'www.tsingtaovietnam.com', 'CÔNG TY TNHH BIA TSINGTAO VIỆT NAM', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.7886474796665!2d106.67013624538163!3d10.750765885178128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x547ea9958a19ef77!2zQ8O0bmcgVHkgVG5oaCBDaG8gVGh1w6ogVsSDbiBQaMOybmcgQW4gUGjDoXQ!5e0!3m2!1svi!2s!4v1602060213496!5m2!1svi!2s\" width=\"100%\" height=\"550\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', '2020-10-19 20:04:25', '2020-10-19 20:04:25');

-- --------------------------------------------------------

--
-- Table structure for table `contact_itemms`
--

CREATE TABLE `contact_itemms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phoneNumber` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactAddress` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactAddressMap` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_itemms`
--

INSERT INTO `contact_itemms` (`id`, `title`, `address`, `phoneNumber`, `contactAddress`, `contactAddressMap`, `priority`, `created_at`, `updated_at`) VALUES
(1, 'Nhà máy của Công ty CP Thiên Hà Kameda Hưng Yên (Trụ sở chính)', '<ul>\r\n<li>Địa chỉ: Km 29, QL 5A, Tổ d&acirc;n phố Bến, Phường Bạch Sam, Thị x&atilde; Mỹ H&agrave;o, Tỉnh Hưng Y&ecirc;n.</li>\r\n<li>Điện thoại: Hotline: 0915 898 688</li>\r\n<li>Li&ecirc;n hệ: info@thienha-kamedafood.com</li>\r\n</ul>', '0915 898 688', '<p>Địa chỉ: Km 29, QL 5A, Tổ d&acirc;n phố Bến, Phường Bạch Sam,<br />Thị x&atilde; Mỹ H&agrave;o, Tỉnh Hưng Y&ecirc;n.<br />Điện thoại: Hotline: 0915 898 688<br />Li&ecirc;n hệ: info@thienha-kamedafood.com</p>', '<p>Địa chỉ: Km 29, QL 5A, Tổ d&acirc;n phố Bến, Phường Bạch Sam,<br />Thị x&atilde; Mỹ H&agrave;o, Tỉnh Hưng Y&ecirc;n.<br />Điện thoại: Hotline: 0915 898 688<br />Li&ecirc;n hệ: info@thienha-kamedafood.com</p>', 1, '2020-09-28 19:57:00', '2020-09-30 03:50:10'),
(2, 'Nhà máy của Công ty CP Thiên Hà Kameda - Huế', '<ul>\r\n<li>Địa chỉ: chi nh&aacute;nh tại Thừa Thi&ecirc;n Huế ở Th&ocirc;n Tam Vị, X&atilde; Lộc Tiến, Huyện Ph&uacute; Lộc, Tỉnh Thừa Thi&ecirc;n Huế</li>\r\n</ul>', NULL, '<p>Địa chỉ: chi nh&aacute;nh tại Thừa Thi&ecirc;n Huế ở Th&ocirc;n Tam Vị, X&atilde; Lộc Tiến, Huyện Ph&uacute; Lộc, Tỉnh Thừa Thi&ecirc;n Huế</p>', '<p>Địa chỉ: chi nh&aacute;nh tại Thừa Thi&ecirc;n Huế ở Th&ocirc;n Tam Vị, X&atilde; Lộc Tiến, Huyện Ph&uacute; Lộc, Tỉnh Thừa Thi&ecirc;n Huế</p>', 2, '2020-09-28 19:59:00', '2020-09-30 03:51:05'),
(3, 'Nhà máy của Công ty CP Thiên Hà Kameda Đồng Tháp', '<ul>\r\n<li>Địa chỉ: Cụm c&ocirc;ng nghiệp dịch vụ, thương mại Trường Xu&acirc;n, Ấp 4 X&atilde; Trường Xu&acirc;n Huyện Đồng Th&aacute;p Mười, Tỉnh Đồng Th&aacute;p</li>\r\n</ul>', NULL, '<p>Địa chỉ: Cụm c&ocirc;ng nghiệp dịch vụ, thương mại Trường Xu&acirc;n, Ấp 4 X&atilde; Trường Xu&acirc;n Huyện Đồng Th&aacute;p Mười, Tỉnh Đồng Th&aacute;p</p>', '<p>Địa chỉ: Cụm c&ocirc;ng nghiệp dịch vụ, thương mại Trường Xu&acirc;n, Ấp 4 X&atilde; Trường Xu&acirc;n Huyện Đồng Th&aacute;p Mười, Tỉnh Đồng Th&aacute;p</p>', 3, '2020-09-28 20:00:00', '2020-09-30 03:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `contact_requests`
--

CREATE TABLE `contact_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachfile` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isRead` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_requests`
--

INSERT INTO `contact_requests` (`id`, `fullName`, `email`, `phone`, `content`, `attachfile`, `isRead`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Trường Nghĩa1313', 'your@email.com', '0383338840', '131`3131331', NULL, 0, '2020-09-30 17:32:41', '2020-09-30 17:32:41'),
(2, 'Nguyễn Trường Nghĩa', 'your@email.com', '0383338840', '1313', NULL, 0, '2020-09-30 22:54:39', '2020-09-30 22:54:39'),
(3, 'Nguyễn Trường Nghĩa', 'your@email.com', '0383338840', 'NGHIATEST', NULL, 0, '2020-10-19 20:11:45', '2020-10-19 20:11:45');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'image1', 'image', 'Background image', 0, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'image2', 'image', 'Banner Ichi', 0, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'image3', 'image', 'footer Banner', 0, 1, 1, 1, 1, 1, '{}', 4),
(26, 4, 'image4', 'image', 'Banner Ichi 2', 0, 1, 1, 1, 1, 1, '{}', 5),
(27, 4, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(30, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(31, 5, 'image1', 'hidden', 'Hình', 0, 0, 1, 1, 1, 1, '{}', 2),
(32, 5, 'image2', 'hidden', 'Image2', 0, 0, 1, 1, 1, 1, '{}', 3),
(33, 5, 'image', 'image', 'Image cover', 0, 1, 1, 1, 1, 1, '{}', 4),
(34, 5, 'title', 'text', 'Text display', 0, 1, 1, 1, 1, 1, '{}', 5),
(35, 5, 'shortDescription', 'text_area', 'Short description', 0, 0, 1, 1, 1, 1, '{}', 6),
(36, 5, 'slug', 'hidden', 'Slug', 0, 1, 1, 1, 1, 1, '{}', 7),
(37, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(38, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(39, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 6, 'image1', 'hidden', 'Image1', 0, 1, 1, 1, 1, 1, '{}', 2),
(41, 6, 'image2', 'hidden', 'Image2', 0, 1, 1, 1, 1, 1, '{}', 3),
(42, 6, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(43, 6, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(44, 6, 'shortDescription', 'rich_text_box', 'ShortDescription', 0, 0, 1, 1, 1, 1, '{}', 6),
(45, 6, 'slug', 'hidden', 'Slug', 0, 0, 1, 1, 1, 1, '{}', 7),
(46, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(47, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(48, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(49, 7, 'image1', 'hidden', 'Image1', 0, 0, 1, 1, 1, 1, '{}', 2),
(50, 7, 'image2', 'hidden', 'Image2', 0, 0, 1, 1, 1, 1, '{}', 3),
(51, 7, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(52, 7, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(53, 7, 'shortDescription', 'rich_text_box', 'ShortDescription', 0, 0, 1, 1, 1, 1, '{}', 6),
(54, 7, 'slug', 'hidden', 'Slug', 0, 0, 1, 1, 1, 1, '{}', 7),
(55, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(56, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(57, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(58, 8, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(59, 8, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, '{}', 3),
(60, 8, 'phone', 'text', 'Phone', 0, 1, 1, 1, 1, 1, '{}', 4),
(61, 8, 'hotline', 'text', 'Hotline', 0, 1, 1, 1, 1, 1, '{}', 5),
(62, 8, 'facebookLink', 'text', 'FacebookLink', 0, 0, 1, 1, 1, 1, '{}', 6),
(63, 8, 'twitterLink', 'text', 'TwitterLink', 0, 0, 1, 1, 1, 1, '{}', 7),
(64, 8, 'instagramLink', 'text', 'InstagramLink', 0, 0, 1, 1, 1, 1, '{}', 8),
(65, 8, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 9),
(71, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 15),
(72, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 16),
(73, 8, 'logo', 'hidden', 'Logo', 0, 0, 1, 1, 1, 1, '{}', 8),
(74, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(75, 9, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(76, 9, 'content', 'rich_text_box', 'Content', 0, 0, 1, 1, 1, 1, '{}', 3),
(77, 9, 'footerText', 'text', 'FooterText', 0, 1, 1, 1, 1, 1, '{}', 4),
(78, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(79, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(80, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(81, 10, 'image1', 'image', 'Image1', 0, 1, 1, 1, 1, 1, '{}', 2),
(82, 10, 'priorites', 'text', 'Priorites', 0, 1, 1, 1, 1, 1, '{}', 3),
(83, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 4),
(84, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(85, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(86, 11, 'priorites', 'number', 'Priorites', 0, 1, 1, 1, 1, 1, '{}', 2),
(87, 11, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(88, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(89, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(90, 9, 'QuatinityText', 'text', 'QuatinityText', 0, 1, 1, 1, 1, 1, '{}', 4),
(91, 10, 'linkfile', 'file', 'Linkfile', 0, 0, 1, 1, 1, 1, '{}', 3),
(92, 10, 'slug', 'hidden', 'Slug', 0, 0, 1, 1, 1, 1, '{}', 4),
(93, 11, 'number', 'number', 'Number', 0, 1, 1, 1, 1, 1, '{}', 4),
(94, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(95, 12, 'image1', 'hidden', 'Image1', 0, 0, 1, 1, 1, 1, '{}', 2),
(96, 12, 'image2', 'hidden', 'Image2', 0, 0, 1, 1, 1, 1, '{}', 3),
(97, 12, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(98, 12, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(99, 12, 'shortDescription', 'text', 'ShortDescription', 0, 0, 1, 1, 1, 1, '{}', 6),
(100, 12, 'slug', 'hidden', 'Slug', 0, 0, 1, 1, 1, 1, '{}', 7),
(101, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(102, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(103, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(104, 13, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(105, 13, 'content', 'rich_text_box', 'Content', 0, 0, 1, 1, 1, 1, '{}', 3),
(106, 13, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 4),
(107, 13, 'step', 'number', 'step', 0, 1, 1, 1, 1, 1, '{}', 5),
(108, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(109, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(110, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(111, 14, 'positionName', 'text', 'PositionName', 0, 1, 1, 1, 1, 1, '{}', 2),
(112, 14, 'step', 'text', 'Step', 0, 0, 1, 1, 1, 1, '{}', 3),
(113, 14, 'locationName', 'text', 'LocationName', 0, 1, 1, 1, 1, 1, '{}', 4),
(114, 14, 'priority', 'number', 'Priority', 0, 1, 1, 1, 1, 1, '{}', 5),
(115, 14, 'content', 'rich_text_box', 'Content', 0, 0, 1, 1, 1, 1, '{}', 6),
(116, 14, 'imageShare', 'image', 'ImageShare', 0, 1, 1, 1, 1, 1, '{}', 7),
(117, 14, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{}', 8),
(118, 14, 'shortDescription', 'text', 'ShortDescription', 0, 0, 1, 1, 1, 1, '{}', 9),
(119, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(120, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(121, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(122, 15, 'title', 'text', 'name', 0, 1, 1, 1, 1, 1, '{}', 2),
(123, 15, 'address', 'rich_text_box', 'Decription ', 0, 0, 1, 1, 1, 1, '{}', 3),
(124, 15, 'phoneNumber', 'hidden', 'PhoneNumber', 0, 0, 1, 1, 1, 1, '{}', 4),
(125, 15, 'contactAddress', 'rich_text_box', 'Google map description', 0, 0, 1, 1, 1, 1, '{}', 5),
(126, 15, 'contactAddressMap', 'hidden', 'Addres-Contact Map', 0, 0, 1, 1, 1, 1, '{}', 6),
(127, 15, 'priority', 'number', 'Priority', 0, 1, 1, 1, 1, 1, '{}', 7),
(128, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(129, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(130, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(131, 16, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(132, 16, 'codeColor', 'text', 'Main color', 0, 1, 1, 1, 1, 1, '{}', 3),
(133, 16, 'backgroundImamge', 'image', 'BackgroundImamge', 0, 1, 1, 1, 1, 1, '{}', 7),
(134, 16, 'shortDescription', 'rich_text_box', 'ShortDescription', 0, 0, 1, 1, 1, 1, '{}', 9),
(135, 16, 'weight', 'text', 'Weight', 0, 0, 1, 1, 1, 1, '{}', 10),
(136, 16, 'priority', 'number', 'Priority', 0, 1, 1, 1, 1, 1, '{}', 11),
(137, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 12),
(138, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(139, 16, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{}', 8),
(140, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(141, 17, 'backgroundImamge', 'image', 'BackgroundImamge', 0, 1, 1, 1, 1, 1, '{}', 2),
(142, 17, 'weight', 'text', 'Weight', 0, 1, 1, 1, 1, 1, '{}', 3),
(143, 17, 'priority', 'number', 'Priority', 0, 1, 1, 1, 1, 1, '{}', 4),
(144, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(145, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(146, 17, 'product_category_image_belongsto_product_category_relationship', 'relationship', 'product_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"categoryId\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"aboutus_pages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(147, 17, 'categoryId', 'text', 'CategoryId', 0, 1, 1, 1, 1, 1, '{}', 5),
(148, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(149, 18, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(150, 18, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{}', 3),
(153, 18, 'shortDescription', 'text', 'ShortDescription', 0, 0, 1, 1, 1, 1, '{}', 6),
(156, 18, 'priority', 'number', 'Priority', 0, 1, 1, 1, 1, 1, '{}', 9),
(157, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(158, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(159, 18, 'product_belongsto_product_category_relationship', 'relationship', 'product_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"aboutus_pages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(160, 5, 'home_page_ichi_belongsto_product_category_relationship', 'relationship', 'product_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"categoryid\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"aboutus_pages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(161, 5, 'categoryid', 'hidden', 'Categoryid', 0, 1, 1, 1, 1, 1, '{}', 9),
(162, 12, 'home_page_kid_ichi_cranker_belongsto_product_category_relationship', 'relationship', 'product_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"categoryId\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"aboutus_pages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(163, 12, 'categoryId', 'hidden', 'CategoryId', 0, 1, 1, 1, 1, 1, '{}', 10),
(164, 7, 'home_page_kid_ichi_belongsto_product_category_relationship', 'relationship', 'product_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"categoryId\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"aboutus_pages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(165, 7, 'categoryId', 'hidden', 'CategoryId', 0, 1, 1, 1, 1, 1, '{}', 10),
(166, 6, 'home_page_snack_ichi_belongsto_product_category_relationship', 'relationship', 'product_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"categoryId\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"aboutus_pages\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(167, 6, 'categoryId', 'hidden', 'CategoryId', 0, 1, 1, 1, 1, 1, '{}', 10),
(169, 16, 'titleDisplay', 'text', 'TitleDisplay', 0, 1, 1, 1, 1, 1, '{}', 5),
(170, 16, 'classCode', 'text', 'ClassCode', 0, 1, 1, 1, 1, 1, '{}', 14),
(171, 16, 'titleDetail', 'text', 'Title page detail', 0, 1, 1, 1, 1, 1, '{}', 6),
(172, 16, 'codeColorProductCategory', 'text', 'Style of page category', 0, 1, 1, 1, 1, 1, '{}', 4),
(173, 16, 'titleHomePage', 'text', 'TitleHomePage', 0, 1, 1, 1, 1, 1, '{}', 3),
(174, 16, 'descriptionHomePage', 'text', 'DescriptionHomePage', 0, 1, 1, 1, 1, 1, '{}', 4),
(175, 16, 'imagHomePage', 'image', 'Image home page', 0, 1, 1, 1, 1, 1, '{}', 5),
(176, 16, 'titleSeo', 'text', 'TitleSeo', 0, 1, 1, 1, 1, 1, '{}', 18),
(177, 16, 'descriptionSEO', 'text', 'DescriptionSEO', 0, 1, 1, 1, 1, 1, '{}', 19),
(178, 16, 'linkImageShare', 'text', 'LinkImageShare', 0, 1, 1, 1, 1, 1, '{}', 20),
(179, 16, 'keyword', 'text', 'Keyword', 0, 1, 1, 1, 1, 1, '{}', 21),
(180, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(181, 19, 'titleHOmePage', 'text', 'Home page text display', 0, 1, 1, 1, 1, 1, '{}', 2),
(182, 19, 'descriptionContentHomePage', 'text_area', 'Home page content display', 0, 1, 1, 1, 1, 1, '{}', 3),
(183, 19, 'imageHomePage', 'image', 'Homepage image', 0, 1, 1, 1, 1, 1, '{}', 4),
(184, 19, 'bannerImage', 'image', 'Banner image', 0, 1, 1, 1, 1, 1, '{}', 5),
(185, 19, 'titleIntroduction', 'text', 'TitleIntroduction', 0, 1, 1, 1, 1, 1, '{}', 6),
(186, 19, 'introductionContent', 'code_editor', 'IntroductionContent', 0, 1, 1, 1, 1, 1, '{}', 7),
(187, 19, 'tsingTaoVietNamTitle', 'text', 'TsingTaoVietNamTitle', 0, 1, 1, 1, 1, 1, '{}', 8),
(188, 19, 'tsingTaoVietNamContent', 'rich_text_box', 'TsingTaoVietNamContent', 0, 1, 1, 1, 1, 1, '{}', 9),
(189, 19, 'tsingTaoVietNamMisionTitle', 'text', 'TsingTaoVietNamMisionTitle', 0, 1, 1, 1, 1, 1, '{}', 10),
(190, 19, 'tsingTaoVietNamMisionContent', 'rich_text_box', 'TsingTaoVietNamMisionContent', 0, 1, 1, 1, 1, 1, '{}', 11),
(191, 19, 'titlePage', 'text', 'TitlePage', 0, 1, 1, 1, 1, 1, '{}', 12),
(192, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(193, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(194, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(195, 20, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(196, 20, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(197, 20, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{}', 4),
(198, 20, 'priorites', 'number', 'Priorites', 1, 1, 1, 1, 1, 1, '{}', 5),
(199, 20, 'isActive', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 6),
(200, 20, 'isSpecial', 'select_dropdown', 'Sepecial', 1, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 7),
(201, 20, 'keyWord', 'text', 'KeyWord', 0, 1, 1, 1, 1, 1, '{}', 8),
(202, 20, 'linkShare', 'image', 'LinkShare', 0, 0, 1, 1, 1, 1, '{}', 9),
(203, 20, 'shortDescription', 'text_area', 'ShortDescription', 0, 1, 1, 1, 1, 1, '{}', 10),
(204, 20, 'backgroundImage', 'image', 'BackgroundImage', 0, 1, 1, 1, 1, 1, '{}', 11),
(205, 20, 'titleSeo', 'text', 'TitleSeo', 0, 1, 1, 1, 1, 1, '{}', 12),
(206, 20, 'imageShare', 'image', 'ImageShare', 0, 1, 1, 1, 1, 1, '{}', 13),
(207, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(208, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(209, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(210, 21, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(211, 21, 'contentDescription', 'rich_text_box', 'ContentDescription', 0, 1, 1, 1, 1, 1, '{}', 3),
(212, 21, 'imageShare', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(213, 21, 'priorites', 'number', 'Priorites', 1, 1, 1, 1, 1, 1, '{}', 5),
(214, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(215, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(216, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(217, 22, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(218, 22, 'contentDescription', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(219, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(220, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(221, 14, 'quatinity', 'number', 'Quatinity', 0, 1, 1, 1, 1, 1, '{}', 11),
(222, 14, 'applicationDealine', 'date', 'Application Date', 0, 1, 1, 1, 1, 1, '{}', 12),
(223, 14, 'keyword', 'text', 'Keyword', 0, 1, 1, 1, 1, 1, '{}', 14),
(224, 14, 'linkImageShare', 'text', 'LinkImageShare', 0, 1, 1, 1, 1, 1, '{}', 15),
(225, 14, 'titleSeo', 'text', 'TitleSeo', 0, 1, 1, 1, 1, 1, '{}', 16),
(226, 14, 'descriptionSEO', 'text', 'DescriptionSEO', 0, 1, 1, 1, 1, 1, '{}', 17),
(227, 14, 'author', 'text', 'Author', 0, 1, 1, 1, 1, 1, '{}', 18),
(228, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(229, 23, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(230, 23, 'contentDescription', 'rich_text_box', 'ContentDescription', 0, 1, 1, 1, 1, 1, '{}', 3),
(231, 23, 'imageShare', 'image', 'ImageShare', 0, 1, 1, 1, 1, 1, '{}', 4),
(232, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(233, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(234, 18, 'backgroundImage', 'image', 'BackgroundImage', 0, 1, 1, 1, 1, 1, '{}', 4),
(235, 18, 'imageContent', 'image', 'ImageContent', 0, 1, 1, 1, 1, 1, '{}', 6),
(236, 18, 'ImageBanner', 'image', 'ImageBanner', 0, 1, 1, 1, 1, 1, '{}', 7),
(237, 18, 'shortDescriptionBanner', 'rich_text_box', 'ShortDescriptionBanner', 0, 1, 1, 1, 1, 1, '{}', 11),
(238, 18, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 12),
(239, 18, 'shortDescriptionTitle', 'text', 'ShortDescriptionTitle', 0, 1, 1, 1, 1, 1, '{}', 13),
(240, 18, 'titleSeo', 'text', 'TitleSeo', 0, 1, 1, 1, 1, 1, '{}', 14),
(241, 18, 'descriptionSEO', 'text', 'DescriptionSEO', 0, 1, 1, 1, 1, 1, '{}', 15),
(242, 18, 'linkImageShare', 'image', 'LinkImageShare', 0, 1, 1, 1, 1, 1, '{}', 16),
(243, 18, 'shortDescriptionContent', 'rich_text_box', 'ShortDescriptionContent', 0, 1, 1, 1, 1, 1, '{}', 17),
(244, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(245, 24, 'priorites', 'number', 'Priorites', 1, 1, 1, 1, 1, 1, '{}', 2),
(246, 24, 'title', 'hidden', 'Title', 0, 0, 1, 1, 1, 1, '{}', 3),
(247, 24, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(248, 24, 'shortDescription', 'hidden', 'ShortDescription', 0, 0, 1, 1, 1, 1, '{}', 5),
(249, 24, 'isActive', 'hidden', 'IsActive', 1, 0, 1, 1, 1, 1, '{}', 6),
(250, 24, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 7),
(251, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(252, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(253, 25, 'Homepage_productsingtaoTile', 'text', 'Homepage ProductsingtaoTile', 0, 1, 1, 1, 1, 1, '{}', 2),
(254, 25, 'Homepage_videoSpecial', 'text', 'Homepage VideoSpecial', 0, 1, 1, 1, 1, 1, '{}', 3),
(255, 25, 'Homepage_newEvent', 'text', 'Homepage NewEvent', 0, 1, 1, 1, 1, 1, '{}', 4),
(256, 25, 'btnSeeMore', 'text', 'BtnSeeMore', 0, 1, 1, 1, 1, 1, '{}', 5),
(257, 25, 'btnSeeDetail', 'text', 'BtnSeeDetail', 0, 1, 1, 1, 1, 1, '{}', 6),
(258, 25, 'carrer_PositionName', 'text', 'Carrer PositionName', 0, 1, 1, 1, 1, 1, '{}', 7),
(259, 25, 'carrer_Quatinity', 'text', 'Carrer Quatinity', 0, 1, 1, 1, 1, 1, '{}', 8),
(260, 25, 'carrer_WorkLocation', 'text', 'Carrer WorkLocation', 0, 1, 1, 1, 1, 1, '{}', 9),
(261, 25, 'carrer_ApplicationDealine', 'text', 'Carrer ApplicationDealine', 0, 1, 1, 1, 1, 1, '{}', 10),
(262, 25, 'new_lasted', 'text', 'New Lasted', 0, 1, 1, 1, 1, 1, '{}', 11),
(263, 25, 'new_Special', 'text', 'New Special', 0, 1, 1, 1, 1, 1, '{}', 12),
(264, 25, 'new_backtoNew', 'text', 'New BacktoNew', 0, 1, 1, 1, 1, 1, '{}', 13),
(265, 25, 'new_other', 'text', 'New Other', 0, 1, 1, 1, 1, 1, '{}', 14),
(266, 25, 'contactinfo', 'text', 'Contactinfo', 0, 1, 1, 1, 1, 1, '{}', 15),
(267, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 16),
(268, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 17),
(269, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(270, 26, 'Address', 'text', 'Address', 0, 1, 1, 1, 1, 1, '{}', 2),
(271, 26, 'phone1', 'text', 'Phone1', 0, 1, 1, 1, 1, 1, '{}', 3),
(272, 26, 'phone2', 'text', 'Phone2', 0, 1, 1, 1, 1, 1, '{}', 4),
(273, 26, 'phone3', 'text', 'Phone3', 0, 1, 1, 1, 1, 1, '{}', 5),
(274, 26, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 6),
(275, 26, 'website', 'text', 'Website', 0, 1, 1, 1, 1, 1, '{}', 7),
(276, 26, 'fullName', 'text', 'Full company name', 0, 1, 1, 1, 1, 1, '{}', 8),
(277, 26, 'googlemap', 'code_editor', 'Googlemap', 0, 1, 1, 1, 1, 1, '{}', 9),
(278, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(279, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(280, 27, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(281, 27, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(282, 27, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(283, 27, 'keyScreen', 'select_dropdown', 'KeyScreen', 0, 1, 1, 1, 1, 1, '{\"default\":\"contact\",\"options\":{\"contact\":\"Li\\u00ean h\\u1ec7\",\"news\":\"Tin t\\u1ee9c\",\"aboutus\":\"Gi\\u1edbi thi\\u1ec7u\",\"career\":\"C\\u01a1 h\\u1ed9i v\\u00e0 ngh\\u1ec1 nghi\\u1ec7p\",\"developer\":\"Ph\\u00e1t tri\\u1ec3n v\\u00e0 b\\u1ec1n v\\u1eefng\",\"product\":\"S\\u1ea3n ph\\u1ea9m\"}}', 4),
(284, 27, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(285, 27, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(286, 28, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(287, 28, 'shortName', 'text', 'ShortName', 0, 1, 1, 1, 1, 1, '{}', 2),
(288, 28, 'phone1', 'text', 'Phone1', 0, 1, 1, 1, 1, 1, '{}', 3),
(289, 28, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 4),
(290, 28, 'footerTextFind', 'text', 'FooterTextFind', 0, 1, 1, 1, 1, 1, '{}', 5),
(291, 28, 'facebooklink', 'text', 'Facebooklink', 0, 1, 1, 1, 1, 1, '{}', 6),
(292, 28, 'youtubeLink', 'text', 'YoutubeLink', 0, 1, 1, 1, 1, 1, '{}', 7),
(293, 28, 'instagramlink', 'text', 'Instagramlink', 0, 1, 1, 1, 1, 1, '{}', 8),
(294, 28, 'footerText', 'text', 'FooterText', 0, 1, 1, 1, 1, 1, '{}', 9),
(295, 28, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(296, 28, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(4, 'banner_home_pages', 'banner-home-pages', 'HomePage - Banner', 'HomePage - Banner', NULL, 'App\\BannerHomePage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-27 20:39:36', '2020-09-27 20:46:49'),
(5, 'home_page_ichis', 'home-page-ichis', 'Home Page Ichi', 'Home Page Ichis', NULL, 'App\\HomePageIchi', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 01:48:40', '2020-09-30 19:20:52'),
(6, 'home_page_snack_ichis', 'home-page-snack-ichis', 'Home Page Snack Ichi', 'Home Page Snack Ichis', NULL, 'App\\HomePageSnackIchi', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 02:01:33', '2020-09-30 19:24:55'),
(7, 'home_page_kid_ichis', 'home-page-kid-ichis', 'Home Page Kid Ichi', 'Home Page Kid Ichis', NULL, 'App\\HomePageKidIchi', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 02:18:34', '2020-09-30 19:22:48'),
(8, 'infomation_companies', 'infomation-companies', 'Infomation Company', 'Infomation Companies', NULL, 'App\\InfomationCompany', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 02:30:14', '2020-09-30 19:26:05'),
(9, 'aboutus_pages', 'aboutus-pages', 'Aboutus Page', 'Aboutus Pages', NULL, 'App\\AboutusPage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 02:37:11', '2020-09-30 19:26:29'),
(10, 'quality_liceneces', 'quality-liceneces', 'Quality Licenece', 'Quality Liceneces', NULL, 'App\\QualityLicenece', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 02:37:35', '2020-09-30 19:27:54'),
(11, 'number_impresses', 'number-impresses', 'Number Impress', 'Number Impresses', NULL, 'App\\NumberImpress', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 02:37:56', '2020-09-28 03:03:50'),
(12, 'home_page_kid_ichi_crankers', 'home-page-kid-ichi-crankers', 'Home Page Kid Ichi Cranker', 'Home Page Kid Ichi Crankers', NULL, 'App\\HomePageKidIchiCranker', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 03:18:06', '2020-09-30 19:21:46'),
(13, 'step_items', 'step-items', 'Step Item', 'Step Items', NULL, 'App\\StepItem', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 18:36:13', '2020-09-30 19:28:48'),
(14, 'carrer_object_items', 'carrer-object-items', 'Management careers', 'Management careers', NULL, 'App\\CarrerObjectItem', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 18:52:45', '2020-10-19 00:22:40'),
(15, 'contact_itemms', 'contact-itemms', 'Contact Itemm', 'Contact Itemms', NULL, 'App\\ContactItemm', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 19:54:04', '2020-09-30 19:30:41'),
(16, 'product_categories', 'product-categories', 'Product Category', 'Product Categories', NULL, 'App\\ProductCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 21:44:10', '2020-10-03 01:15:44'),
(17, 'product_category_images', 'product-category-images', 'Product Category Image', 'Product Category Images', NULL, 'App\\ProductCategoryImage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 21:59:29', '2020-09-28 22:01:04'),
(18, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-09-28 22:07:51', '2020-10-19 01:27:37'),
(19, 'introduction_object_pages', 'introduction-object-pages', 'Introduction Object Page', 'Introduction Object Pages', NULL, 'App\\IntroductionObjectPage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-18 21:07:02', '2020-10-18 21:11:01'),
(20, 'news', 'news', 'News', 'News', NULL, 'App\\News', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-18 21:45:46', '2020-10-18 21:45:46'),
(21, 'developer_items', 'developer-items', 'Developer Item', 'Developer Items', NULL, 'App\\DeveloperItem', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-18 23:53:32', '2020-10-18 23:55:27'),
(22, 'developer_pages', 'developer-pages', 'Developer Page', 'Developer Pages', NULL, 'App\\DeveloperPage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-18 23:54:10', '2020-10-18 23:54:10'),
(23, 'carrer_object_pages', 'carrer-object-pages', 'Carrer Object Page', 'Carrer Object Pages', NULL, 'App\\CarrerObjectPage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-19 00:23:06', '2020-10-19 00:23:06'),
(24, 'slide_banners', 'slide-banners', 'Slide Banner', 'Slide Banners', NULL, 'App\\SlideBanner', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-19 19:10:10', '2020-10-19 19:10:10'),
(25, 'language_displays', 'language-displays', 'Language Display', 'Language Displays', NULL, 'App\\LanguageDisplay', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-19 19:57:36', '2020-10-19 19:57:36'),
(26, 'contact_infos', 'contact-infos', 'Contact Info', 'Contact Infos', NULL, 'App\\ContactInfo', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-19 20:02:33', '2020-10-19 20:02:33'),
(27, 'banner_items', 'banner-items', 'Banner Item', 'Banner Items', NULL, 'App\\BannerItem', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-19 20:38:03', '2020-10-19 20:46:47'),
(28, 'footer_infos', 'footer-infos', 'Footer Info', 'Footer Infos', NULL, 'App\\FooterInfo', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-20 00:27:14', '2020-10-20 00:27:14');

-- --------------------------------------------------------

--
-- Table structure for table `developer_items`
--

CREATE TABLE `developer_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contentDescription` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageShare` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priorites` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `developer_items`
--

INSERT INTO `developer_items` (`id`, `title`, `contentDescription`, `imageShare`, `priorites`, `created_at`, `updated_at`) VALUES
(1, 'NĂNG LỰC CON NGƯỜI & GIỮ GÌN HÀNH TINH', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown</p>', 'developer-items\\October2020\\yJOzVURGz0pHldJ0rFOJ.png', 1, '2020-10-18 23:56:25', '2020-10-18 23:56:25'),
(2, 'BÁO CÁO PHÁT TRIỂN BỀN VỮNG', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown</p>', 'developer-items\\October2020\\IcxjqvzXtXyKeV5SRSCA.png', 2, '2020-10-18 23:56:56', '2020-10-18 23:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `developer_pages`
--

CREATE TABLE `developer_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contentDescription` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `developer_pages`
--

INSERT INTO `developer_pages` (`id`, `title`, `contentDescription`, `created_at`, `updated_at`) VALUES
(1, 'VÌ MỘT VIỆT NAM TỐT ĐẸP HƠN.', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown</p>', '2020-10-18 23:55:01', '2020-10-18 23:55:01');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `footer_infos`
--

CREATE TABLE `footer_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shortName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'TSINGTAO VIETNAM',
  `phone1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '(+84) 908 109 789',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '+852 2851 3029',
  `footerTextFind` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Find Tsingtao on Social Media',
  `facebooklink` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtubeLink` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagramlink` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footerText` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `footer_infos`
--

INSERT INTO `footer_infos` (`id`, `shortName`, `phone1`, `email`, `footerTextFind`, `facebooklink`, `youtubeLink`, `instagramlink`, `footerText`, `created_at`, `updated_at`) VALUES
(1, 'TSINGTAO VIETNAM', '(+84) 28 2210 4346', 'info@tsingtaovietnam.com', 'info@tsingtaovietnam.com Find Tsingtao on Social Media', 'https://www.facebook.com/tsingtaovietnambeer', 'https://www.youtube.com/channel/UCWCagZwu4h3eMd9G8WnjsTw', 'https://www.tiktok.com/@tsingtaovietnam2020?_d=secCgsIARCbDRgBIAMoARI%2BCjzvubWeKyerMiEJwZlJqgkmsZ6v%2FhJaNio1AmlqZ325aznNmxHpiIAM9Sf3kTumY7PJj3i0rH9T4ZzYWJIaAA%3D%3D&language=en&sec_uid=MS4wLjABAAAAijWeQCSo9jqht9TtTS2nZ2Tfb4GxKd0JbCmrToe5f0RoEWH5hwEy9bJPUBHxKDKA&sec_user_id=MS4wLjABAAAAijWeQCSo9jqht9TtTS2nZ2Tfb4GxKd0JbCmrToe5f0RoEWH5hwEy9bJPUBHxKDKA&share_author_id=6877171522647360514&share_link_id=30A58C63-7BD7-4313-BF66-B2CC1C811C7B&tt_from=more&u_code=deihli4b671b1k&user_id=6877171522647360514&utm_campaign=client_share&utm_medium=ios&utm_source=more&source=h5_t', 'Tsingtao © Copyright 2020. All Rights Reserved. Powered by Maskcodex.', '2020-10-20 00:29:11', '2020-10-20 00:29:11');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_ichis`
--

CREATE TABLE `home_page_ichis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `categoryid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_page_ichis`
--

INSERT INTO `home_page_ichis` (`id`, `image1`, `image2`, `image`, `title`, `shortDescription`, `slug`, `created_at`, `updated_at`, `categoryid`) VALUES
(1, NULL, NULL, 'home-page-ichis\\September2020\\zLQVXvSeLbuNEdwXlT2G.png', 'BÁNH GẠO NHẬT ICHI', 'Giòn tan, ngon lành với vị Shouyu Mật ong đặc trưng. \n								Thêm bất ngờ với 2 vị hoàn toàn mới: Pizza và Thịt xông khói & Phô mai', NULL, '2020-09-28 01:50:00', '2020-10-02 05:11:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_page_kid_ichis`
--

CREATE TABLE `home_page_kid_ichis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_page_kid_ichis`
--

INSERT INTO `home_page_kid_ichis` (`id`, `image1`, `image2`, `image`, `title`, `shortDescription`, `slug`, `created_at`, `updated_at`, `categoryId`) VALUES
(1, NULL, NULL, 'home-page-kid-ichis\\September2020\\uPVs6PbZvs6fCyadF1fn.png', 'ICHI KID', '<p>B&aacute;nh ăn dặm d&agrave;nh cho trẻ em từ 6 th&aacute;ng tuổi với hương vị nguy&ecirc;n bản, được sản xuất từ gạo Japonica cao cấp c&oacute; bổ sung canxi, gi&uacute;p b&eacute; tập nhai, cầm nắm v&agrave; bổ sung dinh dưỡng</p>', NULL, '2020-09-28 02:21:00', '2020-09-29 21:57:27', 5);

-- --------------------------------------------------------

--
-- Table structure for table `home_page_kid_ichi_crankers`
--

CREATE TABLE `home_page_kid_ichi_crankers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_page_kid_ichi_crankers`
--

INSERT INTO `home_page_kid_ichi_crankers` (`id`, `image1`, `image2`, `image`, `title`, `shortDescription`, `slug`, `created_at`, `updated_at`, `categoryId`) VALUES
(1, NULL, 'home-page-kid-ichi-crankers\\September2020\\EKgxjXFQ2prrl8lj7L62.png', 'home-page-kid-ichi-crankers\\September2020\\MNORHS3WxH6pPMZk8GDK.png', 'ICHI CRACKER', '', NULL, '2020-09-28 03:19:00', '2020-10-02 05:10:07', 6);

-- --------------------------------------------------------

--
-- Table structure for table `home_page_snack_ichis`
--

CREATE TABLE `home_page_snack_ichis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_page_snack_ichis`
--

INSERT INTO `home_page_snack_ichis` (`id`, `image1`, `image2`, `image`, `title`, `shortDescription`, `slug`, `created_at`, `updated_at`, `categoryId`) VALUES
(1, NULL, NULL, 'home-page-snack-ichis\\September2020\\6pYFcIkyFUXtQ0Cllche.png', 'Snack Gạo Ichi', '<p>K&iacute;ch th&iacute;ch khẩu vị với 5 hương vị phong ph&uacute;:<br />- Vị Mật ong <br />- Vị Cay <br />- Vị S&ocirc; c&ocirc; la<br />- Vị C&agrave; ri <br />- Vị xốt b&aacute;nh x&egrave;o Nhật</p>', NULL, '2020-09-28 02:04:00', '2020-09-29 21:22:40', 2);

-- --------------------------------------------------------

--
-- Table structure for table `infomation_companies`
--

CREATE TABLE `infomation_companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotline` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebookLink` varchar(251) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitterLink` varchar(251) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(251) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagramLink` varchar(251) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `infomation_companies`
--

INSERT INTO `infomation_companies` (`id`, `name`, `address`, `phone`, `hotline`, `facebookLink`, `twitterLink`, `logo`, `instagramLink`, `email`, `created_at`, `updated_at`) VALUES
(1, 'BÁNH GẠO NHẬT', '', NULL, NULL, 'javascript:vold(0)', 'javascript:vold(0)', NULL, 'javascript:vold(0)', NULL, NULL, '2020-09-30 21:55:11');

-- --------------------------------------------------------

--
-- Table structure for table `introduction_object_pages`
--

CREATE TABLE `introduction_object_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titleHOmePage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descriptionContentHomePage` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageHomePage` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bannerImage` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleIntroduction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `introductionContent` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tsingTaoVietNamTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tsingTaoVietNamContent` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tsingTaoVietNamMisionTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tsingTaoVietNamMisionContent` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titlePage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `introduction_object_pages`
--

INSERT INTO `introduction_object_pages` (`id`, `titleHOmePage`, `descriptionContentHomePage`, `imageHomePage`, `bannerImage`, `titleIntroduction`, `introductionContent`, `tsingTaoVietNamTitle`, `tsingTaoVietNamContent`, `tsingTaoVietNamMisionTitle`, `tsingTaoVietNamMisionContent`, `titlePage`, `created_at`, `updated_at`) VALUES
(1, 'GIỚI THIỆU VỀ TSINGTAO', '<p>\r\n									Tsingtao được thương nhân người Đức và người Anh thành lập 1903 tại Thành phố Thanh Đảo, tỉnh Sơn Đông, Trung Quốc.<br>\r\n									Tsingtao là nhà sản xuất bia thứ 6 thế giới và đứng đầu tại Trung Quốc, hiện diện trên 100 quốc gia và vùng lãnh thổ.<br>\r\n									Bia Tsingtao nhận nhiều giải thưởng vàng chất lượng thế giới: Huy chương vàng World Beer Championship tại Mỹ và European Beer Star tại Đức.<br>\r\n									Tsingtao là nhà tài trợ chính thức Thế Vận Hội Olympic 2022.\r\n								</p>', 'introduction-object-pages\\October2020\\cZB0mmIpig0811wxSbKd.jpg', 'introduction-object-pages\\October2020\\dOzXkF00uJIoVPyPPJzd.jpg', 'TẬP ĐOÀN TSINGTAO', '<p>\r\n										Tsingtao được thương nhân người Đức và người Anh thành lập 1903 tại Thành phố Thanh Đảo, tỉnh Sơn Đông, Trung Quốc.<br>\r\n										Tsingtao là nhà sản xuất bia thứ 6 thế giới và đứng đầu tại Trung Quốc, hiện diện trên 100 quốc gia và vùng lãnh thổ.<br>\r\n										Bia Tsingtao nhận nhiều giải thưởng vàng chất lượng thế giới: Huy chương vàng World Beer Championship tại Mỹ và European Beer Star tại Đức.<br>\r\n										Tsingtao là nhà tài trợ chính thức Thế Vận Hội Olympic 2022.\r\n									</p>', 'TSINGTAO VIỆT NAM', '<p>Tsingtao Việt Nam ch&iacute;nh thức th&agrave;nh lập v&agrave;o th&aacute;ng 07 năm 2020. Tsingtao Việt Nam nhập khẩu v&agrave; kinh doanh c&aacute;c sản phẩm Bia v&agrave; Thức uống tại thị trường Việt Nam.</p>', 'TẦM NHÌN, SỨ MỆNH VÀ GIÁ TRỊ', '<p>Tsingtao l&agrave; nh&agrave; sản xuất h&agrave;ng đầu về c&aacute;c sản phẩm bia v&agrave; thức uống đẳng cấp mới, c&oacute; tr&aacute;ch nhiệm với cộng đồng.<br />Tsingtao Việt Nam mang sản phẩm y&ecirc;u th&iacute;ch đến người ti&ecirc;u d&ugrave;ng với sự đam m&ecirc; v&agrave; tạo ra niềm vui cho cuộc sống.<br />Gi&aacute; trị cốt l&otilde;i: thương hiệu bia chất lượng ưu việt, c&oacute; tr&aacute;ch nhiệm với x&atilde; hội, gắn kết những gi&aacute; trị nh&acirc;n văn s&acirc;u sắc.</p>', 'Giới thiệu', '2020-10-18 21:10:00', '2020-10-18 21:21:46');

-- --------------------------------------------------------

--
-- Table structure for table `language_displays`
--

CREATE TABLE `language_displays` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Homepage_productsingtaoTile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'CÁC SẢN PHẨM CỦA TSINGTAO',
  `Homeppage_discovery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Khám phá thương hiệu',
  `Homepage_videoSpecial` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'VIDEO NỔI BẬT',
  `Homepage_newEvent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'TIN TỨC & SỰ KIỆN',
  `btnSeeMore` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Xem thêm',
  `btnSeeDetail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'xem chi tiết',
  `carrer_PositionName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'VỊ TRÍ TUYỂN DỤNG',
  `carrer_Quatinity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Số lượng',
  `carrer_WorkLocation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Nơi làm việc',
  `carrer_ApplicationDealine` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Hạn nộp',
  `new_lasted` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Tin mới nhất',
  `new_Special` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Tin nổi bật',
  `new_backtoNew` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Trở về tin tức',
  `new_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Tin tức khác',
  `contactinfo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Gửi liên hệ',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language_displays`
--

INSERT INTO `language_displays` (`id`, `Homepage_productsingtaoTile`, `Homeppage_discovery`, `Homepage_videoSpecial`, `Homepage_newEvent`, `btnSeeMore`, `btnSeeDetail`, `carrer_PositionName`, `carrer_Quatinity`, `carrer_WorkLocation`, `carrer_ApplicationDealine`, `new_lasted`, `new_Special`, `new_backtoNew`, `new_other`, `contactinfo`, `created_at`, `updated_at`) VALUES
(2, 'CÁC SẢN PHẨM CỦA TSINGTAO', 'Khám phá thương hiệu', 'VIDEO NỔI BẬT', 'TIN TỨC & SỰ KIỆN', 'Xem thêm', 'Xem chi tiết', 'Vị trí tuyển dụng', 'Số lượng', 'Nơi làm việc', 'Hạn nộp', 'Tin mới nhất', 'Tin nổi bật', 'Trở về tin tức', 'Tin tức khác', 'Gửi liên hệ', '2020-10-19 19:58:00', '2020-10-19 20:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(2, 'mainMenu', '2020-09-29 01:42:58', '2020-09-29 01:42:58');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-09-27 19:48:05', '2020-09-27 19:48:05', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-09-27 19:48:05', '2020-09-27 19:48:05', 'voyager.hooks', NULL),
(12, 1, 'HomePage - Banner', '', '_self', NULL, NULL, NULL, 15, '2020-09-27 20:39:36', '2020-09-27 20:39:36', 'voyager.banner-home-pages.index', NULL),
(13, 1, 'Home Page Ichis', '', '_self', NULL, NULL, NULL, 16, '2020-09-28 01:48:40', '2020-09-28 01:48:40', 'voyager.home-page-ichis.index', NULL),
(14, 1, 'Home Page Snack Ichis', '', '_self', NULL, NULL, NULL, 17, '2020-09-28 02:01:33', '2020-09-28 02:01:33', 'voyager.home-page-snack-ichis.index', NULL),
(15, 1, 'Home Page Kid Ichis', '', '_self', NULL, NULL, NULL, 18, '2020-09-28 02:18:34', '2020-09-28 02:18:34', 'voyager.home-page-kid-ichis.index', NULL),
(16, 1, 'Infomation Companies', '', '_self', NULL, NULL, NULL, 19, '2020-09-28 02:30:14', '2020-09-28 02:30:14', 'voyager.infomation-companies.index', NULL),
(17, 1, 'Aboutus Pages', '', '_self', NULL, NULL, NULL, 20, '2020-09-28 02:37:11', '2020-09-28 02:37:11', 'voyager.aboutus-pages.index', NULL),
(18, 1, 'Quality Liceneces', '', '_self', NULL, NULL, NULL, 21, '2020-09-28 02:37:35', '2020-09-28 02:37:35', 'voyager.quality-liceneces.index', NULL),
(19, 1, 'Number Impresses', '', '_self', NULL, NULL, NULL, 22, '2020-09-28 02:37:57', '2020-09-28 02:37:57', 'voyager.number-impresses.index', NULL),
(20, 1, 'Home Page Kid Ichi Crankers', '', '_self', NULL, NULL, NULL, 23, '2020-09-28 03:18:06', '2020-09-28 03:18:06', 'voyager.home-page-kid-ichi-crankers.index', NULL),
(21, 1, 'Step Items', '', '_self', NULL, NULL, NULL, 24, '2020-09-28 18:36:13', '2020-09-28 18:36:13', 'voyager.step-items.index', NULL),
(22, 1, 'Carrer Object Items', '', '_self', NULL, NULL, NULL, 25, '2020-09-28 18:52:45', '2020-09-28 18:52:45', 'voyager.carrer-object-items.index', NULL),
(23, 1, 'Contact Itemms', '', '_self', NULL, NULL, NULL, 26, '2020-09-28 19:54:04', '2020-09-28 19:54:04', 'voyager.contact-itemms.index', NULL),
(24, 1, 'Product Categories', '', '_self', NULL, NULL, NULL, 27, '2020-09-28 21:44:10', '2020-09-28 21:44:10', 'voyager.product-categories.index', NULL),
(25, 1, 'Product Category Images', '', '_self', NULL, NULL, NULL, 28, '2020-09-28 21:59:29', '2020-09-28 21:59:29', 'voyager.product-category-images.index', NULL),
(26, 1, 'Products', '', '_self', NULL, NULL, NULL, 29, '2020-09-28 22:07:51', '2020-09-28 22:07:51', 'voyager.products.index', NULL),
(27, 2, 'Trang chủ', '/', '_self', NULL, '#000000', NULL, 1, '2020-09-29 01:43:35', '2020-09-29 01:45:53', NULL, ''),
(28, 2, 'Giới thiệu', '/gioi-thieu', '_self', NULL, '#000000', NULL, 2, '2020-09-29 01:44:00', '2020-09-29 01:45:59', NULL, ''),
(29, 2, 'Phát triển<br/>bền vững', '/phat-trien-ben-vung', '_self', NULL, '#000000', NULL, 3, '2020-09-29 01:44:16', '2020-10-19 20:19:58', NULL, ''),
(30, 2, 'Cơ hội<br />nghề nghiệp', '/tuyen-dung', '_self', NULL, '#000000', NULL, 4, '2020-09-29 01:44:32', '2020-10-19 20:18:50', NULL, ''),
(31, 2, 'Liên hệ', '/lien-he', '_self', NULL, '#000000', NULL, 6, '2020-09-29 01:44:52', '2020-10-19 20:19:38', NULL, ''),
(32, 2, 'Tập đoàn Tsingtao', '/gioi-thieu#tsingtao', '_self', NULL, '#000000', 28, 1, '2020-09-29 01:45:13', '2020-10-19 20:15:40', NULL, ''),
(33, 2, 'Tsingtao Việt Nam', '/gioi-thieu#tsingtao-vietnam\"', '_self', NULL, '#000000', 28, 2, '2020-09-29 01:45:36', '2020-10-19 20:16:13', NULL, ''),
(34, 1, 'Introduction Object Pages', '', '_self', NULL, NULL, NULL, 30, '2020-10-18 21:07:02', '2020-10-18 21:07:02', 'voyager.introduction-object-pages.index', NULL),
(35, 1, 'News', '', '_self', NULL, NULL, NULL, 31, '2020-10-18 21:45:46', '2020-10-18 21:45:46', 'voyager.news.index', NULL),
(36, 1, 'Developer Items', '', '_self', NULL, NULL, NULL, 32, '2020-10-18 23:53:32', '2020-10-18 23:53:32', 'voyager.developer-items.index', NULL),
(37, 1, 'Developer Pages', '', '_self', NULL, NULL, NULL, 33, '2020-10-18 23:54:10', '2020-10-18 23:54:10', 'voyager.developer-pages.index', NULL),
(38, 1, 'Carrer Object Pages', '', '_self', NULL, NULL, NULL, 34, '2020-10-19 00:23:06', '2020-10-19 00:23:06', 'voyager.carrer-object-pages.index', NULL),
(39, 1, 'Slide Banners', '', '_self', NULL, NULL, NULL, 35, '2020-10-19 19:10:10', '2020-10-19 19:10:10', 'voyager.slide-banners.index', NULL),
(40, 1, 'Language Displays', '', '_self', NULL, NULL, NULL, 36, '2020-10-19 19:57:36', '2020-10-19 19:57:36', 'voyager.language-displays.index', NULL),
(41, 1, 'Contact Infos', '', '_self', NULL, NULL, NULL, 37, '2020-10-19 20:02:33', '2020-10-19 20:02:33', 'voyager.contact-infos.index', NULL),
(42, 2, 'Tầm nhìn, Sứ mệnh và Giá trị', '/gioi-thieu#su-menh', '_self', NULL, '#000000', 28, 3, '2020-10-19 20:16:53', '2020-10-19 20:17:02', NULL, ''),
(43, 2, 'Tin tức', '/tin-tuc', '_self', NULL, '#000000', NULL, 5, '2020-10-19 20:19:33', '2020-10-19 20:19:38', NULL, ''),
(44, 1, 'Banner Items', '', '_self', NULL, NULL, NULL, 38, '2020-10-19 20:38:03', '2020-10-19 20:38:03', 'voyager.banner-items.index', NULL),
(45, 1, 'Footer Infos', '', '_self', NULL, NULL, NULL, 39, '2020-10-20 00:27:14', '2020-10-20 00:27:14', 'voyager.footer-infos.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_01_01_000000_add_voyager_user_fields', 1),
(3, '2016_01_01_000000_create_data_types_table', 1),
(4, '2016_05_19_173453_create_menu_table', 1),
(5, '2016_10_21_190000_create_roles_table', 1),
(6, '2016_10_21_190000_create_settings_table', 1),
(7, '2016_11_30_135954_create_permission_table', 1),
(8, '2016_11_30_141208_create_permission_role_table', 1),
(9, '2016_12_26_201236_data_types__add__server_side', 1),
(10, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(11, '2017_01_14_005015_create_translations_table', 1),
(12, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(13, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(14, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(15, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(16, '2017_08_05_000000_add_group_to_settings_table', 1),
(17, '2017_11_26_013050_add_user_role_relationship', 1),
(18, '2017_11_26_015000_create_user_roles_table', 1),
(19, '2018_03_11_000000_add_user_settings', 1),
(20, '2018_03_14_000000_add_details_to_data_types_table', 1),
(21, '2018_03_16_000000_make_settings_value_nullable', 1),
(22, '2019_08_19_000000_create_failed_jobs_table', 1),
(23, '2020_07_27_074403_create_slide_banners_table', 1),
(24, '2020_07_27_074427_create_banner_pages_table', 1),
(25, '2020_07_27_074449_create_seo_pages_table', 1),
(26, '2020_07_27_074523_create_infomation_companies_table', 1),
(27, '2020_09_28_032233_create_banner_home_pages_table', 2),
(28, '2020_09_28_035309_create_home_page_ichis_table', 3),
(29, '2020_09_28_085741_create_home_page_snack_ichis_table', 4),
(30, '2020_09_28_091142_create_home_page_kid_ichis_table', 5),
(31, '2020_09_28_092237_create_home_page_kid_ichi_crankers_table', 6),
(32, '2020_09_28_093225_create_aboutus_pages_table', 6),
(33, '2020_09_28_093416_create_quality_liceneces_table', 6),
(34, '2020_09_28_093532_create_number_impresses_table', 6),
(35, '2020_09_28_103337_create_step_items_table', 7),
(36, '2020_09_29_014608_create_carrer_object_items_table', 8),
(37, '2020_09_29_021445_create_apply_job_carrers_table', 9),
(38, '2020_09_29_024458_create_contact_itemms_table', 10),
(39, '2020_09_29_043807_create_product_categories_table', 11),
(40, '2020_09_29_045716_create_product_category_images_table', 12),
(41, '2020_09_29_050249_create_products_table', 13),
(42, '2020_09_30_080844_create_banner_items_table', 14),
(43, '2020_10_01_002536_create_contact_requests_table', 15),
(44, '2020_10_19_034842_create_introduction_object_pages_table', 16),
(45, '2020_10_19_042534_create_news_table', 17),
(46, '2020_10_19_064924_create_developer_pages_table', 18),
(47, '2020_10_19_065111_create_developer_items_table', 18),
(48, '2020_10_19_071845_create_carrer_object_pages_table', 19),
(52, '2020_10_20_021612_create_language_displays_table', 20),
(53, '2020_10_20_022833_create_contact_infos_table', 20),
(54, '2020_10_20_072430_create_footer_infos_table', 21);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priorites` int(11) NOT NULL DEFAULT 1,
  `isActive` tinyint(1) NOT NULL DEFAULT 1,
  `isSpecial` tinyint(1) NOT NULL DEFAULT 0,
  `keyWord` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkShare` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `backgroundImage` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleSeo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageShare` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `slug`, `priorites`, `isActive`, `isSpecial`, `keyWord`, `linkShare`, `shortDescription`, `backgroundImage`, `titleSeo`, `imageShare`, `created_at`, `updated_at`) VALUES
(1, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<p>cap nhat</p>', '/tin-tuc-1', 1, 1, 1, '1', 'news\\October2020\\KOX1i5v0DzP9GQNzrFD3.jpg', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem', 'news\\October2020\\oOIRNSC2LfVzM8MPuEuL.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\cjxV60uNiI90zh68iYHH.jpg', '2020-10-18 23:17:07', '2020-10-18 23:17:07'),
(2, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-2', 1, 1, 0, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:29', '2020-10-18 23:25:29'),
(3, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"/images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-3', 1, 1, 1, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:00', '2020-10-18 23:45:42'),
(4, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-4', 1, 1, 1, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:29', '2020-10-18 23:25:29'),
(5, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-5', 1, 1, 1, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:29', '2020-10-18 23:25:29'),
(6, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-6', 1, 1, 1, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:29', '2020-10-18 23:25:29'),
(7, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-7', 1, 1, 0, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:29', '2020-10-18 23:25:29'),
(8, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-8', 1, 1, 0, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:29', '2020-10-18 23:25:29'),
(9, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-9', 1, 1, 0, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:29', '2020-10-18 23:25:29'),
(10, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '<div class=\"col-sm-12 col-12\">\r\n<h2>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<figure><img class=\"w-100 size-full wp-image\" src=\"images/news-detail.jpg\" alt=\"\" /></figure>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 'tin-tuc-10', 1, 1, 0, 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\C3a9mnxyE0IljZhCpDdk.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ...', 'news\\October2020\\fkGVny4DJQzCJwXLUxQZ.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', 'news\\October2020\\UMh8vrzypqR9nBaizQhw.jpg', '2020-10-18 23:25:29', '2020-10-18 23:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `number_impresses`
--

CREATE TABLE `number_impresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `priorites` int(11) DEFAULT 0,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `number_impresses`
--

INSERT INTO `number_impresses` (`id`, `priorites`, `title`, `number`, `created_at`, `updated_at`) VALUES
(1, 1, 'Nhà phân phối trên toàn quốc', 150, '2020-09-28 03:04:45', '2020-09-28 03:04:45'),
(2, 1, 'Nhà phân phối trên toàn quốc', 150, '2020-09-28 03:04:45', '2020-09-28 03:04:45'),
(3, 1, 'Nhà phân phối trên toàn quốc', 150, '2020-09-28 03:04:45', '2020-09-28 03:04:45'),
(4, 1, 'Nhà phân phối trên toàn quốc', 150, '2020-09-28 03:04:45', '2020-09-28 03:04:45'),
(5, 1, 'Nhà phân phối trên toàn quốc', 150, '2020-09-28 03:04:45', '2020-09-28 03:04:45');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(2, 'browse_bread', NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(3, 'browse_database', NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(4, 'browse_media', NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(5, 'browse_compass', NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(6, 'browse_menus', 'menus', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(7, 'read_menus', 'menus', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(8, 'edit_menus', 'menus', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(9, 'add_menus', 'menus', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(10, 'delete_menus', 'menus', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(11, 'browse_roles', 'roles', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(12, 'read_roles', 'roles', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(13, 'edit_roles', 'roles', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(14, 'add_roles', 'roles', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(15, 'delete_roles', 'roles', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(16, 'browse_users', 'users', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(17, 'read_users', 'users', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(18, 'edit_users', 'users', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(19, 'add_users', 'users', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(20, 'delete_users', 'users', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(21, 'browse_settings', 'settings', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(22, 'read_settings', 'settings', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(23, 'edit_settings', 'settings', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(24, 'add_settings', 'settings', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(25, 'delete_settings', 'settings', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(26, 'browse_hooks', NULL, '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(27, 'browse_banner_home_pages', 'banner_home_pages', '2020-09-27 20:39:36', '2020-09-27 20:39:36'),
(28, 'read_banner_home_pages', 'banner_home_pages', '2020-09-27 20:39:36', '2020-09-27 20:39:36'),
(29, 'edit_banner_home_pages', 'banner_home_pages', '2020-09-27 20:39:36', '2020-09-27 20:39:36'),
(30, 'add_banner_home_pages', 'banner_home_pages', '2020-09-27 20:39:36', '2020-09-27 20:39:36'),
(31, 'delete_banner_home_pages', 'banner_home_pages', '2020-09-27 20:39:36', '2020-09-27 20:39:36'),
(32, 'browse_home_page_ichis', 'home_page_ichis', '2020-09-28 01:48:40', '2020-09-28 01:48:40'),
(33, 'read_home_page_ichis', 'home_page_ichis', '2020-09-28 01:48:40', '2020-09-28 01:48:40'),
(34, 'edit_home_page_ichis', 'home_page_ichis', '2020-09-28 01:48:40', '2020-09-28 01:48:40'),
(35, 'add_home_page_ichis', 'home_page_ichis', '2020-09-28 01:48:40', '2020-09-28 01:48:40'),
(36, 'delete_home_page_ichis', 'home_page_ichis', '2020-09-28 01:48:40', '2020-09-28 01:48:40'),
(37, 'browse_home_page_snack_ichis', 'home_page_snack_ichis', '2020-09-28 02:01:33', '2020-09-28 02:01:33'),
(38, 'read_home_page_snack_ichis', 'home_page_snack_ichis', '2020-09-28 02:01:33', '2020-09-28 02:01:33'),
(39, 'edit_home_page_snack_ichis', 'home_page_snack_ichis', '2020-09-28 02:01:33', '2020-09-28 02:01:33'),
(40, 'add_home_page_snack_ichis', 'home_page_snack_ichis', '2020-09-28 02:01:33', '2020-09-28 02:01:33'),
(41, 'delete_home_page_snack_ichis', 'home_page_snack_ichis', '2020-09-28 02:01:33', '2020-09-28 02:01:33'),
(42, 'browse_home_page_kid_ichis', 'home_page_kid_ichis', '2020-09-28 02:18:34', '2020-09-28 02:18:34'),
(43, 'read_home_page_kid_ichis', 'home_page_kid_ichis', '2020-09-28 02:18:34', '2020-09-28 02:18:34'),
(44, 'edit_home_page_kid_ichis', 'home_page_kid_ichis', '2020-09-28 02:18:34', '2020-09-28 02:18:34'),
(45, 'add_home_page_kid_ichis', 'home_page_kid_ichis', '2020-09-28 02:18:34', '2020-09-28 02:18:34'),
(46, 'delete_home_page_kid_ichis', 'home_page_kid_ichis', '2020-09-28 02:18:34', '2020-09-28 02:18:34'),
(47, 'browse_infomation_companies', 'infomation_companies', '2020-09-28 02:30:14', '2020-09-28 02:30:14'),
(48, 'read_infomation_companies', 'infomation_companies', '2020-09-28 02:30:14', '2020-09-28 02:30:14'),
(49, 'edit_infomation_companies', 'infomation_companies', '2020-09-28 02:30:14', '2020-09-28 02:30:14'),
(50, 'add_infomation_companies', 'infomation_companies', '2020-09-28 02:30:14', '2020-09-28 02:30:14'),
(51, 'delete_infomation_companies', 'infomation_companies', '2020-09-28 02:30:14', '2020-09-28 02:30:14'),
(52, 'browse_aboutus_pages', 'aboutus_pages', '2020-09-28 02:37:11', '2020-09-28 02:37:11'),
(53, 'read_aboutus_pages', 'aboutus_pages', '2020-09-28 02:37:11', '2020-09-28 02:37:11'),
(54, 'edit_aboutus_pages', 'aboutus_pages', '2020-09-28 02:37:11', '2020-09-28 02:37:11'),
(55, 'add_aboutus_pages', 'aboutus_pages', '2020-09-28 02:37:11', '2020-09-28 02:37:11'),
(56, 'delete_aboutus_pages', 'aboutus_pages', '2020-09-28 02:37:11', '2020-09-28 02:37:11'),
(57, 'browse_quality_liceneces', 'quality_liceneces', '2020-09-28 02:37:35', '2020-09-28 02:37:35'),
(58, 'read_quality_liceneces', 'quality_liceneces', '2020-09-28 02:37:35', '2020-09-28 02:37:35'),
(59, 'edit_quality_liceneces', 'quality_liceneces', '2020-09-28 02:37:35', '2020-09-28 02:37:35'),
(60, 'add_quality_liceneces', 'quality_liceneces', '2020-09-28 02:37:35', '2020-09-28 02:37:35'),
(61, 'delete_quality_liceneces', 'quality_liceneces', '2020-09-28 02:37:35', '2020-09-28 02:37:35'),
(62, 'browse_number_impresses', 'number_impresses', '2020-09-28 02:37:57', '2020-09-28 02:37:57'),
(63, 'read_number_impresses', 'number_impresses', '2020-09-28 02:37:57', '2020-09-28 02:37:57'),
(64, 'edit_number_impresses', 'number_impresses', '2020-09-28 02:37:57', '2020-09-28 02:37:57'),
(65, 'add_number_impresses', 'number_impresses', '2020-09-28 02:37:57', '2020-09-28 02:37:57'),
(66, 'delete_number_impresses', 'number_impresses', '2020-09-28 02:37:57', '2020-09-28 02:37:57'),
(67, 'browse_home_page_kid_ichi_crankers', 'home_page_kid_ichi_crankers', '2020-09-28 03:18:06', '2020-09-28 03:18:06'),
(68, 'read_home_page_kid_ichi_crankers', 'home_page_kid_ichi_crankers', '2020-09-28 03:18:06', '2020-09-28 03:18:06'),
(69, 'edit_home_page_kid_ichi_crankers', 'home_page_kid_ichi_crankers', '2020-09-28 03:18:06', '2020-09-28 03:18:06'),
(70, 'add_home_page_kid_ichi_crankers', 'home_page_kid_ichi_crankers', '2020-09-28 03:18:06', '2020-09-28 03:18:06'),
(71, 'delete_home_page_kid_ichi_crankers', 'home_page_kid_ichi_crankers', '2020-09-28 03:18:06', '2020-09-28 03:18:06'),
(72, 'browse_step_items', 'step_items', '2020-09-28 18:36:13', '2020-09-28 18:36:13'),
(73, 'read_step_items', 'step_items', '2020-09-28 18:36:13', '2020-09-28 18:36:13'),
(74, 'edit_step_items', 'step_items', '2020-09-28 18:36:13', '2020-09-28 18:36:13'),
(75, 'add_step_items', 'step_items', '2020-09-28 18:36:13', '2020-09-28 18:36:13'),
(76, 'delete_step_items', 'step_items', '2020-09-28 18:36:13', '2020-09-28 18:36:13'),
(77, 'browse_carrer_object_items', 'carrer_object_items', '2020-09-28 18:52:45', '2020-09-28 18:52:45'),
(78, 'read_carrer_object_items', 'carrer_object_items', '2020-09-28 18:52:45', '2020-09-28 18:52:45'),
(79, 'edit_carrer_object_items', 'carrer_object_items', '2020-09-28 18:52:45', '2020-09-28 18:52:45'),
(80, 'add_carrer_object_items', 'carrer_object_items', '2020-09-28 18:52:45', '2020-09-28 18:52:45'),
(81, 'delete_carrer_object_items', 'carrer_object_items', '2020-09-28 18:52:45', '2020-09-28 18:52:45'),
(82, 'browse_contact_itemms', 'contact_itemms', '2020-09-28 19:54:04', '2020-09-28 19:54:04'),
(83, 'read_contact_itemms', 'contact_itemms', '2020-09-28 19:54:04', '2020-09-28 19:54:04'),
(84, 'edit_contact_itemms', 'contact_itemms', '2020-09-28 19:54:04', '2020-09-28 19:54:04'),
(85, 'add_contact_itemms', 'contact_itemms', '2020-09-28 19:54:04', '2020-09-28 19:54:04'),
(86, 'delete_contact_itemms', 'contact_itemms', '2020-09-28 19:54:04', '2020-09-28 19:54:04'),
(87, 'browse_product_categories', 'product_categories', '2020-09-28 21:44:10', '2020-09-28 21:44:10'),
(88, 'read_product_categories', 'product_categories', '2020-09-28 21:44:10', '2020-09-28 21:44:10'),
(89, 'edit_product_categories', 'product_categories', '2020-09-28 21:44:10', '2020-09-28 21:44:10'),
(90, 'add_product_categories', 'product_categories', '2020-09-28 21:44:10', '2020-09-28 21:44:10'),
(91, 'delete_product_categories', 'product_categories', '2020-09-28 21:44:10', '2020-09-28 21:44:10'),
(92, 'browse_product_category_images', 'product_category_images', '2020-09-28 21:59:29', '2020-09-28 21:59:29'),
(93, 'read_product_category_images', 'product_category_images', '2020-09-28 21:59:29', '2020-09-28 21:59:29'),
(94, 'edit_product_category_images', 'product_category_images', '2020-09-28 21:59:29', '2020-09-28 21:59:29'),
(95, 'add_product_category_images', 'product_category_images', '2020-09-28 21:59:29', '2020-09-28 21:59:29'),
(96, 'delete_product_category_images', 'product_category_images', '2020-09-28 21:59:29', '2020-09-28 21:59:29'),
(97, 'browse_products', 'products', '2020-09-28 22:07:51', '2020-09-28 22:07:51'),
(98, 'read_products', 'products', '2020-09-28 22:07:51', '2020-09-28 22:07:51'),
(99, 'edit_products', 'products', '2020-09-28 22:07:51', '2020-09-28 22:07:51'),
(100, 'add_products', 'products', '2020-09-28 22:07:51', '2020-09-28 22:07:51'),
(101, 'delete_products', 'products', '2020-09-28 22:07:51', '2020-09-28 22:07:51'),
(102, 'browse_introduction_object_pages', 'introduction_object_pages', '2020-10-18 21:07:02', '2020-10-18 21:07:02'),
(103, 'read_introduction_object_pages', 'introduction_object_pages', '2020-10-18 21:07:02', '2020-10-18 21:07:02'),
(104, 'edit_introduction_object_pages', 'introduction_object_pages', '2020-10-18 21:07:02', '2020-10-18 21:07:02'),
(105, 'add_introduction_object_pages', 'introduction_object_pages', '2020-10-18 21:07:02', '2020-10-18 21:07:02'),
(106, 'delete_introduction_object_pages', 'introduction_object_pages', '2020-10-18 21:07:02', '2020-10-18 21:07:02'),
(107, 'browse_news', 'news', '2020-10-18 21:45:46', '2020-10-18 21:45:46'),
(108, 'read_news', 'news', '2020-10-18 21:45:46', '2020-10-18 21:45:46'),
(109, 'edit_news', 'news', '2020-10-18 21:45:46', '2020-10-18 21:45:46'),
(110, 'add_news', 'news', '2020-10-18 21:45:46', '2020-10-18 21:45:46'),
(111, 'delete_news', 'news', '2020-10-18 21:45:46', '2020-10-18 21:45:46'),
(112, 'browse_developer_items', 'developer_items', '2020-10-18 23:53:32', '2020-10-18 23:53:32'),
(113, 'read_developer_items', 'developer_items', '2020-10-18 23:53:32', '2020-10-18 23:53:32'),
(114, 'edit_developer_items', 'developer_items', '2020-10-18 23:53:32', '2020-10-18 23:53:32'),
(115, 'add_developer_items', 'developer_items', '2020-10-18 23:53:32', '2020-10-18 23:53:32'),
(116, 'delete_developer_items', 'developer_items', '2020-10-18 23:53:32', '2020-10-18 23:53:32'),
(117, 'browse_developer_pages', 'developer_pages', '2020-10-18 23:54:10', '2020-10-18 23:54:10'),
(118, 'read_developer_pages', 'developer_pages', '2020-10-18 23:54:10', '2020-10-18 23:54:10'),
(119, 'edit_developer_pages', 'developer_pages', '2020-10-18 23:54:10', '2020-10-18 23:54:10'),
(120, 'add_developer_pages', 'developer_pages', '2020-10-18 23:54:10', '2020-10-18 23:54:10'),
(121, 'delete_developer_pages', 'developer_pages', '2020-10-18 23:54:10', '2020-10-18 23:54:10'),
(122, 'browse_carrer_object_pages', 'carrer_object_pages', '2020-10-19 00:23:06', '2020-10-19 00:23:06'),
(123, 'read_carrer_object_pages', 'carrer_object_pages', '2020-10-19 00:23:06', '2020-10-19 00:23:06'),
(124, 'edit_carrer_object_pages', 'carrer_object_pages', '2020-10-19 00:23:06', '2020-10-19 00:23:06'),
(125, 'add_carrer_object_pages', 'carrer_object_pages', '2020-10-19 00:23:06', '2020-10-19 00:23:06'),
(126, 'delete_carrer_object_pages', 'carrer_object_pages', '2020-10-19 00:23:06', '2020-10-19 00:23:06'),
(127, 'browse_slide_banners', 'slide_banners', '2020-10-19 19:10:10', '2020-10-19 19:10:10'),
(128, 'read_slide_banners', 'slide_banners', '2020-10-19 19:10:10', '2020-10-19 19:10:10'),
(129, 'edit_slide_banners', 'slide_banners', '2020-10-19 19:10:10', '2020-10-19 19:10:10'),
(130, 'add_slide_banners', 'slide_banners', '2020-10-19 19:10:10', '2020-10-19 19:10:10'),
(131, 'delete_slide_banners', 'slide_banners', '2020-10-19 19:10:10', '2020-10-19 19:10:10'),
(132, 'browse_language_displays', 'language_displays', '2020-10-19 19:57:36', '2020-10-19 19:57:36'),
(133, 'read_language_displays', 'language_displays', '2020-10-19 19:57:36', '2020-10-19 19:57:36'),
(134, 'edit_language_displays', 'language_displays', '2020-10-19 19:57:36', '2020-10-19 19:57:36'),
(135, 'add_language_displays', 'language_displays', '2020-10-19 19:57:36', '2020-10-19 19:57:36'),
(136, 'delete_language_displays', 'language_displays', '2020-10-19 19:57:36', '2020-10-19 19:57:36'),
(137, 'browse_contact_infos', 'contact_infos', '2020-10-19 20:02:33', '2020-10-19 20:02:33'),
(138, 'read_contact_infos', 'contact_infos', '2020-10-19 20:02:33', '2020-10-19 20:02:33'),
(139, 'edit_contact_infos', 'contact_infos', '2020-10-19 20:02:33', '2020-10-19 20:02:33'),
(140, 'add_contact_infos', 'contact_infos', '2020-10-19 20:02:33', '2020-10-19 20:02:33'),
(141, 'delete_contact_infos', 'contact_infos', '2020-10-19 20:02:33', '2020-10-19 20:02:33'),
(142, 'browse_banner_items', 'banner_items', '2020-10-19 20:38:03', '2020-10-19 20:38:03'),
(143, 'read_banner_items', 'banner_items', '2020-10-19 20:38:03', '2020-10-19 20:38:03'),
(144, 'edit_banner_items', 'banner_items', '2020-10-19 20:38:03', '2020-10-19 20:38:03'),
(145, 'add_banner_items', 'banner_items', '2020-10-19 20:38:03', '2020-10-19 20:38:03'),
(146, 'delete_banner_items', 'banner_items', '2020-10-19 20:38:03', '2020-10-19 20:38:03'),
(147, 'browse_footer_infos', 'footer_infos', '2020-10-20 00:27:14', '2020-10-20 00:27:14'),
(148, 'read_footer_infos', 'footer_infos', '2020-10-20 00:27:14', '2020-10-20 00:27:14'),
(149, 'edit_footer_infos', 'footer_infos', '2020-10-20 00:27:14', '2020-10-20 00:27:14'),
(150, 'add_footer_infos', 'footer_infos', '2020-10-20 00:27:14', '2020-10-20 00:27:14'),
(151, 'delete_footer_infos', 'footer_infos', '2020-10-20 00:27:14', '2020-10-20 00:27:14');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `backgroundImage` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageContent` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ImageBanner` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shortDescriptionBanner` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescriptionTitle` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleSeo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descriptionSEO` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkImageShare` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescriptionContent` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `slug`, `backgroundImage`, `shortDescription`, `imageContent`, `ImageBanner`, `priority`, `created_at`, `updated_at`, `shortDescriptionBanner`, `content`, `shortDescriptionTitle`, `titleSeo`, `descriptionSEO`, `linkImageShare`, `shortDescriptionContent`) VALUES
(4, 'TSINGTAO PURE DRAFT 1		', 'tsingtao-pure-draft', 'products\\October2020\\NmMzcvufdj5sOAbw5QQT.png', 'TSINGTAO PURE DRAFT	', 'products\\October2020\\RW13ny92Qgk6vr7Y1nfm.png', 'products\\October2020\\e4kGehGP0sB9I5CDntUa.jpg', 1, '2020-10-19 01:31:47', '2020-10-19 01:31:47', '<ul>\r\n<li>Hương vị thanh nh&atilde; h&ograve;a quyện từ nguy&ecirc;n liệu ch&acirc;u &Acirc;u, &Uacute;c chưng cất nước tinh khiết m&ugrave;a xu&acirc;n Lao Sơn Tuyền.</li>\r\n<li>Hương thơm đặc trưng như bia Draft trong Tap Beerpub</li>\r\n</ul>', '<p>Th&agrave;nh phần: Nước, Đại mạch, Ngũ cốc, Hoa bia<br />Thể t&iacute;ch: 330ml<br />Độ cồn: 4.3%<br />Thời hạn sử dụng: 12 th&aacute;ng kể từ ng&agrave;y sản xuất.</p>', 'Hương Bia Tươi Mới', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'products\\October2020\\thoLrTZpPMmc3OYfAToh.jpg', NULL),
(5, 'TSINGTAO PURE DRAFT								', 'tsingtao-pure-draft1', 'products\\October2020\\NmMzcvufdj5sOAbw5QQT.png', 'TSINGTAO PURE DRAFT	', 'products\\October2020\\RW13ny92Qgk6vr7Y1nfm.png', 'products\\October2020\\e4kGehGP0sB9I5CDntUa.jpg', 1, '2020-10-19 01:31:47', '2020-10-19 01:31:47', '<ul>\r\n<li>Hương vị thanh nh&atilde; h&ograve;a quyện từ nguy&ecirc;n liệu ch&acirc;u &Acirc;u, &Uacute;c chưng cất nước tinh khiết m&ugrave;a xu&acirc;n Lao Sơn Tuyền.</li>\r\n<li>Hương thơm đặc trưng như bia Draft trong Tap Beerpub</li>\r\n</ul>', '<p>Th&agrave;nh phần: Nước, Đại mạch, Ngũ cốc, Hoa bia<br />Thể t&iacute;ch: 330ml<br />Độ cồn: 4.3%<br />Thời hạn sử dụng: 12 th&aacute;ng kể từ ng&agrave;y sản xuất.</p>', 'Hương Bia Tươi Mới', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'products\\October2020\\thoLrTZpPMmc3OYfAToh.jpg', NULL),
(6, 'TSINGTAO PURE DRAFT								', 'tsingtao-pure-draft2', 'products\\October2020\\NmMzcvufdj5sOAbw5QQT.png', 'TSINGTAO PURE DRAFT	', 'products\\October2020\\RW13ny92Qgk6vr7Y1nfm.png', 'products\\October2020\\e4kGehGP0sB9I5CDntUa.jpg', 1, '2020-10-19 01:31:47', '2020-10-19 01:31:47', '<ul>\r\n<li>Hương vị thanh nh&atilde; h&ograve;a quyện từ nguy&ecirc;n liệu ch&acirc;u &Acirc;u, &Uacute;c chưng cất nước tinh khiết m&ugrave;a xu&acirc;n Lao Sơn Tuyền.</li>\r\n<li>Hương thơm đặc trưng như bia Draft trong Tap Beerpub</li>\r\n</ul>', '<p>Th&agrave;nh phần: Nước, Đại mạch, Ngũ cốc, Hoa bia<br />Thể t&iacute;ch: 330ml<br />Độ cồn: 4.3%<br />Thời hạn sử dụng: 12 th&aacute;ng kể từ ng&agrave;y sản xuất.</p>', 'Hương Bia Tươi Mới', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'products\\October2020\\thoLrTZpPMmc3OYfAToh.jpg', NULL),
(7, 'TSINGTAO PURE DRAFT								', 'tsingtao-pure-draft3', 'products\\October2020\\NmMzcvufdj5sOAbw5QQT.png', 'TSINGTAO PURE DRAFT	', 'products\\October2020\\RW13ny92Qgk6vr7Y1nfm.png', 'products\\October2020\\e4kGehGP0sB9I5CDntUa.jpg', 1, '2020-10-19 01:31:47', '2020-10-19 01:31:47', '<ul>\r\n<li>Hương vị thanh nh&atilde; h&ograve;a quyện từ nguy&ecirc;n liệu ch&acirc;u &Acirc;u, &Uacute;c chưng cất nước tinh khiết m&ugrave;a xu&acirc;n Lao Sơn Tuyền.</li>\r\n<li>Hương thơm đặc trưng như bia Draft trong Tap Beerpub</li>\r\n</ul>', '<p>Th&agrave;nh phần: Nước, Đại mạch, Ngũ cốc, Hoa bia<br />Thể t&iacute;ch: 330ml<br />Độ cồn: 4.3%<br />Thời hạn sử dụng: 12 th&aacute;ng kể từ ng&agrave;y sản xuất.</p>', 'Hương Bia Tươi Mới', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'products\\October2020\\thoLrTZpPMmc3OYfAToh.jpg', NULL),
(8, 'TSINGTAO PURE DRAFT								', 'tsingtao-pure-draft4', 'products\\October2020\\NmMzcvufdj5sOAbw5QQT.png', 'TSINGTAO PURE DRAFT	', 'products\\October2020\\RW13ny92Qgk6vr7Y1nfm.png', 'products\\October2020\\e4kGehGP0sB9I5CDntUa.jpg', 1, '2020-10-19 01:31:47', '2020-10-19 01:31:47', '<ul>\r\n<li>Hương vị thanh nh&atilde; h&ograve;a quyện từ nguy&ecirc;n liệu ch&acirc;u &Acirc;u, &Uacute;c chưng cất nước tinh khiết m&ugrave;a xu&acirc;n Lao Sơn Tuyền.</li>\r\n<li>Hương thơm đặc trưng như bia Draft trong Tap Beerpub</li>\r\n</ul>', '<p>Th&agrave;nh phần: Nước, Đại mạch, Ngũ cốc, Hoa bia<br />Thể t&iacute;ch: 330ml<br />Độ cồn: 4.3%<br />Thời hạn sử dụng: 12 th&aacute;ng kể từ ng&agrave;y sản xuất.</p>', 'Hương Bia Tươi Mới', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'Thành phần: Nước, Đại mạch, Ngũ cốc, Hoa bia', 'products\\October2020\\thoLrTZpPMmc3OYfAToh.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleHomePage` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descriptionHomePage` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagHomePage` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleDetail` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codeColorProductCategory` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleDisplay` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codeColor` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `backgroundImamge` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `classCode` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleSeo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descriptionSEO` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkImageShare` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ProductCategoryImage` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `title`, `titleHomePage`, `descriptionHomePage`, `imagHomePage`, `titleDetail`, `codeColorProductCategory`, `titleDisplay`, `codeColor`, `slug`, `backgroundImamge`, `shortDescription`, `weight`, `priority`, `created_at`, `updated_at`, `classCode`, `titleSeo`, `descriptionSEO`, `linkImageShare`, `keyword`, `ProductCategoryImage`) VALUES
(1, 'Bánh gạo Nhật ICHI', 'BÁNH GẠO NHẬT ICHI', '<p>								Giòn tan, ngon lành với vị Shouyu Mật ong đặc trưng. 								Thêm bất ngờ với 2 vị hoàn toàn mới: Pizza và Thịt xông khói & Phô mai							</p>', 'product-categories\\October2020\\7C1o89uPjbi0DQKMEqiT.png', 'BÁNH GẠO NHẬT ICHI VỊ SHOUYU MẬT ONG ', 'background-color: #ff7258;', 'SẢN PHẨM CỦA BÁNH GẠO ICHI2', '#e41e1f', 'banh-gao-nhat-ichi-vi-shouyu-mat-ong', 'product-categories\\September2020\\K8pYOTWC5bJbvZAeE9SX.png', '<p>Sản phẩm đặc trưng của thương hiệu b&aacute;nh gạo ICHI, được l&agrave;m từ nguy&ecirc;n liệu gạo cao cấp Japonica, kết hợp c&ugrave;ng hương vị đặc trưng của Nhật Bản (Shouyu mật ong) v&agrave; c&aacute;c hương vị mới độc đ&aacute;o (Pizza, Thịt x&ocirc;ng kh&oacute;i &amp; Ph&ocirc; mai), cho bạn trải nghiệm ẩm thực h&agrave;i h&ograve;a, độc đ&aacute;o đến từ xứ sở hoa anh đ&agrave;o.<br />Đặc biệt, Shouyu mật ong &ndash; hương vị được y&ecirc;u th&iacute;ch nhất của ICHI mang đến những chiếc b&aacute;nh gi&ograve;n m&agrave; kh&ocirc;ng cứng, vị mặn ngọt h&agrave;i h&ograve;a, ăn m&atilde;i kh&ocirc;ng ch&aacute;n. Sản phẩm được đ&oacute;ng g&oacute;i nhỏ gọn, dễ sử dụng l&agrave;m m&oacute;n ăn vặt trong ng&agrave;y, trong c&aacute;c buổi li&ecirc;n hoan, dễ d&agrave;ng mang theo trong những chuyến đi chơi xa hay l&agrave;m qu&agrave; tặng cho bạn b&egrave;, người th&acirc;n.</p>', '', 1, '2020-09-28 21:56:00', '2020-10-03 01:16:43', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Snack gạo ICHI', 'Snack Gạo Ichi ', '<p>\r\n							Kích thích khẩu vị với 5 hương vị phong phú:<br/>\r\n							- Vị Mật ong <br/>\r\n							- Vị Cay <br/>\r\n							- Vị Sô cô la<br/>\r\n							- Vị Cà ri <br/>\r\n							- Vị xốt bánh xèo Nhật<br/>\r\n						</p>', 'product-categories\\October2020\\7C1o89uPjbi0DQKMEqiT.png', 'SNACK GẠO ICHI VỊ MẬT ONG', 'background-color: #efbb5c;', 'SẢN PHẨM CỦA SNACK GẠO ICHI1', '#efbb5c', 'product_snack_mat_ong', 'product-categories\\September2020\\yj7Z7uIxXnGH3oDrNC98.png', '<p>\r\n                                    Thưởng thức bánh gạo theo cách hoàn toàn mới với snack gạo ICHI. Vẫn giữ trọn vị thơm ngon và dinh dưỡng tự nhiên của gạo Japonica, những chiếc bánh từ dòng snack gạo ICHI có kích thước nhỏ hơn, được phối trộn nhiều hương vị độc đáo, mang đến trải nghiệm ăn vặt vui hơn, phong phú hơn cho cả gia đình bạn.\r\n                                </p>\r\n                                <p>\r\n                                    Kích thích khẩu vị với 5 hương vị phong phú:<br>\r\n                                    - Vị Mật ong <br>\r\n                                    - Vị Cay <br>\r\n                                    - Vị Sô cô la<br>\r\n                                    - Vị Cà ri <br>\r\n                                    - Vị xốt bánh xèo Nhật \r\n                                </p>', NULL, 2, '2020-09-28 21:56:00', '2020-09-29 21:19:07', NULL, '', '', '', '', NULL),
(5, 'ICHI Kid', 'ICHI KID', '<p>\r\n								Bánh ăn dặm dành cho trẻ em từ 6 tháng tuổi với \r\n								hương vị nguyên bản, được sản xuất từ gạo Japonica \r\n								cao cấp có bổ sung canxi, giúp bé tập nhai, cầm \r\n								nắm và bổ sung dinh dưỡng\r\n							</p>', 'product-categories\\October2020\\7C1o89uPjbi0DQKMEqiT.png', 'ICHI Kid', 'background-color: #fdd790;;', NULL, 'product_color_kid', 'product_kid', 'product-categories\\September2020\\ZhtpJCIHX6UpcI7DdXg4.png', '<p>\r\n									Sản phẩm bánh gạo đặc biệt dành cho trẻ em từ 6 tháng tuổi.\r\n                                </p>\r\n\r\n                                <p>\r\n                                    Được sản xuất từ gạo Japonica cao cấp có bổ sung canxi, với hương\r\n									vị nguyên bản, những chiếc bánh nhỏ xinh là món ăn dặm thú vị\r\n									không những giúp bé tập cầm nắm, tập nhai mà còn bổ sung dinh\r\n									dưỡng cần thiết.\r\n                                </p>\r\n\r\n                                <p>\r\n                                    Nếu bạn muốn thưởng thức hương vị nguyên bản của bánh gạo,\r\n									hương vị thanh nhẹ &amp; giòn tan của ICHI Kid cũng sẽ khiến bạn hài\r\n									lòng.\r\n                                </p>', '29gr', 3, '2020-09-29 21:49:00', '2020-09-29 21:56:39', 'product_color_kid', '', '', '', '', NULL),
(6, 'ICHI Cracker', 'ICHI Cracker', NULL, 'product-categories\\October2020\\iQfzmjxnt4K7MDagd9Mo.png', 'ICHI Cracker', 'background-color: #fadac2;', '', 'product_color_cracker', 'product_cracker.', 'product-categories\\September2020\\X5vzuFKgrRl6rENtrxFB.png', '<p>Si&ecirc;u mỏng, si&ecirc;u gi&ograve;n, si&ecirc;u ngon, ICHI Cracker l&agrave; lựa chọn ho&agrave;n hảo cho một m&oacute;n ăn vặt vừa vui miệng, vui tai, vừa tốt cho sức khỏe.</p>\n<p>Được l&agrave;m từ nguy&ecirc;n liệu cao cấp l&agrave; gạo lứt Japonica, bổ sung m&egrave; đen v&agrave; c&aacute;c gia vị đặc biệt, những chiếc b&aacute;nh gạo được c&aacute;n thật mỏng để đạt đến độ gi&ograve;n ho&agrave;n hảo, vừa cắn đ&atilde; tan tanh t&aacute;ch trong miệng, ăn ho&agrave;i kh&ocirc;ng ng&aacute;n.</p>\n<p>H&atilde;y thưởng thức v&agrave; cảm nhận vị thơm ngọn của gạo lứt, m&egrave; đen từ nơi đầu lưỡi v&agrave; c&ugrave;ng lắng nghe vị gi&ograve;n tan tanh t&aacute;ch của b&aacute;nh gạo Nhật Ichi Cracker gạo lứt m&egrave; đen - cho bạn th&ecirc;m vui th&ecirc;m y&ecirc;u đời.</p>', '85gr', 4, '2020-09-29 22:05:00', '2020-10-03 01:43:31', 'product_color_cracker', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category_images`
--

CREATE TABLE `product_category_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `backgroundImamge` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_category_images`
--

INSERT INTO `product_category_images` (`id`, `backgroundImamge`, `weight`, `priority`, `categoryId`, `created_at`, `updated_at`) VALUES
(1, 'product-category-images\\September2020\\2OydexUk1CZqYW8fQYAE.png', '180', 1, 1, '2020-09-28 22:01:39', '2020-09-28 22:01:39'),
(2, 'product-category-images\\September2020\\EgwXhpZKIiEHzzifEsKD.png', '100 g', 2, 1, '2020-09-28 22:02:06', '2020-09-28 22:02:06'),
(3, 'product-category-images\\September2020\\YKxngwBl2xBJcyL2U61U.png', '180g', 1, 2, '2020-09-29 21:29:40', '2020-09-29 21:29:40'),
(4, 'product-category-images\\September2020\\blZXH2mWm43GeDEY6vCD.png', '28gr', 1, 5, '2020-09-29 22:03:21', '2020-09-29 22:03:21'),
(5, 'product-category-images\\September2020\\iLHI0SCkIlhK4HF3MblS.png', '85gr', NULL, 6, '2020-09-29 22:06:00', '2020-09-29 22:54:28');

-- --------------------------------------------------------

--
-- Table structure for table `quality_liceneces`
--

CREATE TABLE `quality_liceneces` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image1` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkfile` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priorites` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quality_liceneces`
--

INSERT INTO `quality_liceneces` (`id`, `image1`, `linkfile`, `slug`, `priorites`, `created_at`, `updated_at`) VALUES
(1, 'quality-liceneces\\September2020\\3o2AA5dEgvH6mxnwGd8l.jpg', '[{\"download_link\":\"quality-liceneces\\\\September2020\\\\6vULqxsrMeq1j6YP0Ztd.pdf\",\"original_name\":\"ChungNhanHACCP.pdf\"}]', NULL, 1, '2020-09-28 03:01:09', '2020-09-28 03:01:09'),
(2, 'quality-liceneces\\September2020\\3o2AA5dEgvH6mxnwGd8l.jpg', '[{\"download_link\":\"quality-liceneces\\\\September2020\\\\6vULqxsrMeq1j6YP0Ztd.pdf\",\"original_name\":\"ChungNhanHACCP.pdf\"}]', NULL, 2, '2020-09-28 03:01:09', '2020-09-28 03:01:09'),
(3, 'quality-liceneces\\September2020\\3o2AA5dEgvH6mxnwGd8l.jpg', '[{\"download_link\":\"quality-liceneces\\\\September2020\\\\6vULqxsrMeq1j6YP0Ztd.pdf\",\"original_name\":\"ChungNhanHACCP.pdf\"}]', NULL, 3, '2020-09-28 03:01:09', '2020-09-28 03:01:09'),
(4, 'quality-liceneces\\September2020\\3o2AA5dEgvH6mxnwGd8l.jpg', '[{\"download_link\":\"quality-liceneces\\\\September2020\\\\6vULqxsrMeq1j6YP0Ztd.pdf\",\"original_name\":\"ChungNhanHACCP.pdf\"}]', NULL, 4, '2020-09-28 03:01:09', '2020-09-28 03:01:09'),
(5, 'quality-liceneces\\September2020\\3o2AA5dEgvH6mxnwGd8l.jpg', '[{\"download_link\":\"quality-liceneces\\\\September2020\\\\6vULqxsrMeq1j6YP0Ztd.pdf\",\"original_name\":\"ChungNhanHACCP.pdf\"}]', NULL, 5, '2020-09-28 03:01:09', '2020-09-28 03:01:09');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-09-27 19:48:05', '2020-09-27 19:48:05'),
(2, 'user', 'Normal User', '2020-09-27 19:48:05', '2020-09-27 19:48:05');

-- --------------------------------------------------------

--
-- Table structure for table `seo_pages`
--

CREATE TABLE `seo_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `keyScreen` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `titlePage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titleSeo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `slide_banners`
--

CREATE TABLE `slide_banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `priorites` int(11) NOT NULL DEFAULT 1,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shortDescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slide_banners`
--

INSERT INTO `slide_banners` (`id`, `priorites`, `title`, `image`, `shortDescription`, `isActive`, `created_at`, `updated_at`) VALUES
(1, 1, '', 'slide-banners\\October2020\\sc2Qx31L4Cv5Zyanr5VY.jpg', '', NULL, '2020-10-19 19:11:11', '2020-10-19 19:11:11'),
(2, 2, '', 'slide-banners\\October2020\\pwq6Ok48wk7xVVZElYa9.jpg', '', NULL, '2020-10-19 19:11:30', '2020-10-19 19:11:30'),
(3, 4, '', 'slide-banners\\October2020\\u3GVNcIqQyeaNMCvUdhH.jpg', '', NULL, '2020-10-19 19:11:49', '2020-10-19 19:11:49'),
(4, 3, '', 'slide-banners\\October2020\\b4GVUaVKXJLSV6m0uja3.jpg', '', NULL, '2020-10-19 19:12:12', '2020-10-19 19:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `step_items`
--

CREATE TABLE `step_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `step_items`
--

INSERT INTO `step_items` (`id`, `title`, `content`, `avatar`, `step`, `created_at`, `updated_at`) VALUES
(1, 'SẢN XUẤT TRÊN CÔNG NGHỆ ĐỘC QUYỀN CỦA KAMEDA SEIKA – NHẬT BẢN', '<p>B&aacute;nh gạo ICHI được sản xuất tr&ecirc;n c&ocirc;ng nghệ độc quyền của Kameda Seida Nhật Bản, kh&ocirc;ng chất tạo m&agrave;u, kh&ocirc;ng chất bảo quản m&agrave; vẫn giữ được hương vị thơm ngon v&agrave; dinh dưỡng tự nhi&ecirc;n từ hạt gạo. Mọi kh&acirc;u bắt đầu từ xử l&yacute; nguy&ecirc;n liệu đến đ&oacute;ng g&oacute;i th&agrave;nh phẩm đều phải tu&acirc;n thủ nghi&ecirc;m ngặt c&aacute;c quy chuẩn, quy định về chất lượng, chỉ ti&ecirc;u an to&agrave;n thực phẩm.</p>', 'step-items\\September2020\\1q6SWuhpXcB46Lz4Yi2B.png', 2, '2020-09-28 18:37:40', '2020-09-28 18:37:40'),
(2, 'CHỌN LỰA NGUYÊN LIỆU THƯỢNG HẠNG', '<p>Hương vị thơm ngon của ICHI bắt nguồn từ nguy&ecirc;n liệu thượng hạng được chọn lựa v&agrave; kiểm tra nghi&ecirc;m ngặt.</p>\r\n<p>B&aacute;nh gạo ICHI sử dụng 100% gạo Japonica c&oacute; xuất xứ từ Nhật, cho hương vị thơm ngon chuẩn Nhật Bản. C&aacute;c th&agrave;nh phần kh&aacute;c l&agrave;m n&ecirc;n m&ugrave;i vị thơm ngon của b&aacute;nh gạo &amp; đặc trưng hương vị Nhật Bản như Shouyu (nước tương/x&igrave; dầu), mật ong, pizza, thịt x&ocirc;ng kh&oacute;i, b&aacute;nh x&egrave;o Nhật, c&agrave; ri&hellip; cũng được tuyển chọn từ nguồn nguy&ecirc;n liệu tươi ngon, cao cấp, đảm bảo chất lượng cho mọi d&ograve;ng sản phẩm.</p>', 'step-items\\September2020\\odZRj2LixigjbSNUqM81.png', 1, '2020-09-28 18:38:00', '2020-09-28 18:44:21'),
(3, 'CAM KẾT SẢN PHẨM CHẤT LƯỢNG', '<p>Thi&ecirc;n H&agrave; Kameda đặt chất lượng sản phẩm l&agrave;m cốt l&otilde;i, cam kết mang đến cho qu&yacute; kh&aacute;ch h&agrave;ng những sản phẩm chất lượng tốt nhất từ nguy&ecirc;n liệu thượng hạng, cao cấp c&ugrave;ng d&acirc;y chuyền sản xuất hiện đại nhất từ thương hiệu sản xuất b&aacute;nh gạo số 1 Nhật Bản, để mỗi chiếc b&aacute;nh l&agrave; một trải nghiệm ẩm thực độc đ&aacute;o, lu&ocirc;n khiến bạn h&agrave;i l&ograve;ng, ngon miệng v&agrave; vui th&iacute;ch.</p>', 'step-items\\September2020\\8z9NrRvgSgPRg9AABcS0.png', 3, '2020-09-28 18:38:52', '2020-09-28 18:38:52');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_rows', 'display_name', 22, 'jp', 'Id', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(2, 'data_rows', 'display_name', 23, 'jp', 'Image1', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(3, 'data_rows', 'display_name', 24, 'jp', 'Image2', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(4, 'data_rows', 'display_name', 25, 'jp', 'Image3', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(5, 'data_rows', 'display_name', 26, 'jp', 'Image4', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(6, 'data_rows', 'display_name', 27, 'jp', 'Title', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(7, 'data_rows', 'display_name', 28, 'jp', 'Created At', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(8, 'data_rows', 'display_name', 29, 'jp', 'Updated At', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(9, 'data_types', 'display_name_singular', 4, 'jp', 'HomePage - Banner', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(10, 'data_types', 'display_name_plural', 4, 'jp', 'HomePage - Banner', '2020-09-27 20:46:49', '2020-09-27 20:46:49'),
(11, 'data_rows', 'display_name', 39, 'jp', 'Id', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(12, 'data_rows', 'display_name', 40, 'jp', 'Image1', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(13, 'data_rows', 'display_name', 41, 'jp', 'Image2', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(14, 'data_rows', 'display_name', 42, 'jp', 'Image', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(15, 'data_rows', 'display_name', 43, 'jp', 'Title', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(16, 'data_rows', 'display_name', 44, 'jp', 'ShortDescription', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(17, 'data_rows', 'display_name', 45, 'jp', 'Slug', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(18, 'data_rows', 'display_name', 46, 'jp', 'Created At', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(19, 'data_rows', 'display_name', 47, 'jp', 'Updated At', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(20, 'data_types', 'display_name_singular', 6, 'jp', 'Home Page Snack Ichi', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(21, 'data_types', 'display_name_plural', 6, 'jp', 'Home Page Snack Ichis', '2020-09-28 02:03:46', '2020-09-28 02:03:46'),
(22, 'data_rows', 'display_name', 57, 'jp', 'Id', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(23, 'data_rows', 'display_name', 58, 'jp', 'Name', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(24, 'data_rows', 'display_name', 59, 'jp', 'Address', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(25, 'data_rows', 'display_name', 60, 'jp', 'Phone', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(26, 'data_rows', 'display_name', 61, 'jp', 'Hotline', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(27, 'data_rows', 'display_name', 62, 'jp', 'FacebookLink', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(28, 'data_rows', 'display_name', 63, 'jp', 'TwitterLink', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(29, 'data_rows', 'display_name', 64, 'jp', 'InstagramLink', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(30, 'data_rows', 'display_name', 65, 'jp', 'Email', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(31, 'data_rows', 'display_name', 66, 'jp', 'IntroductionContent', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(32, 'data_rows', 'display_name', 67, 'jp', 'MIssionContent', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(33, 'data_rows', 'display_name', 68, 'jp', 'TypeofBussiness', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(34, 'data_rows', 'display_name', 69, 'jp', 'AboutContent', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(35, 'data_rows', 'display_name', 70, 'jp', 'TechologyContent', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(36, 'data_rows', 'display_name', 71, 'jp', 'Created At', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(37, 'data_rows', 'display_name', 72, 'jp', 'Updated At', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(38, 'data_types', 'display_name_singular', 8, 'jp', 'Infomation Company', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(39, 'data_types', 'display_name_plural', 8, 'jp', 'Infomation Companies', '2020-09-28 02:31:02', '2020-09-28 02:31:02'),
(40, 'data_rows', 'display_name', 73, 'jp', 'Logo', '2020-09-28 02:31:59', '2020-09-28 02:31:59'),
(41, 'data_rows', 'display_name', 74, 'jp', 'Id', '2020-09-28 02:47:14', '2020-09-28 02:47:14'),
(42, 'data_rows', 'display_name', 75, 'jp', 'Title', '2020-09-28 02:47:14', '2020-09-28 02:47:14'),
(43, 'data_rows', 'display_name', 76, 'jp', 'Content', '2020-09-28 02:47:14', '2020-09-28 02:47:14'),
(44, 'data_rows', 'display_name', 77, 'jp', 'FooterText', '2020-09-28 02:47:14', '2020-09-28 02:47:14'),
(45, 'data_rows', 'display_name', 78, 'jp', 'Created At', '2020-09-28 02:47:14', '2020-09-28 02:47:14'),
(46, 'data_rows', 'display_name', 79, 'jp', 'Updated At', '2020-09-28 02:47:14', '2020-09-28 02:47:14'),
(47, 'data_types', 'display_name_singular', 9, 'jp', 'Aboutus Page', '2020-09-28 02:47:14', '2020-09-28 02:47:14'),
(48, 'data_types', 'display_name_plural', 9, 'jp', 'Aboutus Pages', '2020-09-28 02:47:14', '2020-09-28 02:47:14'),
(49, 'data_rows', 'display_name', 80, 'jp', 'Id', '2020-09-28 03:00:20', '2020-09-28 03:00:20'),
(50, 'data_rows', 'display_name', 81, 'jp', 'Image1', '2020-09-28 03:00:20', '2020-09-28 03:00:20'),
(51, 'data_rows', 'display_name', 82, 'jp', 'Priorites', '2020-09-28 03:00:20', '2020-09-28 03:00:20'),
(52, 'data_rows', 'display_name', 83, 'jp', 'Created At', '2020-09-28 03:00:20', '2020-09-28 03:00:20'),
(53, 'data_rows', 'display_name', 84, 'jp', 'Updated At', '2020-09-28 03:00:20', '2020-09-28 03:00:20'),
(54, 'data_types', 'display_name_singular', 10, 'jp', 'Quality Licenece', '2020-09-28 03:00:20', '2020-09-28 03:00:20'),
(55, 'data_types', 'display_name_plural', 10, 'jp', 'Quality Liceneces', '2020-09-28 03:00:20', '2020-09-28 03:00:20'),
(56, 'data_rows', 'display_name', 85, 'jp', 'Id', '2020-09-28 03:03:50', '2020-09-28 03:03:50'),
(57, 'data_rows', 'display_name', 86, 'jp', 'Priorites', '2020-09-28 03:03:50', '2020-09-28 03:03:50'),
(58, 'data_rows', 'display_name', 87, 'jp', 'Title', '2020-09-28 03:03:50', '2020-09-28 03:03:50'),
(59, 'data_rows', 'display_name', 88, 'jp', 'Created At', '2020-09-28 03:03:50', '2020-09-28 03:03:50'),
(60, 'data_rows', 'display_name', 89, 'jp', 'Updated At', '2020-09-28 03:03:50', '2020-09-28 03:03:50'),
(61, 'data_types', 'display_name_singular', 11, 'jp', 'Number Impress', '2020-09-28 03:03:50', '2020-09-28 03:03:50'),
(62, 'data_types', 'display_name_plural', 11, 'jp', 'Number Impresses', '2020-09-28 03:03:50', '2020-09-28 03:03:50'),
(63, 'data_rows', 'display_name', 121, 'jp', 'Id', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(64, 'data_rows', 'display_name', 122, 'jp', 'name', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(65, 'data_rows', 'display_name', 123, 'jp', 'Address', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(66, 'data_rows', 'display_name', 124, 'jp', 'PhoneNumber', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(67, 'data_rows', 'display_name', 125, 'jp', 'Addres-Contact', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(68, 'data_rows', 'display_name', 126, 'jp', 'Addres-Contact Map', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(69, 'data_rows', 'display_name', 127, 'jp', 'Priority', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(70, 'data_rows', 'display_name', 128, 'jp', 'Created At', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(71, 'data_rows', 'display_name', 129, 'jp', 'Updated At', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(72, 'data_types', 'display_name_singular', 15, 'jp', 'Contact Itemm', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(73, 'data_types', 'display_name_plural', 15, 'jp', 'Contact Itemms', '2020-09-28 19:56:05', '2020-09-28 19:56:05'),
(74, 'data_rows', 'display_name', 130, 'jp', 'Id', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(75, 'data_rows', 'display_name', 131, 'jp', 'Title', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(76, 'data_rows', 'display_name', 132, 'jp', 'CodeColor', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(77, 'data_rows', 'display_name', 133, 'jp', 'BackgroundImamge', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(78, 'data_rows', 'display_name', 134, 'jp', 'ShortDescription', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(79, 'data_rows', 'display_name', 135, 'jp', 'Weight', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(80, 'data_rows', 'display_name', 136, 'jp', 'Priority', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(81, 'data_rows', 'display_name', 137, 'jp', 'Created At', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(82, 'data_rows', 'display_name', 138, 'jp', 'Updated At', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(83, 'data_types', 'display_name_singular', 16, 'jp', 'Product Category', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(84, 'data_types', 'display_name_plural', 16, 'jp', 'Product Categories', '2020-09-28 21:53:39', '2020-09-28 21:53:39'),
(85, 'data_rows', 'display_name', 140, 'jp', 'Id', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(86, 'data_rows', 'display_name', 141, 'jp', 'BackgroundImamge', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(87, 'data_rows', 'display_name', 142, 'jp', 'Weight', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(88, 'data_rows', 'display_name', 143, 'jp', 'Priority', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(89, 'data_rows', 'display_name', 144, 'jp', 'Created At', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(90, 'data_rows', 'display_name', 145, 'jp', 'Updated At', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(91, 'data_rows', 'display_name', 146, 'jp', 'product_categories', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(92, 'data_types', 'display_name_singular', 17, 'jp', 'Product Category Image', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(93, 'data_types', 'display_name_plural', 17, 'jp', 'Product Category Images', '2020-09-28 22:01:04', '2020-09-28 22:01:04'),
(94, 'data_rows', 'display_name', 148, 'jp', 'Id', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(95, 'data_rows', 'display_name', 149, 'jp', 'Title', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(96, 'data_rows', 'display_name', 150, 'jp', 'Slug', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(97, 'data_rows', 'display_name', 151, 'jp', 'Description', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(98, 'data_rows', 'display_name', 152, 'jp', 'BackgroundImamge', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(99, 'data_rows', 'display_name', 153, 'jp', 'ShortDescription', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(100, 'data_rows', 'display_name', 154, 'jp', 'Weight', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(101, 'data_rows', 'display_name', 155, 'jp', 'CategoryId', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(102, 'data_rows', 'display_name', 156, 'jp', 'Priority', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(103, 'data_rows', 'display_name', 157, 'jp', 'Created At', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(104, 'data_rows', 'display_name', 158, 'jp', 'Updated At', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(105, 'data_rows', 'display_name', 159, 'jp', 'product_categories', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(106, 'data_types', 'display_name_singular', 18, 'jp', 'Product', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(107, 'data_types', 'display_name_plural', 18, 'jp', 'Products', '2020-09-28 22:09:36', '2020-09-28 22:09:36'),
(108, 'menu_items', 'title', 29, 'jp', 'Sản phẩm', '2020-09-29 02:40:42', '2020-09-29 02:40:42'),
(109, 'data_rows', 'display_name', 30, 'jp', 'Id', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(110, 'data_rows', 'display_name', 31, 'jp', 'Hình', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(111, 'data_rows', 'display_name', 32, 'jp', 'Image2', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(112, 'data_rows', 'display_name', 33, 'jp', 'Image cover', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(113, 'data_rows', 'display_name', 34, 'jp', 'Text display', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(114, 'data_rows', 'display_name', 35, 'jp', 'Short description', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(115, 'data_rows', 'display_name', 36, 'jp', 'Slug', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(116, 'data_rows', 'display_name', 37, 'jp', 'Created At', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(117, 'data_rows', 'display_name', 38, 'jp', 'Updated At', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(118, 'data_rows', 'display_name', 160, 'jp', 'product_categories', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(119, 'data_types', 'display_name_singular', 5, 'jp', 'Home Page Ichi', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(120, 'data_types', 'display_name_plural', 5, 'jp', 'Home Page Ichis', '2020-09-29 20:48:10', '2020-09-29 20:48:10'),
(121, 'data_rows', 'display_name', 94, 'jp', 'Id', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(122, 'data_rows', 'display_name', 95, 'jp', 'Image1', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(123, 'data_rows', 'display_name', 96, 'jp', 'Image2', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(124, 'data_rows', 'display_name', 97, 'jp', 'Image', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(125, 'data_rows', 'display_name', 98, 'jp', 'Title', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(126, 'data_rows', 'display_name', 99, 'jp', 'ShortDescription', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(127, 'data_rows', 'display_name', 100, 'jp', 'Slug', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(128, 'data_rows', 'display_name', 101, 'jp', 'Created At', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(129, 'data_rows', 'display_name', 102, 'jp', 'Updated At', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(130, 'data_rows', 'display_name', 162, 'jp', 'product_categories', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(131, 'data_types', 'display_name_singular', 12, 'jp', 'Home Page Kid Ichi Cranker', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(132, 'data_types', 'display_name_plural', 12, 'jp', 'Home Page Kid Ichi Crankers', '2020-09-29 20:49:40', '2020-09-29 20:49:40'),
(133, 'data_rows', 'display_name', 48, 'jp', 'Id', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(134, 'data_rows', 'display_name', 49, 'jp', 'Image1', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(135, 'data_rows', 'display_name', 50, 'jp', 'Image2', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(136, 'data_rows', 'display_name', 51, 'jp', 'Image', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(137, 'data_rows', 'display_name', 52, 'jp', 'Title', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(138, 'data_rows', 'display_name', 53, 'jp', 'ShortDescription', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(139, 'data_rows', 'display_name', 54, 'jp', 'Slug', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(140, 'data_rows', 'display_name', 55, 'jp', 'Created At', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(141, 'data_rows', 'display_name', 56, 'jp', 'Updated At', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(142, 'data_rows', 'display_name', 164, 'jp', 'product_categories', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(143, 'data_types', 'display_name_singular', 7, 'jp', 'Home Page Kid Ichi', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(144, 'data_types', 'display_name_plural', 7, 'jp', 'Home Page Kid Ichis', '2020-09-29 20:50:19', '2020-09-29 20:50:19'),
(145, 'data_rows', 'display_name', 161, 'jp', 'Categoryid', '2020-09-29 20:51:38', '2020-09-29 20:51:38'),
(146, 'data_rows', 'display_name', 166, 'jp', 'product_categories', '2020-09-29 21:25:26', '2020-09-29 21:25:26'),
(147, 'data_rows', 'display_name', 139, 'jp', 'Slug', '2020-09-29 21:54:22', '2020-09-29 21:54:22'),
(148, 'data_rows', 'display_name', 169, 'jp', 'TitleDisplay', '2020-09-29 23:11:33', '2020-09-29 23:11:33'),
(149, 'data_rows', 'display_name', 170, 'jp', 'ClassCode', '2020-09-29 23:11:33', '2020-09-29 23:11:33'),
(150, 'data_rows', 'display_name', 171, 'jp', 'Title page detail', '2020-09-29 23:15:37', '2020-09-29 23:15:37'),
(151, 'data_rows', 'display_name', 168, 'jp', 'CodeColor', '2020-09-29 23:31:30', '2020-09-29 23:31:30'),
(152, 'data_rows', 'display_name', 172, 'jp', 'Color of page category', '2020-09-29 23:33:05', '2020-09-29 23:33:05'),
(153, 'data_rows', 'display_name', 30, 'en_US', 'Id', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(154, 'data_rows', 'display_name', 31, 'en_US', 'Hình', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(155, 'data_rows', 'display_name', 32, 'en_US', 'Image2', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(156, 'data_rows', 'display_name', 33, 'en_US', 'Image cover', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(157, 'data_rows', 'display_name', 34, 'en_US', 'Text display', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(158, 'data_rows', 'display_name', 35, 'en_US', 'Short description', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(159, 'data_rows', 'display_name', 36, 'en_US', 'Slug', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(160, 'data_rows', 'display_name', 37, 'en_US', 'Created At', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(161, 'data_rows', 'display_name', 38, 'en_US', 'Updated At', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(162, 'data_rows', 'display_name', 161, 'en_US', 'Categoryid', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(163, 'data_rows', 'display_name', 160, 'en_US', 'product_categories', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(164, 'data_types', 'display_name_singular', 5, 'en_US', 'Home Page Ichi', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(165, 'data_types', 'display_name_plural', 5, 'en_US', 'Home Page Ichis', '2020-09-30 19:20:52', '2020-09-30 19:20:52'),
(166, 'data_rows', 'display_name', 94, 'en_US', 'Id', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(167, 'data_rows', 'display_name', 95, 'en_US', 'Image1', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(168, 'data_rows', 'display_name', 96, 'en_US', 'Image2', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(169, 'data_rows', 'display_name', 97, 'en_US', 'Image', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(170, 'data_rows', 'display_name', 98, 'en_US', 'Title', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(171, 'data_rows', 'display_name', 99, 'en_US', 'ShortDescription', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(172, 'data_rows', 'display_name', 100, 'en_US', 'Slug', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(173, 'data_rows', 'display_name', 101, 'en_US', 'Created At', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(174, 'data_rows', 'display_name', 102, 'en_US', 'Updated At', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(175, 'data_rows', 'display_name', 163, 'jp', 'CategoryId', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(176, 'data_rows', 'display_name', 163, 'en_US', 'CategoryId', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(177, 'data_rows', 'display_name', 162, 'en_US', 'product_categories', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(178, 'data_types', 'display_name_singular', 12, 'en_US', 'Home Page Kid Ichi Cranker', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(179, 'data_types', 'display_name_plural', 12, 'en_US', 'Home Page Kid Ichi Crankers', '2020-09-30 19:21:46', '2020-09-30 19:21:46'),
(180, 'data_rows', 'display_name', 48, 'en_US', 'Id', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(181, 'data_rows', 'display_name', 49, 'en_US', 'Image1', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(182, 'data_rows', 'display_name', 50, 'en_US', 'Image2', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(183, 'data_rows', 'display_name', 51, 'en_US', 'Image', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(184, 'data_rows', 'display_name', 52, 'en_US', 'Title', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(185, 'data_rows', 'display_name', 53, 'en_US', 'ShortDescription', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(186, 'data_rows', 'display_name', 54, 'en_US', 'Slug', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(187, 'data_rows', 'display_name', 55, 'en_US', 'Created At', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(188, 'data_rows', 'display_name', 56, 'en_US', 'Updated At', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(189, 'data_rows', 'display_name', 165, 'jp', 'CategoryId', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(190, 'data_rows', 'display_name', 165, 'en_US', 'CategoryId', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(191, 'data_rows', 'display_name', 164, 'en_US', 'product_categories', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(192, 'data_types', 'display_name_singular', 7, 'en_US', 'Home Page Kid Ichi', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(193, 'data_types', 'display_name_plural', 7, 'en_US', 'Home Page Kid Ichis', '2020-09-30 19:22:48', '2020-09-30 19:22:48'),
(194, 'data_rows', 'display_name', 39, 'en_US', 'Id', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(195, 'data_rows', 'display_name', 40, 'en_US', 'Image1', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(196, 'data_rows', 'display_name', 41, 'en_US', 'Image2', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(197, 'data_rows', 'display_name', 42, 'en_US', 'Image', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(198, 'data_rows', 'display_name', 43, 'en_US', 'Title', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(199, 'data_rows', 'display_name', 44, 'en_US', 'ShortDescription', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(200, 'data_rows', 'display_name', 45, 'en_US', 'Slug', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(201, 'data_rows', 'display_name', 46, 'en_US', 'Created At', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(202, 'data_rows', 'display_name', 47, 'en_US', 'Updated At', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(203, 'data_rows', 'display_name', 167, 'jp', 'CategoryId', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(204, 'data_rows', 'display_name', 167, 'en_US', 'CategoryId', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(205, 'data_rows', 'display_name', 166, 'en_US', 'product_categories', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(206, 'data_types', 'display_name_singular', 6, 'en_US', 'Home Page Snack Ichi', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(207, 'data_types', 'display_name_plural', 6, 'en_US', 'Home Page Snack Ichis', '2020-09-30 19:24:25', '2020-09-30 19:24:25'),
(208, 'data_rows', 'display_name', 57, 'en_US', 'Id', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(209, 'data_rows', 'display_name', 58, 'en_US', 'Name', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(210, 'data_rows', 'display_name', 59, 'en_US', 'Address', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(211, 'data_rows', 'display_name', 60, 'en_US', 'Phone', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(212, 'data_rows', 'display_name', 61, 'en_US', 'Hotline', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(213, 'data_rows', 'display_name', 62, 'en_US', 'FacebookLink', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(214, 'data_rows', 'display_name', 63, 'en_US', 'TwitterLink', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(215, 'data_rows', 'display_name', 73, 'en_US', 'Logo', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(216, 'data_rows', 'display_name', 64, 'en_US', 'InstagramLink', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(217, 'data_rows', 'display_name', 65, 'en_US', 'Email', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(218, 'data_rows', 'display_name', 71, 'en_US', 'Created At', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(219, 'data_rows', 'display_name', 72, 'en_US', 'Updated At', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(220, 'data_types', 'display_name_singular', 8, 'en_US', 'Infomation Company', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(221, 'data_types', 'display_name_plural', 8, 'en_US', 'Infomation Companies', '2020-09-30 19:26:05', '2020-09-30 19:26:05'),
(222, 'data_rows', 'display_name', 74, 'en_US', 'Id', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(223, 'data_rows', 'display_name', 75, 'en_US', 'Title', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(224, 'data_rows', 'display_name', 76, 'en_US', 'Content', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(225, 'data_rows', 'display_name', 90, 'jp', 'QuatinityText', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(226, 'data_rows', 'display_name', 90, 'en_US', 'QuatinityText', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(227, 'data_rows', 'display_name', 77, 'en_US', 'FooterText', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(228, 'data_rows', 'display_name', 78, 'en_US', 'Created At', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(229, 'data_rows', 'display_name', 79, 'en_US', 'Updated At', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(230, 'data_types', 'display_name_singular', 9, 'en_US', 'Aboutus Page', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(231, 'data_types', 'display_name_plural', 9, 'en_US', 'Aboutus Pages', '2020-09-30 19:26:29', '2020-09-30 19:26:29'),
(232, 'data_rows', 'display_name', 80, 'en_US', 'Id', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(233, 'data_rows', 'display_name', 81, 'en_US', 'Image1', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(234, 'data_rows', 'display_name', 91, 'jp', 'Linkfile', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(235, 'data_rows', 'display_name', 91, 'en_US', 'Linkfile', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(236, 'data_rows', 'display_name', 92, 'jp', 'Slug', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(237, 'data_rows', 'display_name', 92, 'en_US', 'Slug', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(238, 'data_rows', 'display_name', 82, 'en_US', 'Priorites', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(239, 'data_rows', 'display_name', 83, 'en_US', 'Created At', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(240, 'data_rows', 'display_name', 84, 'en_US', 'Updated At', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(241, 'data_types', 'display_name_singular', 10, 'en_US', 'Quality Licenece', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(242, 'data_types', 'display_name_plural', 10, 'en_US', 'Quality Liceneces', '2020-09-30 19:26:58', '2020-09-30 19:26:58'),
(243, 'data_rows', 'display_name', 103, 'jp', 'Id', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(244, 'data_rows', 'display_name', 103, 'en_US', 'Id', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(245, 'data_rows', 'display_name', 104, 'jp', 'Title', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(246, 'data_rows', 'display_name', 104, 'en_US', 'Title', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(247, 'data_rows', 'display_name', 105, 'jp', 'Content', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(248, 'data_rows', 'display_name', 105, 'en_US', 'Content', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(249, 'data_rows', 'display_name', 106, 'jp', 'Avatar', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(250, 'data_rows', 'display_name', 106, 'en_US', 'Avatar', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(251, 'data_rows', 'display_name', 107, 'jp', 'step', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(252, 'data_rows', 'display_name', 107, 'en_US', 'step', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(253, 'data_rows', 'display_name', 108, 'jp', 'Created At', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(254, 'data_rows', 'display_name', 108, 'en_US', 'Created At', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(255, 'data_rows', 'display_name', 109, 'jp', 'Updated At', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(256, 'data_rows', 'display_name', 109, 'en_US', 'Updated At', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(257, 'data_types', 'display_name_singular', 13, 'jp', 'Step Item', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(258, 'data_types', 'display_name_singular', 13, 'en_US', 'Step Item', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(259, 'data_types', 'display_name_plural', 13, 'jp', 'Step Items', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(260, 'data_types', 'display_name_plural', 13, 'en_US', 'Step Items', '2020-09-30 19:28:48', '2020-09-30 19:28:48'),
(261, 'data_rows', 'display_name', 121, 'en_US', 'Id', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(262, 'data_rows', 'display_name', 122, 'en_US', 'name', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(263, 'data_rows', 'display_name', 123, 'en_US', 'Decription ', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(264, 'data_rows', 'display_name', 124, 'en_US', 'PhoneNumber', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(265, 'data_rows', 'display_name', 125, 'en_US', 'Google map description', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(266, 'data_rows', 'display_name', 126, 'en_US', 'Addres-Contact Map', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(267, 'data_rows', 'display_name', 127, 'en_US', 'Priority', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(268, 'data_rows', 'display_name', 128, 'en_US', 'Created At', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(269, 'data_rows', 'display_name', 129, 'en_US', 'Updated At', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(270, 'data_types', 'display_name_singular', 15, 'en_US', 'Contact Itemm', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(271, 'data_types', 'display_name_plural', 15, 'en_US', 'Contact Itemms', '2020-09-30 19:30:02', '2020-09-30 19:30:02'),
(272, 'data_rows', 'display_name', 110, 'jp', 'Id', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(273, 'data_rows', 'display_name', 110, 'en_US', 'Id', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(274, 'data_rows', 'display_name', 111, 'jp', 'PositionName', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(275, 'data_rows', 'display_name', 111, 'en_US', 'PositionName', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(276, 'data_rows', 'display_name', 112, 'jp', 'Step', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(277, 'data_rows', 'display_name', 112, 'en_US', 'Step', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(278, 'data_rows', 'display_name', 113, 'jp', 'LocationName', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(279, 'data_rows', 'display_name', 113, 'en_US', 'LocationName', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(280, 'data_rows', 'display_name', 114, 'jp', 'Priority', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(281, 'data_rows', 'display_name', 114, 'en_US', 'Priority', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(282, 'data_rows', 'display_name', 115, 'jp', 'Content', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(283, 'data_rows', 'display_name', 115, 'en_US', 'Content', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(284, 'data_rows', 'display_name', 116, 'jp', 'ImageShare', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(285, 'data_rows', 'display_name', 116, 'en_US', 'ImageShare', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(286, 'data_rows', 'display_name', 117, 'jp', 'Slug', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(287, 'data_rows', 'display_name', 117, 'en_US', 'Slug', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(288, 'data_rows', 'display_name', 118, 'jp', 'ShortDescription', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(289, 'data_rows', 'display_name', 118, 'en_US', 'ShortDescription', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(290, 'data_rows', 'display_name', 119, 'jp', 'Created At', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(291, 'data_rows', 'display_name', 119, 'en_US', 'Created At', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(292, 'data_rows', 'display_name', 120, 'jp', 'Updated At', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(293, 'data_rows', 'display_name', 120, 'en_US', 'Updated At', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(294, 'data_types', 'display_name_singular', 14, 'jp', 'Carrer Object Item', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(295, 'data_types', 'display_name_singular', 14, 'en_US', 'Carrer Object Item', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(296, 'data_types', 'display_name_plural', 14, 'jp', 'Carrer Object Items', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(297, 'data_types', 'display_name_plural', 14, 'en_US', 'Carrer Object Items', '2020-09-30 19:32:16', '2020-09-30 19:32:16'),
(298, 'data_rows', 'display_name', 130, 'en_US', 'Id', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(299, 'data_rows', 'display_name', 131, 'en_US', 'Title', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(300, 'data_rows', 'display_name', 171, 'en_US', 'Title page detail', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(301, 'data_rows', 'display_name', 172, 'en_US', 'Style of page category', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(302, 'data_rows', 'display_name', 169, 'en_US', 'TitleDisplay', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(303, 'data_rows', 'display_name', 132, 'en_US', 'Main color', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(304, 'data_rows', 'display_name', 139, 'en_US', 'Slug', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(305, 'data_rows', 'display_name', 133, 'en_US', 'BackgroundImamge', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(306, 'data_rows', 'display_name', 134, 'en_US', 'ShortDescription', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(307, 'data_rows', 'display_name', 135, 'en_US', 'Weight', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(308, 'data_rows', 'display_name', 136, 'en_US', 'Priority', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(309, 'data_rows', 'display_name', 137, 'en_US', 'Created At', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(310, 'data_rows', 'display_name', 138, 'en_US', 'Updated At', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(311, 'data_rows', 'display_name', 170, 'en_US', 'ClassCode', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(312, 'data_types', 'display_name_singular', 16, 'en_US', 'Product Category', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(313, 'data_types', 'display_name_plural', 16, 'en_US', 'Product Categories', '2020-09-30 19:33:52', '2020-09-30 19:33:52'),
(314, 'data_rows', 'display_name', 148, 'en_US', 'Id', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(315, 'data_rows', 'display_name', 149, 'en_US', 'Title', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(316, 'data_rows', 'display_name', 150, 'en_US', 'Slug', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(317, 'data_rows', 'display_name', 151, 'en_US', 'Description', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(318, 'data_rows', 'display_name', 152, 'en_US', 'BackgroundImamge', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(319, 'data_rows', 'display_name', 153, 'en_US', 'ShortDescription', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(320, 'data_rows', 'display_name', 154, 'en_US', 'Weight', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(321, 'data_rows', 'display_name', 155, 'en_US', 'CategoryId', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(322, 'data_rows', 'display_name', 156, 'en_US', 'Priority', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(323, 'data_rows', 'display_name', 157, 'en_US', 'Created At', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(324, 'data_rows', 'display_name', 158, 'en_US', 'Updated At', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(325, 'data_rows', 'display_name', 168, 'en_US', 'CodeColor', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(326, 'data_rows', 'display_name', 159, 'en_US', 'product_categories', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(327, 'data_types', 'display_name_singular', 18, 'en_US', 'Product', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(328, 'data_types', 'display_name_plural', 18, 'en_US', 'Products', '2020-09-30 19:35:16', '2020-09-30 19:35:16'),
(329, 'banner_home_pages', 'title', 1, 'jp', 'BÁNH GẠO NHẬT', '2020-09-30 19:58:36', '2020-09-30 19:58:36'),
(330, 'banner_home_pages', 'title', 1, 'en_US', 'BÁNH GẠO NHẬT tieng anh', '2020-09-30 19:58:36', '2020-09-30 19:58:36'),
(331, 'infomation_companies', 'name', 1, 'jp', 'BÁNH GẠO NHẬT', '2020-09-30 21:55:11', '2020-09-30 21:55:11'),
(332, 'infomation_companies', 'name', 1, 'en_US', 'BÁNH GẠO NHẬT', '2020-09-30 21:55:11', '2020-09-30 21:55:11'),
(333, 'product_categories', 'title', 6, 'jp', 'ICHI Cracker', '2020-10-02 05:09:26', '2020-10-02 05:09:26'),
(334, 'product_categories', 'title', 6, 'en_US', 'ICHI Cracker', '2020-10-02 05:09:26', '2020-10-02 05:09:26'),
(335, 'product_categories', 'titleDetail', 6, 'jp', 'ICHI Cracker', '2020-10-02 05:09:26', '2020-10-02 05:09:26'),
(336, 'product_categories', 'titleDetail', 6, 'en_US', 'ICHI Cracker', '2020-10-02 05:09:26', '2020-10-02 05:09:26'),
(337, 'product_categories', 'shortDescription', 6, 'jp', '<p>Si&ecirc;u mỏng, si&ecirc;u gi&ograve;n, si&ecirc;u ngon, ICHI Cracker l&agrave; lựa chọn ho&agrave;n hảo cho một m&oacute;n ăn vặt vừa vui miệng, vui tai, vừa tốt cho sức khỏe.</p>\n<p>Được l&agrave;m từ nguy&ecirc;n liệu cao cấp l&agrave; gạo lứt Japonica, bổ sung m&egrave; đen v&agrave; c&aacute;c gia vị đặc biệt, những chiếc b&aacute;nh gạo được c&aacute;n thật mỏng để đạt đến độ gi&ograve;n ho&agrave;n hảo, vừa cắn đ&atilde; tan tanh t&aacute;ch trong miệng, ăn ho&agrave;i kh&ocirc;ng ng&aacute;n.</p>\n<p>H&atilde;y thưởng thức v&agrave; cảm nhận vị thơm ngọn của gạo lứt, m&egrave; đen từ nơi đầu lưỡi v&agrave; c&ugrave;ng lắng nghe vị gi&ograve;n tan tanh t&aacute;ch của b&aacute;nh gạo Nhật Ichi Cracker gạo lứt m&egrave; đen - cho bạn th&ecirc;m vui th&ecirc;m y&ecirc;u đời.</p>', '2020-10-02 05:09:26', '2020-10-02 05:09:26'),
(338, 'product_categories', 'shortDescription', 6, 'en_US', '<p>\r\n									Siêu mỏng, siêu giòn, siêu ngon, ICHI Cracker là lựa chọn hoàn hảo cho một món ăn vặt vừa vui miệng, vui tai, vừa tốt cho sức khỏe.\r\n\r\n                                </p>\r\n\r\n                                <p>\r\n									Được làm từ nguyên liệu cao cấp là gạo lứt Japonica, bổ sung mè đen và các gia vị đặc biệt, những chiếc bánh gạo được cán thật mỏng để đạt đến độ giòn hoàn hảo, vừa cắn đã tan tanh tách trong miệng, ăn hoài không ngán.\r\n\r\n                                </p>\r\n								<p>\r\n									Hãy thưởng thức và cảm nhận vị thơm ngọn của gạo lứt, mè đen từ nơi đầu lưỡi\r\n									và cùng lắng nghe vị giòn tan tanh tách của bánh gạo Nhật Ichi Cracker gạo lứt mè đen - cho bạn thêm vui thêm yêu đời.\r\n								</p>', '2020-10-02 05:09:26', '2020-10-02 05:09:26'),
(339, 'product_categories', 'weight', 6, 'jp', '85gr', '2020-10-02 05:09:26', '2020-10-02 05:09:26'),
(340, 'product_categories', 'weight', 6, 'en_US', '85gr', '2020-10-02 05:09:26', '2020-10-02 05:09:26'),
(341, 'home_page_kid_ichi_crankers', 'title', 1, 'jp', 'ICHIクラッカー', '2020-10-02 05:10:07', '2020-10-02 05:13:30'),
(342, 'home_page_kid_ichi_crankers', 'title', 1, 'en_US', 'ICHI CRACKER', '2020-10-02 05:10:07', '2020-10-02 05:10:07'),
(343, 'home_page_ichis', 'title', 1, 'jp', 'ICHIのおせんべい', '2020-10-02 05:11:20', '2020-10-02 05:11:20'),
(344, 'home_page_ichis', 'title', 1, 'en_US', 'JAPANESE ICHI RICE CRACKER', '2020-10-02 05:11:20', '2020-10-02 05:13:53'),
(345, 'home_page_ichis', 'shortDescription', 1, 'jp', 'Giòn tan, ngon lành với vị Shouyu Mật ong đặc trưng. \n								Thêm bất ngờ với 2 vị hoàn toàn mới: Pizza và Thịt xông khói & Phô mai', '2020-10-02 05:11:20', '2020-10-02 05:11:20'),
(346, 'home_page_ichis', 'shortDescription', 1, 'en_US', 'Giòn tan, ngon lành với vị Shouyu Mật ong đặc trưng. \n								Thêm bất ngờ với 2 vị hoàn toàn mới: Pizza và Thịt xông khói & Phô mai', '2020-10-02 05:11:20', '2020-10-02 05:13:53'),
(347, 'home_page_snack_ichis', 'title', 1, 'jp', 'ご米のICHIスナック', '2020-10-02 05:12:36', '2020-10-02 05:12:36'),
(348, 'home_page_snack_ichis', 'title', 1, 'en_US', 'ICHI SNACK', '2020-10-02 05:12:36', '2020-10-02 05:12:36'),
(349, 'home_page_snack_ichis', 'shortDescription', 1, 'jp', '<p>K&iacute;ch th&iacute;ch khẩu vị với 5 hương vị phong ph&uacute;:<br />- Vị Mật ong <br />- Vị Cay <br />- Vị S&ocirc; c&ocirc; la<br />- Vị C&agrave; ri <br />- Vị xốt b&aacute;nh x&egrave;o Nhật</p>', '2020-10-02 05:12:36', '2020-10-02 05:12:36'),
(350, 'home_page_snack_ichis', 'shortDescription', 1, 'en_US', '<p>K&iacute;ch th&iacute;ch khẩu vị với 5 hương vị phong ph&uacute;:<br />- Vị Mật ong <br />- Vị Cay <br />- Vị S&ocirc; c&ocirc; la<br />- Vị C&agrave; ri <br />- Vị xốt b&aacute;nh x&egrave;o Nhật</p>', '2020-10-02 05:12:36', '2020-10-02 05:12:36'),
(351, 'home_page_kid_ichis', 'title', 1, 'jp', '子供のICHI', '2020-10-02 05:13:04', '2020-10-02 05:13:04'),
(352, 'home_page_kid_ichis', 'title', 1, 'en_US', 'ICHI KID', '2020-10-02 05:13:04', '2020-10-02 05:13:04'),
(353, 'home_page_kid_ichis', 'shortDescription', 1, 'jp', '<p>B&aacute;nh ăn dặm d&agrave;nh cho trẻ em từ 6 th&aacute;ng tuổi với hương vị nguy&ecirc;n bản, được sản xuất từ gạo Japonica cao cấp c&oacute; bổ sung canxi, gi&uacute;p b&eacute; tập nhai, cầm nắm v&agrave; bổ sung dinh dưỡng</p>', '2020-10-02 05:13:04', '2020-10-02 05:13:04'),
(354, 'home_page_kid_ichis', 'shortDescription', 1, 'en_US', '<p>B&aacute;nh ăn dặm d&agrave;nh cho trẻ em từ 6 th&aacute;ng tuổi với hương vị nguy&ecirc;n bản, được sản xuất từ gạo Japonica cao cấp c&oacute; bổ sung canxi, gi&uacute;p b&eacute; tập nhai, cầm nắm v&agrave; bổ sung dinh dưỡng</p>', '2020-10-02 05:13:04', '2020-10-02 05:13:04'),
(355, 'data_rows', 'display_name', 173, 'jp', 'TitleHomePage', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(356, 'data_rows', 'display_name', 173, 'en_US', 'TitleHomePage', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(357, 'data_rows', 'display_name', 174, 'jp', 'DescriptionHomePage', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(358, 'data_rows', 'display_name', 174, 'en_US', 'DescriptionHomePage', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(359, 'data_rows', 'display_name', 175, 'jp', 'ImagHomePage', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(360, 'data_rows', 'display_name', 175, 'en_US', 'ImagHomePage', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(361, 'data_rows', 'display_name', 176, 'jp', 'TitleSeo', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(362, 'data_rows', 'display_name', 176, 'en_US', 'TitleSeo', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(363, 'data_rows', 'display_name', 177, 'jp', 'DescriptionSEO', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(364, 'data_rows', 'display_name', 177, 'en_US', 'DescriptionSEO', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(365, 'data_rows', 'display_name', 178, 'jp', 'LinkImageShare', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(366, 'data_rows', 'display_name', 178, 'en_US', 'LinkImageShare', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(367, 'data_rows', 'display_name', 179, 'jp', 'Keyword', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(368, 'data_rows', 'display_name', 179, 'en_US', 'Keyword', '2020-10-03 01:01:53', '2020-10-03 01:01:53'),
(369, 'product_categories', 'title', 1, 'jp', 'Bánh gạo Nhật ICHI', '2020-10-03 01:16:43', '2020-10-03 01:16:43'),
(370, 'product_categories', 'title', 1, 'en_US', 'Bánh gạo Nhật ICHI', '2020-10-03 01:16:43', '2020-10-03 01:16:43'),
(371, 'product_categories', 'titleDisplay', 1, 'jp', 'SẢN PHẨM CỦA BÁNH GẠO ICHI2', '2020-10-03 01:16:43', '2020-10-03 01:16:43'),
(372, 'product_categories', 'titleDisplay', 1, 'en_US', 'SẢN PHẨM CỦA BÁNH GẠO ICHI2', '2020-10-03 01:16:43', '2020-10-03 01:16:43'),
(373, 'product_categories', 'titleDetail', 1, 'jp', 'BÁNH GẠO NHẬT ICHI VỊ SHOUYU MẬT ONG ', '2020-10-03 01:16:43', '2020-10-03 01:16:43'),
(374, 'product_categories', 'titleDetail', 1, 'en_US', 'BÁNH GẠO NHẬT ICHI VỊ SHOUYU MẬT ONG ', '2020-10-03 01:16:43', '2020-10-03 01:16:43'),
(375, 'product_categories', 'shortDescription', 1, 'jp', '<p>\r\n                                    Sản phẩm đặc trưng của thương hiệu bánh gạo ICHI, được làm từ nguyên liệu gạo cao cấp Japonica, \r\n                                    kết hợp cùng hương vị đặc trưng của Nhật Bản (Shouyu mật ong) và các hương vị mới độc đáo \r\n                                    (Pizza, Thịt xông khói &amp; Phô mai), cho bạn trải nghiệm ẩm thực hài hòa, độc đáo đến từ xứ sở hoa anh đào.<br>\r\n                                    Đặc biệt, Shouyu mật ong – hương vị được yêu thích nhất của ICHI mang đến những chiếc bánh giòn mà không cứng,\r\n                                     vị mặn ngọt hài hòa, ăn mãi không chán.\r\n                                    Sản phẩm được đóng gói nhỏ gọn, dễ sử dụng làm món ăn vặt trong ngày, trong các buổi liên hoan, \r\n                                    dễ dàng mang theo trong những chuyến đi chơi xa hay làm quà tặng cho bạn bè, người thân. \r\n                                </p>', '2020-10-03 01:16:43', '2020-10-03 01:16:43'),
(376, 'product_categories', 'shortDescription', 1, 'en_US', '<p>\r\n                                    Sản phẩm đặc trưng của thương hiệu bánh gạo ICHI, được làm từ nguyên liệu gạo cao cấp Japonica, \r\n                                    kết hợp cùng hương vị đặc trưng của Nhật Bản (Shouyu mật ong) và các hương vị mới độc đáo \r\n                                    (Pizza, Thịt xông khói &amp; Phô mai), cho bạn trải nghiệm ẩm thực hài hòa, độc đáo đến từ xứ sở hoa anh đào.<br>\r\n                                    Đặc biệt, Shouyu mật ong – hương vị được yêu thích nhất của ICHI mang đến những chiếc bánh giòn mà không cứng,\r\n                                     vị mặn ngọt hài hòa, ăn mãi không chán.\r\n                                    Sản phẩm được đóng gói nhỏ gọn, dễ sử dụng làm món ăn vặt trong ngày, trong các buổi liên hoan, \r\n                                    dễ dàng mang theo trong những chuyến đi chơi xa hay làm quà tặng cho bạn bè, người thân. \r\n                                </p>', '2020-10-03 01:16:43', '2020-10-03 01:16:43'),
(377, 'aboutus_pages', 'title', 1, 'jp', 'VỀ CHÚNG TÔI', '2020-10-05 02:48:41', '2020-10-05 02:48:41'),
(378, 'aboutus_pages', 'title', 1, 'en_US', 'VỀ CHÚNG TÔI', '2020-10-05 02:48:41', '2020-10-05 02:48:41'),
(379, 'aboutus_pages', 'content', 1, 'jp', '<p>B&aacute;nh gạo Nhật ICHI l&agrave; sản phẩm độc quyền thuộc C&ocirc;ng Ty Cổ Phần Thi&ecirc;n H&agrave; Kameda. Thi&ecirc;n H&agrave; Kameda được th&agrave;nh lập ng&agrave;y 9/1/2013 bởi sự hợp t&aacute;c, li&ecirc;n doanh giữa doanh nghiệp Việt Nam l&agrave; C&ocirc;ng Ty Cổ Phần Thi&ecirc;n H&agrave; Corp v&agrave; C&ocirc;ng Ty Nhật Bản Kameda Seika Co., LTD (thương hiệu b&aacute;nh gạo c&oacute; doanh thu số 1 Nhật Bản).</p>\n<p>Với số lượng CBCNV 300 người c&ugrave;ng đội ngũ l&atilde;nh đạo chuy&ecirc;n nghiệp, sau một khoảng thời gian h&igrave;nh th&agrave;nh v&agrave; ph&aacute;t triển, Thi&ecirc;n H&agrave; Kameda đ&atilde; tạo được tiếng vang lớn tr&ecirc;n thị trường Việt Nam với sản phẩm b&aacute;nh gạo gi&ograve;n ngon chuẩn Nhật, được người ti&ecirc;u d&ugrave;ng đ&oacute;n nhận v&agrave; y&ecirc;u th&iacute;ch. Từ một nh&agrave; m&aacute;y đầu ti&ecirc;n tại miền Bắc (Hưng Y&ecirc;n), Thi&ecirc;n H&agrave; Kameda tiếp tục mở rộng quy m&ocirc; sản xuất tại miền Trung v&agrave; miền Nam bằng việc th&agrave;nh lập 2 nh&agrave; m&aacute;y tại Thừa Thi&ecirc;n Huế (năm 2015) v&agrave; Đồng Th&aacute;p (năm 2016).</p>\n<p>Với mong muốn mang đến cho người Việt sản phẩm b&aacute;nh gạo chất lượng chuẩn Nhật Bản nhưng vẫn ph&ugrave; hợp với khẩu vị Việt Nam, đội ngũ Thi&ecirc;n H&agrave; Kameda lu&ocirc;n ch&uacute; trọng từng kh&acirc;u trong quy tr&igrave;nh sản xuất, kh&ocirc;ng ngừng cải tiến để mang đến cho người d&ugrave;ng những hương vị độc đ&aacute;o trong từng chiếc b&aacute;nh gi&ograve;n tan.</p>\n<p>Hiện tại, ICHI l&agrave; một trong những thương hiệu b&aacute;nh gạo b&aacute;n chạy nhất tại thị trường Việt Nam. Trong đ&oacute;, sản phẩm ghi dấu ấn v&agrave; được y&ecirc;u th&iacute;ch nhất l&agrave; b&aacute;nh gạo Nhật vị nước tương Shouyu mật ong. Với sự k&ecirc;́t hợp của những nguy&ecirc;n liệu thượng hạng: gạo Japonica thơm lừng, nước tương Shouyu đặc trưng của Nhật Bản v&agrave; vị ngọt dịu của m&acirc;̣t ong, từng chiếc b&aacute;nh cho người d&ugrave;ng trải nghiệm ẩm thực độc đ&aacute;o, thơm ngon, gi&ograve;n tan bất ngờ.</p>\n<p>Giữ trọn chất lượng Nhật Bản trong từng chiếc b&aacute;nh, kh&ocirc;ng ngừng mang đến những hương vị v&agrave; trải nghiệm mới lạ, b&aacute;nh gạo Nhật ICHI (c&oacute; nghĩa l&agrave; số 1 trong tiếng Nhật), đ&uacute;ng như t&ecirc;n gọi, tiếp tục sẽ l&agrave; c&aacute;i t&ecirc;n người d&ugrave;ng nhớ đến đầu ti&ecirc;n khi nghĩ đến m&oacute;n b&aacute;nh gạo Nhật thơm ngon.</p>', '2020-10-05 02:48:41', '2020-10-05 02:48:41');
INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(380, 'aboutus_pages', 'content', 1, 'en_US', '<p>B&aacute;nh gạo Nhật ICHI l&agrave; sản phẩm độc quyền thuộc C&ocirc;ng Ty Cổ Phần Thi&ecirc;n H&agrave; Kameda. Thi&ecirc;n H&agrave; Kameda được th&agrave;nh lập ng&agrave;y 9/1/2013 bởi sự hợp t&aacute;c, li&ecirc;n doanh giữa doanh nghiệp Việt Nam l&agrave; C&ocirc;ng Ty Cổ Phần Thi&ecirc;n H&agrave; Corp v&agrave; C&ocirc;ng Ty Nhật Bản Kameda Seika Co., LTD (thương hiệu b&aacute;nh gạo c&oacute; doanh thu số 1 Nhật Bản).</p>\r\n<p>Với số lượng CBCNV 300 người c&ugrave;ng đội ngũ l&atilde;nh đạo chuy&ecirc;n nghiệp, sau một khoảng thời gian h&igrave;nh th&agrave;nh v&agrave; ph&aacute;t triển, Thi&ecirc;n H&agrave; Kameda đ&atilde; tạo được tiếng vang lớn tr&ecirc;n thị trường Việt Nam với sản phẩm b&aacute;nh gạo gi&ograve;n ngon chuẩn Nhật, được người ti&ecirc;u d&ugrave;ng đ&oacute;n nhận v&agrave; y&ecirc;u th&iacute;ch. Từ một nh&agrave; m&aacute;y đầu ti&ecirc;n tại miền Bắc (Hưng Y&ecirc;n), Thi&ecirc;n H&agrave; Kameda tiếp tục mở rộng quy m&ocirc; sản xuất tại miền Trung v&agrave; miền Nam bằng việc th&agrave;nh lập 2 nh&agrave; m&aacute;y tại Thừa Thi&ecirc;n Huế (năm 2015) v&agrave; Đồng Th&aacute;p (năm 2016).</p>\r\n<p>Với mong muốn mang đến cho người Việt sản phẩm b&aacute;nh gạo chất lượng chuẩn Nhật Bản nhưng vẫn ph&ugrave; hợp với khẩu vị Việt Nam, đội ngũ Thi&ecirc;n H&agrave; Kameda lu&ocirc;n ch&uacute; trọng từng kh&acirc;u trong quy tr&igrave;nh sản xuất, kh&ocirc;ng ngừng cải tiến để mang đến cho người d&ugrave;ng những hương vị độc đ&aacute;o trong từng chiếc b&aacute;nh gi&ograve;n tan.</p>\r\n<p>Hiện tại, ICHI l&agrave; một trong những thương hiệu b&aacute;nh gạo b&aacute;n chạy nhất tại thị trường Việt Nam. Trong đ&oacute;, sản phẩm ghi dấu ấn v&agrave; được y&ecirc;u th&iacute;ch nhất l&agrave; b&aacute;nh gạo Nhật vị nước tương Shouyu mật ong. Với sự k&ecirc;́t hợp của những nguy&ecirc;n liệu thượng hạng: gạo Japonica thơm lừng, nước tương Shouyu đặc trưng của Nhật Bản v&agrave; vị ngọt dịu của m&acirc;̣t ong, từng chiếc b&aacute;nh cho người d&ugrave;ng trải nghiệm ẩm thực độc đ&aacute;o, thơm ngon, gi&ograve;n tan bất ngờ.</p>\r\n<p>Giữ trọn chất lượng Nhật Bản trong từng chiếc b&aacute;nh, kh&ocirc;ng ngừng mang đến những hương vị v&agrave; trải nghiệm mới lạ, b&aacute;nh gạo Nhật ICHI (c&oacute; nghĩa l&agrave; số 1 trong tiếng Nhật), đ&uacute;ng như t&ecirc;n gọi, tiếp tục sẽ l&agrave; c&aacute;i t&ecirc;n người d&ugrave;ng nhớ đến đầu ti&ecirc;n khi nghĩ đến m&oacute;n b&aacute;nh gạo Nhật thơm ngon.</p>', '2020-10-05 02:48:41', '2020-10-05 02:48:41'),
(381, 'aboutus_pages', 'footerText', 1, 'jp', 'Hàng triệu người dùng đã lựa chọn & yêu thích', '2020-10-05 02:48:41', '2020-10-05 02:48:41'),
(382, 'aboutus_pages', 'footerText', 1, 'en_US', 'Hàng triệu người dùng đã lựa chọn & yêu thích', '2020-10-05 02:48:41', '2020-10-05 02:48:41'),
(383, 'aboutus_pages', 'QuatinityText', 1, 'jp', '2424242424', '2020-10-05 02:48:41', '2020-10-05 02:48:41'),
(384, 'aboutus_pages', 'QuatinityText', 1, 'en_US', 'Suốt quá trình hình thành và phát triển, Thiên Hà Kameda đã vinh dự nhận được các chứng nhận 								về HT an toàn thực phẩm 22000, giấy chứng nhận HACCP 2003, ISO 22000:205.', '2020-10-05 02:48:41', '2020-10-05 02:48:41'),
(385, 'data_rows', 'display_name', 180, 'jp', 'Id', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(386, 'data_rows', 'display_name', 180, 'en_US', 'Id', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(387, 'data_rows', 'display_name', 181, 'jp', 'Home page text display', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(388, 'data_rows', 'display_name', 181, 'en_US', 'Home page text display', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(389, 'data_rows', 'display_name', 182, 'jp', 'Home page content display', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(390, 'data_rows', 'display_name', 182, 'en_US', 'Home page content display', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(391, 'data_rows', 'display_name', 183, 'jp', 'Homepage image', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(392, 'data_rows', 'display_name', 183, 'en_US', 'Homepage image', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(393, 'data_rows', 'display_name', 184, 'jp', 'Banner image', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(394, 'data_rows', 'display_name', 184, 'en_US', 'Banner image', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(395, 'data_rows', 'display_name', 185, 'jp', 'TitleIntroduction', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(396, 'data_rows', 'display_name', 185, 'en_US', 'TitleIntroduction', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(397, 'data_rows', 'display_name', 186, 'jp', 'IntroductionContent', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(398, 'data_rows', 'display_name', 186, 'en_US', 'IntroductionContent', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(399, 'data_rows', 'display_name', 187, 'jp', 'TsingTaoVietNamTitle', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(400, 'data_rows', 'display_name', 187, 'en_US', 'TsingTaoVietNamTitle', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(401, 'data_rows', 'display_name', 188, 'jp', 'TsingTaoVietNamContent', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(402, 'data_rows', 'display_name', 188, 'en_US', 'TsingTaoVietNamContent', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(403, 'data_rows', 'display_name', 189, 'jp', 'TsingTaoVietNamMisionTitle', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(404, 'data_rows', 'display_name', 189, 'en_US', 'TsingTaoVietNamMisionTitle', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(405, 'data_rows', 'display_name', 190, 'jp', 'TsingTaoVietNamMisionContent', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(406, 'data_rows', 'display_name', 190, 'en_US', 'TsingTaoVietNamMisionContent', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(407, 'data_rows', 'display_name', 191, 'jp', 'TitlePage', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(408, 'data_rows', 'display_name', 191, 'en_US', 'TitlePage', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(409, 'data_rows', 'display_name', 192, 'jp', 'Created At', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(410, 'data_rows', 'display_name', 192, 'en_US', 'Created At', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(411, 'data_rows', 'display_name', 193, 'jp', 'Updated At', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(412, 'data_rows', 'display_name', 193, 'en_US', 'Updated At', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(413, 'data_types', 'display_name_singular', 19, 'jp', 'Introduction Object Page', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(414, 'data_types', 'display_name_singular', 19, 'en_US', 'Introduction Object Page', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(415, 'data_types', 'display_name_plural', 19, 'jp', 'Introduction Object Pages', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(416, 'data_types', 'display_name_plural', 19, 'en_US', 'Introduction Object Pages', '2020-10-18 21:11:01', '2020-10-18 21:11:01'),
(417, 'news', 'title', 3, 'jp', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '2020-10-18 23:45:42', '2020-10-18 23:45:42'),
(418, 'news', 'title', 3, 'en_US', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form', '2020-10-18 23:45:42', '2020-10-18 23:45:42'),
(419, 'data_rows', 'display_name', 209, 'jp', 'Id', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(420, 'data_rows', 'display_name', 209, 'en_US', 'Id', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(421, 'data_rows', 'display_name', 210, 'jp', 'Title', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(422, 'data_rows', 'display_name', 210, 'en_US', 'Title', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(423, 'data_rows', 'display_name', 211, 'jp', 'ContentDescription', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(424, 'data_rows', 'display_name', 211, 'en_US', 'ContentDescription', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(425, 'data_rows', 'display_name', 212, 'jp', 'Image', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(426, 'data_rows', 'display_name', 212, 'en_US', 'Image', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(427, 'data_rows', 'display_name', 213, 'jp', 'Priorites', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(428, 'data_rows', 'display_name', 213, 'en_US', 'Priorites', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(429, 'data_rows', 'display_name', 214, 'jp', 'Created At', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(430, 'data_rows', 'display_name', 214, 'en_US', 'Created At', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(431, 'data_rows', 'display_name', 215, 'jp', 'Updated At', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(432, 'data_rows', 'display_name', 215, 'en_US', 'Updated At', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(433, 'data_types', 'display_name_singular', 21, 'jp', 'Developer Item', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(434, 'data_types', 'display_name_singular', 21, 'en_US', 'Developer Item', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(435, 'data_types', 'display_name_plural', 21, 'jp', 'Developer Items', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(436, 'data_types', 'display_name_plural', 21, 'en_US', 'Developer Items', '2020-10-18 23:55:28', '2020-10-18 23:55:28'),
(437, 'language_displays', 'Homepage_productsingtaoTile', 2, 'jp', 'CÁC SẢN PHẨM CỦA TSINGTAO', '2020-10-19 19:59:57', '2020-10-19 19:59:57'),
(438, 'language_displays', 'Homepage_productsingtaoTile', 2, 'en_US', 'CÁC SẢN PHẨM CỦA TSINGTAO', '2020-10-19 19:59:57', '2020-10-19 19:59:57'),
(439, 'menu_items', 'title', 32, 'jp', 'Về chúng tôi', '2020-10-19 20:15:40', '2020-10-19 20:15:40'),
(440, 'menu_items', 'title', 32, 'en_US', 'Về chúng tôi', '2020-10-19 20:15:40', '2020-10-19 20:15:40'),
(441, 'menu_items', 'title', 33, 'jp', 'Quy trình sản xuất', '2020-10-19 20:16:13', '2020-10-19 20:16:13'),
(442, 'menu_items', 'title', 33, 'en_US', 'Quy trình sản xuất', '2020-10-19 20:16:13', '2020-10-19 20:16:13'),
(443, 'menu_items', 'title', 29, 'en_US', 'Sản phẩm', '2020-10-19 20:17:29', '2020-10-19 20:17:29'),
(444, 'menu_items', 'title', 30, 'jp', 'Tuyển dụng', '2020-10-19 20:18:35', '2020-10-19 20:18:35'),
(445, 'menu_items', 'title', 30, 'en_US', 'Tuyển dụng', '2020-10-19 20:18:35', '2020-10-19 20:18:35'),
(446, 'data_rows', 'display_name', 280, 'jp', 'Id', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(447, 'data_rows', 'display_name', 280, 'en', 'Id', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(448, 'data_rows', 'display_name', 281, 'jp', 'Image', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(449, 'data_rows', 'display_name', 281, 'en', 'Image', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(450, 'data_rows', 'display_name', 282, 'jp', 'Title', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(451, 'data_rows', 'display_name', 282, 'en', 'Title', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(452, 'data_rows', 'display_name', 283, 'jp', 'KeyScreen', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(453, 'data_rows', 'display_name', 283, 'en', 'KeyScreen', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(454, 'data_rows', 'display_name', 284, 'jp', 'Created At', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(455, 'data_rows', 'display_name', 284, 'en', 'Created At', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(456, 'data_rows', 'display_name', 285, 'jp', 'Updated At', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(457, 'data_rows', 'display_name', 285, 'en', 'Updated At', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(458, 'data_types', 'display_name_singular', 27, 'jp', 'Banner Item', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(459, 'data_types', 'display_name_singular', 27, 'en', 'Banner Item', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(460, 'data_types', 'display_name_plural', 27, 'jp', 'Banner Items', '2020-10-19 20:46:47', '2020-10-19 20:46:47'),
(461, 'data_types', 'display_name_plural', 27, 'en', 'Banner Items', '2020-10-19 20:46:47', '2020-10-19 20:46:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', 'your@email.com', 'users/default.png', NULL, '$2y$10$WMrKwqjOnjJXDm6CEpRRa.Qr5CL1tdo9hHi7gVAobEY5ryDl0/WrG', 'cOsLbJG81nsfVnhRAODSYVgVbceNgdRzhKXZCkVEYojqsmGVe022P1SRrKut', NULL, '2020-09-27 19:48:34', '2020-09-27 19:48:34'),
(2, 1, 'banhgao', 'banhgao@email.com', 'users/default.png', NULL, '$2y$10$rQSBE8cl8Ib1GeGbeofSNOZ9j4QKO4VKLbU8Asw26Ide.Tpt7gWiy', NULL, NULL, '2020-09-30 03:41:41', '2020-09-30 03:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus_pages`
--
ALTER TABLE `aboutus_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apply_job_carrers`
--
ALTER TABLE `apply_job_carrers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_home_pages`
--
ALTER TABLE `banner_home_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_items`
--
ALTER TABLE `banner_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_pages`
--
ALTER TABLE `banner_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carrer_object_items`
--
ALTER TABLE `carrer_object_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carrer_object_pages`
--
ALTER TABLE `carrer_object_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_infos`
--
ALTER TABLE `contact_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_itemms`
--
ALTER TABLE `contact_itemms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_requests`
--
ALTER TABLE `contact_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `developer_items`
--
ALTER TABLE `developer_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `developer_pages`
--
ALTER TABLE `developer_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_infos`
--
ALTER TABLE `footer_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_ichis`
--
ALTER TABLE `home_page_ichis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_kid_ichis`
--
ALTER TABLE `home_page_kid_ichis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_kid_ichi_crankers`
--
ALTER TABLE `home_page_kid_ichi_crankers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_page_snack_ichis`
--
ALTER TABLE `home_page_snack_ichis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infomation_companies`
--
ALTER TABLE `infomation_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `introduction_object_pages`
--
ALTER TABLE `introduction_object_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language_displays`
--
ALTER TABLE `language_displays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_slug_unique` (`slug`);

--
-- Indexes for table `number_impresses`
--
ALTER TABLE `number_impresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category_images`
--
ALTER TABLE `product_category_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quality_liceneces`
--
ALTER TABLE `quality_liceneces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `seo_pages`
--
ALTER TABLE `seo_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `slide_banners`
--
ALTER TABLE `slide_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `step_items`
--
ALTER TABLE `step_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus_pages`
--
ALTER TABLE `aboutus_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `apply_job_carrers`
--
ALTER TABLE `apply_job_carrers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banner_home_pages`
--
ALTER TABLE `banner_home_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner_items`
--
ALTER TABLE `banner_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `banner_pages`
--
ALTER TABLE `banner_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carrer_object_items`
--
ALTER TABLE `carrer_object_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carrer_object_pages`
--
ALTER TABLE `carrer_object_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_infos`
--
ALTER TABLE `contact_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_itemms`
--
ALTER TABLE `contact_itemms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_requests`
--
ALTER TABLE `contact_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `developer_items`
--
ALTER TABLE `developer_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `developer_pages`
--
ALTER TABLE `developer_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer_infos`
--
ALTER TABLE `footer_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page_ichis`
--
ALTER TABLE `home_page_ichis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page_kid_ichis`
--
ALTER TABLE `home_page_kid_ichis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page_kid_ichi_crankers`
--
ALTER TABLE `home_page_kid_ichi_crankers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page_snack_ichis`
--
ALTER TABLE `home_page_snack_ichis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `infomation_companies`
--
ALTER TABLE `infomation_companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `introduction_object_pages`
--
ALTER TABLE `introduction_object_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `language_displays`
--
ALTER TABLE `language_displays`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `number_impresses`
--
ALTER TABLE `number_impresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_category_images`
--
ALTER TABLE `product_category_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `quality_liceneces`
--
ALTER TABLE `quality_liceneces`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `seo_pages`
--
ALTER TABLE `seo_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `slide_banners`
--
ALTER TABLE `slide_banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `step_items`
--
ALTER TABLE `step_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=462;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
