<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */


    'homePage' => [
        'mission' => 'ミッション',
        'viewDetail' =>'View detail',
        'titleContact'=>'CONTACT US'
    ],
    'aboutusPage' => [
        'licenesTextDiplay' => 'QUALITY CERTIFICATION ',
        'numberImpress'=>'IMPRESSIVE NUMBERS'

    ],

    'productPage' => [
        'description' => 'Product Description:',
        'khamphahuongvikhac' => 'Discover more other flavors',
        'projectTitleText' => '開発実績',
        'HeadOfficeLable' => '本社',
        'techologyTitle'=>'技術スタック',
        'introductionM2'=>'M2 Holdingsについて'
    ],


    'contactPage'=> [

        'textTitle'=>"CONTACT INFORMATION",
        'emptyClick'=>'(Click to close)',
        'titleform' => 'send the contact',
        'fullName' => 'Full name', // Họ & tên
        'PhoneNumber'=>'phone number', // Số điện thoại
        'Eamil'=>'Email', // Email
        'location'=> 'location',
        'titleOfMessage'=>'Message', // Thông điệp
        'yourMessage'=>'Enter contact text', // Nôi dung thông điệp
        'sendNow'=> 'SEND THE CONTACT', // Gửi ngay
        'successCreateRequest'=>'SEND A REQUEST FOR SUCCESS', // Gửi yêu cầu thành công!
    'errorRequire'=> [
        'fullName' => 'Full name is required', // Họ & tên là bắt buộc
        'email'=> 'Email is required', //Email  là bắt buộc
        'phoneNumber'=>'Phone number is required', //Số điện thoại là bắt buộc
        'titleOfMessageR'=>'Content message is required', //Bắt buộc điền
        'yourMessage'=>'Content contact is required' //Bắt buộc điền
        ],
    'errorValidate'=> [
        'phoneNumber'=>'invalid phone number', // Số điện thoại không hợp lệ
        'Email' => 'Email is illegal' , //Email không hợp lệ
        'titleOfMessager'=>'Content contact is required ' //Bắt buộc điền
            ],
        ],

	'tuyendungPage'=> [
		'postionName'=>'VACANCIES',
		'numnber'=>'AMOUNT',
		'locationWork'=>'WORKPLACE',
        'registerDate' => 'DATE SUBMITTED',
        'tuyendung' =>'CAREERS',
        'quytrinhsanxuat'=>'PRODUCTION PROCESS',
        'descriptionWork'=>'Description work',
        'applyform'=>'Apply form',

        'uploadFile'=>'Upload your profile',
        'uploadFileSize'=>'Use the format: docx, pdf (5Mb)',
        'sendButton'=>'Send records',


	'errorRequire'=> [
		'fileNotSelect' => 'You have not uploaded your profile yet, please upload it before submitting it', // Họ & tên là bắt buộc

		],
	'errorValidate'=> [
			'phoneNumber'=>'invalid phone number', // Số điện thoại không hợp lệ
			'Email' => 'Email is illegal' , //Email không hợp lệ
			'titleOfMessager'=>'Content contact is required ' //Bắt buộc điền
			],
		],
    'footerPage' => [
        'followOn'=>'FOLLOW US ON:',
        'banquyen' => 'Copyright belongs to Joint Stock Company Thien Ha Kameda ',
        'footerText' => 'hien Ha Kameda has received Quality Certifications of Food Safety conditions 22000, HACCP 2003 license, ISO 22000:2005, FSSC2200.',
    ]
];
