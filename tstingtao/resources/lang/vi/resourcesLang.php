<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */


    'homePage' => [
        'mission' => 'ミッション',
        'viewDetail' =>'Xem chi tiết',
        'titleContact'=>'LIÊN HỆ VỚI CHÚNG TÔI'
    ],
    'aboutusPage' => [
        'licenesTextDiplay' => 'CÁC CHỨNG NHẬN CHẤT LƯỢNG',
        'numberImpress'=>'NHỮNG CON SỐ ẤN TƯỢNG'

    ],

    'productPage' => [
        'description' => 'Mô tả sản phẩm:',
        'khamphahuongvikhac' => 'Khám phá thêm nhiều hương vị khác
        ',
        'projectTitleText' => '開発実績',
        'HeadOfficeLable' => '本社',
        'techologyTitle'=>'技術スタック',
        'introductionM2'=>'M2 Holdingsについて'
    ],


    'contactPage'=> [

        'textTitle'=>"THÔNG TIN LIÊN HỆ",
        'emptyClick'=>'(Click để đóng)',
        'titleform' => 'GỬI LIÊN HỆ',
        'fullName' => 'Họ & tên', // Họ & tên
        'PhoneNumber'=>'Số điện thoại', // Số điện thoại
        'Eamil'=>'Email', // Email
        'location'=> 'location',
        'titleOfMessage'=>'Thông điệp', // Thông điệp
        'yourMessage'=>'Nhập nội dung liên hệ', // Nôi dung thông điệp
        'sendNow'=> 'GỬI LIÊN HỆ', // Gửi ngay
        'successCreateRequest'=>'GỬI YÊU CẦU THÀNH CÔNG', // Gửi yêu cầu thành công!
    'errorRequire'=> [
        'fullName' => 'Họ & tên là bắt buộc', // Họ & tên là bắt buộc
        'email'=> 'Email  là bắt buộc', //Email  là bắt buộc
        'phoneNumber'=>'Số điện thoại là bắt buộc', //Số điện thoại là bắt buộc
        'titleOfMessageR'=>'Nội dung liên hệ bắt buộc nhập', //Bắt buộc điền
        'yourMessage'=>'Nội dung liên hệ bắt buộc nhập' //Bắt buộc điền
        ],
    'errorValidate'=> [
            'phoneNumber'=>'Số điện thoại không hợp lệ', // Số điện thoại không hợp lệ
	        'Email' => 'Email không hợp lệ' , //Email không hợp lệ
            'titleOfMessager'=>'Nội dung liên hệ bắt buộc nhập' //Bắt buộc điền
            ],
        ],

	'tuyendungPage'=> [
		'postionName'=>'VỊ TRÍ TUYỂN DỤNG',
		'numnber'=>'SỐ LƯỢNG',
		'locationWork'=>'NƠI LÀM VIỆC',
        'registerDate' => 'NGÀY ĐĂNG',
        'tuyendung' =>'TUYỂN DỤNG',
        'quytrinhsanxuat'=>'QUY TRÌNH SẢN XUẤT',
        'descriptionWork'=>'Mô tả công việc',
        'applyform'=>'Nộp đơn ứng tuyển',
        'uploadFile'=>'Tải lên hồ sơ của bạn',
        'uploadFileSize'=>'Sử dụng định dạng: docx, pdf (5Mb)',
        'sendButton'=>'Gửi Hồ Sơ',

	'errorRequire'=> [
		'fileNotSelect' => 'Bạn chưa upload hồ sơ của bạn, vui lòng upload trước khi submit', // Họ & tên là bắt buộc

		],
	'errorValidate'=> [
			'phoneNumber'=>'Số điện thoại không hợp lệ', // Số điện thoại không hợp lệ
			'Email' => 'Email không hợp lệ' , //Email không hợp lệ
			'titleOfMessager'=>'Nội dung liên hệ bắt buộc nhập' //Bắt buộc điền
			],
		],



    'footerPage' => [
        'followOn'=>'THEO DÕI CHÚNG TÔI TRÊN:',
        'banquyen' => 'Bản quyền thuộc về Công Ty Cổ Phần Thiên Hà Kameda ',
        'footerText' => 'Thiên Hà Kameda đã vinh dự nhận được các chứng nhận về HT an toàn thực phẩm 22000, giấy chứng nhận HACCP 2003, ISO 22000:2005, FSSC2200.',
    ]
];
