<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */


    'homePage' => [
        'mission' => 'ミッション',
        'viewDetail' =>'詳細を表示',
        'titleContact'=>'お問い合わせ',
    ],
    'aboutusPage' => [
        'licenesTextDiplay' => '品質証明',
        'numberImpress'=>'実績'

    ],

    'productPage' => [
        'description' => '製品説明：',
        'khamphahuongvikhac' => '他のフレーバーをもっと発見する
        ',
        'projectTitleText' => '開発実績',
        'HeadOfficeLable' => '本社',
        'techologyTitle'=>'技術スタック',
        'introductionM2'=>'M2 Holdingsについて'
    ],


    'contactPage'=> [

        'textTitle'=>'連絡先',
        'emptyClick'=>'（クリックして閉じる',
        'titleform' => '指示を送信する',
        'fullName' => 'フルネーム', // Họ & tên
        'PhoneNumber'=>'電話番号', // Số điện thoại
        'Eamil'=>'Eメール', // Email
        'location'=> 'ロケーション',
        'titleOfMessage'=>'メッセージ', // Thông điệp
        'yourMessage'=>'連絡先テキストを入力してください', // Nôi dung thông điệp
        'sendNow'=> '連絡先を送信する', // Gửi ngay
        'successCreateRequest'=>'リクエストを正常に送信する', // Gửi yêu cầu thành công!
    'errorRequire'=> [
        'fullName' => 'フルネームが必要です', // Họ & tên là bắt buộc
        'email'=> 'メールが必要です', //Email  là bắt buộc
        'phoneNumber'=>'電話番号が必要です', //Số điện thoại là bắt buộc
        'titleOfMessageR'=>'お問い合わせ強制入場', //Bắt buộc điền
        'yourMessage'=>'お問い合わせ強制入場' //Bắt buộc điền
        ],
    'errorValidate'=> [
            'phoneNumber'=>'無効な電話番号', // Số điện thoại không hợp lệ
	        'Email' => '無効なメール' , //Email không hợp lệ
            'titleOfMessager'=>'お問い合わせ強制入場' //Bắt buộc điền
            ],
        ],

	'tuyendungPage'=> [
		'postionName'=>'欠員',
		'numnber'=>'量',
		'locationWork'=>' 職場',
        'registerDate' => '提出日',
        'tuyendung' =>'募集',
        'quytrinhsanxuat'=>'生産工程',
        'descriptionWork'=>'仕事内容',
        'applyform'=>'申請書の提出',
        'uploadFile'=>'プロフィールをアップロードする',
        'uploadFileSize'=>'次の形式を使用します：docx、pdf（5Mb）',
        'sendButton'=>'レコードを送信する',


	'errorRequire'=> [
		'fileNotSelect' => ' プロフィールをまだアップロードしていません。送信する前にアップロードしてください', // Họ & tên là bắt buộc

		],
	'errorValidate'=> [
			'phoneNumber'=>'無効な電話番号', // Số điện thoại không hợp lệ
			'Email' => ' 無効なメール' , //Email không hợp lệ
			'titleOfMessager'=>'お問い合わせ強制入場' //Bắt buộc điền
			],
		],



    'footerPage' => [
        'followOn'=>'フォローしてください',
        'banquyen' => '著作権は共同株式会社に帰属します Thien Ha Kameda ',
        'footerText' => '品質証明: ISO2200, HACCP 2003, ISO 22000:205 ',
    ]
];
