@php
    $bannerHomePage = Cache::get("bannerHomePage");
    $routerName = Route::currentRouteName();
    $bannerIchiPage = Cache::get("bannerIchiPage");
    $bannerSnackePage  = Cache::get("bannerSnackePage");
    $bannerichikid = Cache::get("bannerichikid");

    $bannerichiCraker = Cache::get('bannerichiCraker');
    $allSeoObject = Cache::get("SeoPage",[]);

    $currentSeo = null;
    if ($allSeoObject != null  ) {
        $currentSeo = $allSeoObject->first();

foreach ($allSeoObject as $seoItem) {
    if($seoItem->keyScreen == $routerName)
    {
    $currentSeo = $seoItem;
    break;
    }
}

if (Voyager::translatable($currentSeo)) {
    $currentSeo = $currentSeo->translate(app()->getLocale(), 'vi');
}

    }

@endphp

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @if (empty($__env->yieldContent('headerCustom')))
				@if ($currentSeo != null )
						<title>{{$currentSeo->titlePage}}</title>
						<meta charset="utf-8">
						<meta http-equiv="X-UA-Compatible" content="IE=edge">

						<meta name="description" content="Tsingtao Vietnam">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<link rel="icon" href="/images/favicon.png" type="image/png" sizes="any">
						<link rel="stylesheet" type="text/css" href="/styles/bootstrap4/bootstrap.min.css">
						<link href="/plugins/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
						<link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
						<link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
						<link rel="stylesheet" href="/plugins/plyr/plyr.css" />
						<link rel="stylesheet" type="text/css" href="/plugins/slicknav/slicknav.min.css">
						<link rel="stylesheet" type="text/css" href="/styles/main_styles.css">


						<meta property="og:title" content="{{$currentSeo->titlePage}}">
						<meta property="og:description" content="{{$currentSeo->shortDescription}}">
						<meta property="og:image" content="{{Voyager::image($currentSeo->image)}}">
						<meta property="og:url" content="{{url()->current()}}">
						<meta name="csrf-token" content="{{ csrf_token() }}">
				@else
						<title>Tsingtao - Home</title>
						<meta charset="utf-8">
						<meta http-equiv="X-UA-Compatible" content="IE=edge">
						<meta name="description" content="Tsingtao Vietnam">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<link rel="icon" href="images/favicon.png" type="/image/png" sizes="any">
						<link rel="stylesheet" type="text/css" href="/styles/bootstrap4/bootstrap.min.css">
						<link href="/plugins/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
						<link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
						<link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
						<link rel="stylesheet" href="/plugins/plyr/plyr.css" />
						<link rel="stylesheet" type="text/css" href="/plugins/slicknav/slicknav.min.css">
						<link rel="stylesheet" type="text/css" href="/styles/main_styles.css">
				@endif


        @else

				@yield('headerCustom')


        @endif

				@yield('css')
    </head>

<body>

	<div class="super_container">

		@include('headerLogo')
		<!-- End / header -->
		<div class="main_content">

            @yield('content')


		<!-- Footer -->

		@include('footer')
    </div>


	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="/styles/bootstrap4/popper.js"></script>
	<script src="/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="/plugins/slicknav/jquery.slicknav.js"></script>
	<script src="/plugins/greensock/gsap.min.js"></script>
	<script src="/plugins/greensock/ScrollTrigger.min.js"></script>
	<script src="/js/menu.min.js"></script>
	<script src="/js/jquery.waterwheelCarousel.js"></script>
	<script src="/plugins/plyr/plyr.js"></script>
	<script src="/js/custom.js"></script>

	{{-- <script>

		$( document ).ready(function() {

			if (typeof localStorage !== 'undefined') {
			var x = localStorage.getItem('isProcess');
			if(x==null)
			{
				window.location.href = 'age.html';

			} else {


			}
			}


		});
	</script> --}}



@yield('js')

<script>
var resources = {!! json_encode(__('resourcesLang')) !!};

document.getElementById("{{Route::currentRouteName()}}").classList.add("current-menu-item");
</script>
</body>
</html>
