@extends('layout')
@php
@endphp

@section('css')
@endsection

@php

@endphp

@section('headerCustom')
<title>{{$data->title}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="description" content="Tsingtao Vietnam">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="/images/favicon.png" type="image/png" sizes="any">
<link rel="stylesheet" type="text/css" href="/styles/bootstrap4/bootstrap.min.css">
<link href="/plugins/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" href="/plugins/plyr/plyr.css" />
<link rel="stylesheet" type="text/css" href="/plugins/slicknav/slicknav.min.css">
<link rel="stylesheet" type="text/css" href="/styles/main_styles.css">


<meta property="og:title" content="{{$data->titleSeo}}">
<meta property="og:description" content="{{$data->descriptionSEO}}">
<meta property="og:image" content="{{Voyager::image($data->linkImageShare)}}">
<meta property="og:url" content="{{url()->current()}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')

<div class="product-banner">
    <div class="banner-background" style="background-image:url({{Voyager::image($data->ImageBanner)}})"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-12 banner-content">
                <h2 class="banner-title"> {{$data->title}}</h2>
                <p> {{$data->shortDescriptionTitle}}</p>
               {!!$data->shortDescriptionBanner!!}
            </div>
        </div>
    </div>
</div>
<!-- Product Detail -->
<div class="product-detail">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="col-md-6 col-sm-12 col-12 product-content">
                <h2>THÔNG TIN SẢN PHẨM</h2>
                {!!$data->content!!}
            </div>
            <div class="col-md-6 col-sm-12 col-12 product-image">
                <img src="{{Voyager::image($data->imageContent)}}" alt="">
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12 col-12">
                <div class="seemore_button text-center">
                    <div class="button">
                        <a href="/san-pham">Sản phẩm khác</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
@endsection

