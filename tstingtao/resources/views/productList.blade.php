@extends('layout')

@section('contentPage')


        <div class="products_slider">
            <div class="row no-gutters">
                <ul class="nav nav-tabs row no-gutters w-100" id="productsTab" role="tablist">
                    @foreach ($allCategory as $itemCategory)
                    @php
                    if (Voyager::translatable($itemCategory)) {
                     $itemCategory = $itemCategory->translate(app()->getLocale(), 'vi');
                          }
                   @endphp
                     @php
                        $allChile = $itemCategory->ListProduct;
                        $countarray = count($allChile);

                     @endphp

                        @if ($countarray>0)
                        <li class="nav-item col-md-3 col-sm-6 col-6 product_slide_item">
                            @if ($loop->index==0)
                            <a class="{{($loop->index+1) == $priorities ? 'active':''}}" id="bk{{$itemCategory->id}}-tab"
                                data-toggle="tab" href="#bk{{$itemCategory->id}}" role="tab" aria-controls="bk{{$itemCategory->id}}" aria-selected="true">
                            @else
                            <a class="{{($loop->index+1) == $priorities ? 'active':''}}"  id="bk{{$itemCategory->id}}-tab"
                                data-toggle="tab" href="#bk{{$itemCategory->id}}" role="tab" aria-controls="bk{{$itemCategory->id}}" aria-selected="true">
                            @endif

                                    <div class="ratio-3-4" style="{{$itemCategory->codeColorProductCategory}}">
                                        <div class="ratio-content">
                                            @if ($itemCategory->ProductCategoryImage =="")
                                            <img class="product_slide_image wow fadeInUp" data-wow-delay="0s" src="{{Voyager::image($itemCategory->backgroundImamge)}}">
                                            @else
                                            <img class="product_slide_image wow fadeInUp" data-wow-delay="0s" src="{{Voyager::image($itemCategory->ProductCategoryImage)}}">
                                            @endif
                                            <h2 class="product_slide_item_title">{{$itemCategory->title}}</h2>
                                        </div>
                                    </div>
                                    <div class="arrow-bottom"></div>
                                </a>
                            </li>
                        @else

                        <li class="nav-item col-md-3 col-sm-6 col-6 product_slide_item">
                            <div class="ratio-3-4" style="{{$itemCategory->codeColorProductCategory}}">
                                <a class="ratio-content"  href="/danh-muc-san-pham/{{$itemCategory->slug}}">
                                    @if ($itemCategory->ProductCategoryImage =="")
                                    <img class="product_slide_image wow fadeInUp" data-wow-delay=".4s" src="{{Voyager::image($itemCategory->backgroundImamge)}}">
                                    @else
                                    <img class="product_slide_image wow fadeInUp" data-wow-delay="0s" src="{{Voyager::image($itemCategory->ProductCategoryImage)}}">
                                    @endif

                                    <h2 class="product_slide_item_title">{{$itemCategory->title}}</h2>
                                </a>
                            </div>
                        </li>

                        @endif

                   @endforeach

                  </ul>
                  <div class="tab-content col-sm-12 col-12" id="myTabContent">
                      @foreach ($allCategory as $itemCategory1)


                    @php
                    if (Voyager::translatable($itemCategory1)) {
                    $itemCategory1 = $itemCategory1->translate(app()->getLocale(), 'vi');
                        }
                    @endphp
                      @php
                          $allChile1 = $itemCategory1->ListProduct;
                      @endphp
                      @if ($loop->index ==0)
                    <div class="tab-pane fade show  {{($loop->index+1) == $priorities ? 'active':''}}" id="bk{{$itemCategory1->id}}" role="tabpanel" aria-labelledby="banhgao-tab">
                      @else
                      <div class="tab-pane fade show {{($loop->index+1) == $priorities ? 'active':''}}  "  id="bk{{$itemCategory1->id}}" role="tabpanel" aria-labelledby="banhgao-tab">
                      @endif

                        <div class="product_section" style="background-image: url(images/bg_pattern_gray.png); background-size: contain; ">
                            <div class="container">
                                <h3 class="product_sub_title">{{$itemCategory1->titleDisplay}}</h3>
                                <div class="row product_list">
                                    @foreach ($allChile1 as $item)

                                    @php

                                if (Voyager::translatable($item)) {
                                    $item = $item->translate(app()->getLocale(), 'vi');
                                        }

                                    @endphp

                                    <div class="col-md-4 col-sm-6 col-12">

                                        <a href="/chi-tiet-san-pham/{{$item->slug}}">
                                            <div class="product_list_item wow fadeInUp" data-wow-delay="0s">
                                                <img src="{{Voyager::image($item->backgroundImamge)}}">
                                                <div class="product_list_item_title">
                                                    {{$item->title}}
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    @endforeach



                                </div>
                            </div>
                        </div>

                    </div>


                      @endforeach


                  </div>
            </div>
        </div>
    </div>

@endsection
