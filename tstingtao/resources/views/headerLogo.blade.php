
<!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <div class="offcanvas__logo">
            <a href="index.html"><img src="/images/logo.svg" alt=""></a>
        </div>
        <div id="mobile-menu-wrap"></div>
        <div class="languages">
            <a href="comingsoon.html">EN</a>
        </div>
    </div>
    <!-- Offcanvas Menu End -->

    <!-- header -->
    @if (App::isLocale('vi'))
    <header class="header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-12 col-12 nav-menu-container">
                    <div class="header__logo"><a href="index.html"><img src="/images/logo.svg" alt="" /></a></div>
                    <div class="header__right">
                        <nav class="main-nav">
                            <!-- main-menu -->
                            <ul class="main-menu">
                                <li id ="homePage" class="home-item ">
                                    <a href="/"><i class="fas fa-home"></i></a>
                                </li>
                                <li id ="aboutus" class="menu-item-has-children">
                                    <a href="/gioi-thieu">Giới thiệu</a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="/gioi-thieu#tsingtao">Tập đoàn Tsingtao</a>

                                        </li>
                                        <li>
                                            <a href="/gioi-thieu#tsingtao-vietnam">Tsingtao Việt Nam</a>
                                        </li>
                                        <li>
                                            <a href="/gioi-thieu#su-menh">Tầm nhìn, Sứ mệnh và Giá trị</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id ="product">
                                    <a href="/san-pham">Các nhãn hiệu</a>
                                </li>
                                <li>
                                    <a href="/phat-trien-ben-vung">Phát triển<br/>bền vững</a>
                                </li>

                                <li id ="career">

                                    <a href="/tuyen-dung">Cơ hội<br />nghề nghiệp</a> </li>


                                    <li id ="news"><a href="/tin-tuc">Tin tức</a>
                                </li>
                                <li id ="contact">
                                    <a href="/lien-he">Liên hệ</a></li>
                                </li>
                                <li class="menu-item-has-children language_nav">
                                    @php
                                    $urlvn = "/locale/vi";
                                    $urljp = "/locale/jp";
                                    $urlusa = "/locale/en";
                                   @endphp

                                    @if (App::isLocale('vi'))


                                   <a href="{{$urlusa}}"><span>EN</span></a>


                                    @else

                                    <a href="{{$urlvn}}"><span>Vi</span></a>

                                    @endif

                                </li>
                                <li>
                                    <div class="search-form">
                                        <a href="javascript:void(0)" class="search-form__toggle"><i class="fas fa-search"></i></a>
                                        <div class="search-form__form">

                                            <!-- form-search -->
                                            <div class="form-search">
                                                <form>
                                                    <input class="form-control" type="text" placeholder="Search text ...">
                                                </form>
                                            </div><!-- End / form-search -->
                                        </div>
                                    </div>
                                </li>
                            </ul><!-- main-menu -->
                            <div class="navbar-toggle"><span></span><span></span><span></span></div>
                        </nav><!-- End / main-nav -->

                    </div>

                </div>
            </div>


        </div>
    </header>
    @else
    <header class="header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-12 col-12 nav-menu-container">
                    <div class="header__logo"><a href="/"><img src="/images/logo.svg" alt="" /></a></div>
                    <div class="header__right">
                        <nav class="main-nav">
                            <!-- main-menu -->
                            <ul class="main-menu">
                                <li id ="homePage" class="home-item ">
                                    <a href="/"><i class="fas fa-home"></i></a>
                                </li>
                                <li id ="aboutus" class="menu-item-has-children">
                                    <a  href="/gioi-thieu">Introduction</a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="/gioi-thieu#tsingtao">Tập đoàn Tsingtao</a>

                                        </li>
                                        <li>
                                            <a href="/gioi-thieu#tsingtao-vietnam">Tsingtao Việt Nam</a>
                                        </li>
                                        <li>
                                            <a href="/gioi-thieu#su-menh">Tầm nhìn, Sứ mệnh và Giá trị</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id ="product">
                                    <a href="/san-pham">Product</a>
                                </li>
                                <li id ="developer">
                                    <a href="/phat-trien-ben-vung">Phát triển<br/>bền vững</a>
                                </li>

                                <li id ="career">

                                    <a href="/tuyen-dung">Cơ hội<br />nghề nghiệp</a>

                                </li>
                                <li id ="news">
                                    <a href="/tin-tuc">News</a>
                                </li>
                                <li id ="contact">
                                    <a href="/lien-he">Contact</a></li>
                                </li>
                                <li class="menu-item-has-children language_nav">
                                    @php
                                    $urlvn = "/locale/vi";
                                    $urljp = "/locale/jp";
                                    $urlusa = "/locale/en";
                                   @endphp

                                    @if (App::isLocale('vi'))


                                    <a href="{{$urlusa}}"><span>EN</span></a>


                                    @else

                                    <a href="{{$urlvn}}"><span>Vi</span></a>

                                    @endif
                                </li>
                                <li>
                                    <div class="search-form">
                                        <a href="javascript:void(0)" class="search-form__toggle"><i class="fas fa-search"></i></a>
                                        <div class="search-form__form">

                                            <!-- form-search -->
                                            <div class="form-search">

                                                <form action="/tim-kiem" method="get" id ="fromSearch">

                                                    <input name = "keysearch"  id ="searchText" class="form-control" type="text" placeholder="Search text ...">
                                                    <input type="submit" id ="submitform" value="Submit" style="display:none">
                                                </form>
                                             </div><!-- End / form-search -->
                                        </div>
                                    </div>
                                </li>
                            </ul><!-- main-menu -->
                            <div class="navbar-toggle"><span></span><span></span><span></span></div>
                        </nav><!-- End / main-nav -->

                    </div>

                </div>
            </div>


        </div>
    </header>
    @endif

    <script>

        var input = document.getElementById("searchText");

        // Execute a function when the user releases a key on the keyboard
        input.addEventListener("keyup", function(event) {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click


        document.getElementById("submitform").click();
        }
        });

    </script>
