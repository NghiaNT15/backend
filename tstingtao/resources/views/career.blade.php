@php
    $carrerObjectPage = Cache::get("carrerObjectPage");


    if (Voyager::translatable($carrerObjectPage)) {
           $carrerObjectPage = $carrerObjectPage->translate(app()->getLocale(), 'vi');
    }


    $languageDisplay  = Cache::get('languageDisplay');
    if (Voyager::translatable($languageDisplay)) {
           $languageDisplay = $languageDisplay->translate(app()->getLocale(), 'vi');
    }



@endphp

@php
    $allBanner  = Cache::get('allBanner');
  $routerName = Route::currentRouteName();
    $currentBanner = null;
    if ($allBanner != null  ) {
    $currentBanner = $allBanner->first();
    }

    foreach ($allBanner as $seoItem) {
    if($seoItem->keyScreen == $routerName)
    {
    $currentBanner = $seoItem;
    break;
    }
    }

    if (Voyager::translatable($currentBanner)) {
        $currentBanner = $currentBanner->translate(app()->getLocale(), 'vi');
    }

@endphp
@extends('layout')

@section('content')
<div class="main_content">
    <div class="page-banner">
        <div class="images" style="background-image: url({{Voyager::image($currentBanner->image)}});">
            <h2>{{$currentBanner->title}}</h2>
        </div>
    </div>
    <!-- Career -->
    <div class="career">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-12 col-12">
                    <div class="career-title">
                        <h2>
                          {{$carrerObjectPage->title}}
                        </h2>
                    </div>
                    <div class="career-text">
                    {!!$carrerObjectPage->contentDescription!!}
                    </div>

                </div>
                <div class="col-lg-7 col-sm-12 col-12 career-image">
                    <img src="images/career-01.jpg" alt="">
                </div>
            </div>
            <div class="row">

                <div class="col-sm-12 col-12 text-center">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>{{ucwords($languageDisplay->carrer_PositionName)}}</th>
                                    <th>{{ucwords($languageDisplay->carrer_Quatinity)}}</th>
                                    <th>{{ucwords($languageDisplay->carrer_WorkLocation)}}</th>
                                    <th>{{ucwords($languageDisplay->carrer_ApplicationDealine)}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allData as $item)


                                <tr>
                                <td data-column="{{ucwords($languageDisplay->carrer_PositionName)}}"><a href="/tuyen-dung/{{$item->slug}}">{{$item->positionName}}</a></td>
                                    <td data-column="{{ucwords($languageDisplay->carrer_Quatinity)}}">{{$item->quatinity}}</td>
                                    <td data-column="{{ucwords($languageDisplay->carrer_WorkLocation)}}">{{$item->locationName}}</td>
                                    <td data-column="{{ucwords($languageDisplay->carrer_ApplicationDealine)}}">{{$item->applicationDealine}}</td>
                                </tr>

                                @endforeach


                            </tbody>
                        </table>

                    </div>
                    <!-- <p class="pt-empty">Không có thông tin tuyển dụng</p> -->
                </div>
            </div>
        </div>
    </div>

</div>
@endsection





