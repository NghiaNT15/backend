<ul class="main-menu">
    @foreach($items as $menu_item)
            @php

            if (Voyager::translatable($menu_item)) {
                $menu_item = $menu_item->translate(app()->getLocale(), 'vi');
            }
        @endphp

        @if($loop->index ==0 )
            <li class="navigation-item">
                <a href="{{ $menu_item->url }}" class="navigation-link-1  homePage">{{ $menu_item->title }}</a>
            </li>
        @endif

        @if($loop->index ==1 )
            <li class="navigation-item">
                <a href="{{ $menu_item->url }}" class="navigation-link-2 careerDetail career">{{ $menu_item->title }}</a>
            </li>
        @endif

        @if($loop->index ==2 )
            <li class="navigation-item">
                <a href="{{ $menu_item->url }}" class="navigation-link-4 contact">{{ $menu_item->title }}</a>
            </li>
        @endif




        @endforeach
</ul>






