@php

    $allStepItem  = Cache::get("allStepItem",[]);


    // $allQuatinityLience = Cache::get("allQuatinityLience");
    // $allNumberImpress = Cache::get("allNumberImpress");
@endphp


@php

    $aboutUspage = Cache::get("aboutUspage");
    $allQuatinityLience = Cache::get("allQuatinityLience");
    $allNumberImpress = Cache::get("allNumberImpress");
@endphp


@php
    $routerName = Route::currentRouteName();
    $allbannerItem = Cache::get("allbannerItem",[]);

    $currentBanner =null;
    foreach ($allbannerItem as $bannerItem) {
        if($bannerItem->keyScreen == $routerName)
        {
                  $currentBanner  = $bannerItem;
                  break;
        }
    }



@endphp
@extends('layout')

@section('contentPage')

	<!-- Home -->

	<!-- About Us -->



    <div class="banner-section">
        <div class="banner-image">
            <img src="images/step/step-banner.jpg" alt="">
        </div>
        <div class="banner-content">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col text-center">
                        <div class="banner_title wow fadeInUp">
                            <h1>{{ __('resourcesLang.tuyendungPage.quytrinhsanxuat') }}</h1>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- Step 1 -->



    @foreach ($allStepItem as $item)
    @php
          if (Voyager::translatable($item)) {
           $item = $item->translate(app()->getLocale(), 'vi');
    }
    @endphp
    @if ($loop->index ==0)
    <div class="step step-1">
       <div class="step-background" style="background-image: url(images/step/bg-section-1.png);"></div>

       <div class="container">
           <div class="row">
               <div class="col-sm-12 col-12 text-center">
                   <div class="step_title wow bounceIn">
                       <img src="images/step/step-circle.png" alt="">
                   <h2>0{{$loop->index+1}}</h2>
                   </div>
               <div class="section_title"><h2>{{$item->title}}</h2></div>
               </div>
           </div>
           <div class="row align-items-center">
               <div class="col-lg-4 col-sm-12 col-12">
                   <div class="step_content">
                       <div class="step-image text-center wow fadeInLeft" data-wow-delay=".2s">
                           <img src="{{Voyager::image($item->avatar)}}" alt="">
                       </div>
                   </div>
               </div>
               <div class="col-lg-8 col-sm-12 col-12">
                   <div class="step_content">
                       <div class="step-text">
                           {!!$item->content!!}
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   @endif

   @if ($loop->index ==1)

   <div class="step step-2">
       <div class="step-2-overlay" style="background-image: url(images/step/bg-section-1_1.png);"></div>
       <div class="container">
           <div class="row">
               <div class="col-sm-12 col-12 text-center">
                   <div class="step_title wow bounceIn">
                       <img src="images/step/step-circle.png" alt="">
                       <h2>0{{$loop->index+1}}</h2>
                   </div>
                   <div class="section_title"><h2>{{$item->title}}</h2></div>
               </div>
           </div>
           <div class="row align-items-center flex-row-reverse">
               <div class="col-lg-4 col-sm-12 col-12">
                   <div class="step_content">
                       <div class="step-image text-center wow fadeInRight" data-wow-delay=".2s">
                           <img src="{{Voyager::image($item->avatar)}}" alt="">
                       </div>
                   </div>
               </div>
               <div class="col-lg-8 col-sm-12 col-12">
                   <div class="step_content">
                       <div class="step-text">
                           {!!$item->content!!}
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

    @endif


    @if ($loop->index ==2)
    <div class="step step-3">
       <div class="step-3-background" style="background-image: url(images/step/bg-section-3.png);"></div>
       <div class="step-3-overlay" style="background-image: url(images/step/bg-section-3_1.png);"></div>
       <div class="container">
           <div class="row">
               <div class="col-sm-12 col-12 text-center">
                   <div class="step_title wow bounceIn">
                       <img src="images/step/step-circle.png" alt="">
                       <h2>0{{$loop->index +1}}</h2>
                   </div>
                   <div class="section_title"><h2>{{$item->title}}</h2></div>
               </div>
           </div>
           <div class="row align-items-center">
               <div class="col-lg-4 col-sm-12 col-12">
                   <div class="step_content">
                       <div class="step-image text-center wow fadeInLeft" data-wow-delay=".2s">
                           <img src="{{Voyager::image($item->avatar)}}" alt="">
                       </div>
                   </div>
               </div>
               <div class="col-lg-8 col-sm-12 col-12">
                   <div class="step_content">
                       <div class="step-text">
                           {!!$item->content!!}
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>


     @endif
    @endforeach



    @endsection

