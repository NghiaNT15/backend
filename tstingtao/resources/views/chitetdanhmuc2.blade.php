<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>{{$data->title}}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="CareMed demo project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/images/favicon.png" type="image/png" sizes="any">
        <link rel="stylesheet" type="text/css" href="/styles/bootstrap4/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/plugins/font-awesome/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="/plugins/animate/animate.css"/>
        <link rel="stylesheet" type="text/css" href="/styles/menu.css">
        <link rel="stylesheet" type="text/css" href="/styles/main_styles.css">
        <link rel="stylesheet" type="text/css" href="/styles/responsive.css">
        @php
        if (Voyager::translatable($data)) {
           $data = $data->translate(app()->getLocale(), 'vi');

         }
         @endphp

        @if ($data->linkImageShare =="")
<meta property="og:image" content="{{Voyager::image($data->backgroundImamge)}}">
@else
<meta property="og:image" content="{{Voyager::image($data->linkImageShare)}}">
@endif


@if ($data->descriptionSEO =="")
<meta property="og:description" content="{{$data->shortDescription}}">
@else
<meta property="og:description" content="{{$data->descriptionSEO}}">
@endif



@if ($data->titleSeo =="")
<meta property="og:title" content="{{$data->title}}">
@else
<meta property="og:title" content="{{$data->titleSeo}}">
@endif


<meta property="og:url" content="{{url()->current()}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
<body>

    <div class="super_container">

        <!-- Header -->
        @include('headerLogo')

        <!-- Menu -->


        @php
            $colorCode = $data->codeColor;
            if($data->codeColor =="")
            {
                $colorCode = "#e41e1f";
            }

            $classCode = $data->classCode;
        @endphp




        <div class="product_detail product_section product_color_kid" style="background-color: #ffe5d4;">
			<div class="product_detail-bg" style="background-image:url(/images/bg_pattern_white.png)"></div>
            <div class="container">
                <h2 class="product_title">{{$data->titleDetail}}</h2>
                <div class="row">
                    <div class="col-lg-6 col-sm-12 col-12">
                        <div class="product_content">
                            <div class="product_des_title">{{ __('resourcesLang.productPage.description') }}</div>
                            <div class="product_des_content">
                                {!!$data->shortDescription!!}

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-12">
                        <div class="product_feature_image">
                            <img src="{{Voyager::image($data->backgroundImamge)}}">
                            <div class="product_weight"><span>{{$data->weight}}</span></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



      @include('footer')
    </div>

    <script src="/js/jquery.min.js"></script>
<script src="/styles/bootstrap4/popper.js"></script>
<script src="/styles/bootstrap4/bootstrap.min.js"></script>
<script src="/plugins/lettering/jquery.lettering.js"></script>
<script src="/plugins/wow/wow.js"></script>
<script src="/plugins/CounterUp/jquery.counterup.js"></script>
<script src="/plugins/waypoint/jquery.waypoints.min.js"></script>

<script src="/js/map.js"></script>
<script src="/js/custom.js"></script>


</body>

</html>
