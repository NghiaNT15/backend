
<ul>
        @foreach($items as $menu_item)

        @php

            if (Voyager::translatable($menu_item)) {
                $menu_item = $menu_item->translate(app()->getLocale(), 'vi');
            }
        @endphp

        <li><a href="{{ $menu_item->url }}">{{ $menu_item->title }}</a></li>

        @endforeach
</ul
