<div class="menu_inner menu_mm">
    <div class="menu menu_mm">
        <ul class="menu_list menu_mm">


            @foreach ($items as $menu_item)
            <li class="menu_item menu_mm"><a href="{{ $menu_item->url }}">{{ $menu_item->title }}</a></li>
            @endforeach

            <li class="menu-item menu_mm language_mm">
                <a href="#"><img src="images/flag-jp.jpg" alt=""></a>
                <a href="#"><img src="images/flag-usa.jpg" alt=""></a>
            </li>

        </ul>
    </div>
</div>
