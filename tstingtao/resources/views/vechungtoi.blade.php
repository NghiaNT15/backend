    @php

    $introductionPage = Cache::get("introductionPage");
    $allQuatinityLience = Cache::get("allQuatinityLience");
    $allNumberImpress = Cache::get("allNumberImpress");
    if (Voyager::translatable($introductionPage)) {
    $introductionPage = $introductionPage->translate(app()->getLocale(), 'vi');
    }
    @endphp

@php
$allBanner  = Cache::get('allBanner');
$routerName = Route::currentRouteName();
$currentBanner = null;
if ($allBanner != null  ) {
$currentBanner = $allBanner->first();
}

foreach ($allBanner as $seoItem) {
if($seoItem->keyScreen == $routerName)
{
$currentBanner = $seoItem;
break;
}
}

if (Voyager::translatable($currentBanner)) {
    $currentBanner = $currentBanner->translate(app()->getLocale(), 'vi');
}


@endphp


@extends('layout')

@section('content')

<!-- End / header -->
<div class="main_content">
<div class="page-banner">
    <div class="images" style="background-image: url({{Voyager::image($currentBanner->image)}});">

    </div>
</div>

    <!-- About us -->

    <div class="about page_section">

        <div class="container">
            <div class="row relative">

                <div class="col-sm-12 col-12">
                    <div class="about_content">
                    <div class="about_title page_anchor" id="tsingtao"><h2>{{$introductionPage->titleIntroduction}}</h2></div>
                        <div class="about_text">
                            {!!$introductionPage->introductionContent!!}
                        </div>

                    </div>
                    <div class="about_content">
                        <div class="about_title page_anchor" id="tsingtao-vietnam" ><h2>{{$introductionPage->tsingTaoVietNamTitle}}</h2></div>
                        <div class="about_text">
                            {!!$introductionPage->tsingTaoVietNamContent!!}
                        </div>

                    </div>
                    <div class="about_content">
                        <div class="about_title page_anchor" id="su-menh"><h2>{{$introductionPage->tsingTaoVietNamMisionTitle}}</h2></div>
                        <div class="about_text">
                            {!!$introductionPage->tsingTaoVietNamMisionContent!!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @endsection
