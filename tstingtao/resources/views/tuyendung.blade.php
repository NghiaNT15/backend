


@php
    $routerName = Route::currentRouteName();
    $allObjectCarrerItem  = Cache::get("allObjectCarrerItem",[]);
    $allbannerItem = Cache::get("allbannerItem",[]);

    $currentBanner =null;
    foreach ($allbannerItem as $bannerItem) {
        if($bannerItem->keyScreen == $routerName)
        {
                  $currentBanner  = $bannerItem;
                  break;
        }
    }



@endphp
@extends('layout')

@section('contentPage')


<div class="banner-section">
    <div class="banner-image" style="background-image: url(/images/career/career-banner.jpg);"></div>
    <div class="banner-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col text-center">

                    <div class="banner_title">
                        <h1>{{ __('resourcesLang.tuyendungPage.tuyendung') }}</h1>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



	<!-- Career -->

	<div class="career">
		<!-- <div class="career-background" style="background-image: url(images/career/bg-section-9.jpg);"></div> -->
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-12 text-center">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>{{ __('resourcesLang.tuyendungPage.postionName') }}</th>
                                    <th>{{ __('resourcesLang.tuyendungPage.numnber') }}</th>
									<th>{{ __('resourcesLang.tuyendungPage.locationWork') }}</th>
									<th>{{ __('resourcesLang.tuyendungPage.registerDate') }}</th>
								</tr>
							</thead>
							<tbody>
                                @foreach ($allObjectCarrerItem as $item)

                                    @php
                                    if (Voyager::translatable($item)) {
                                    $item = $item->translate(app()->getLocale(), 'vi');
                                    }
                                    @endphp
                                <tr>
                                <td data-column="VỊ TRÍ TUYỂN DỤNG"><a href="/tuyen-dung/{{$item->slug}}">{{$item->positionName}}</a></td>
									<td data-column="SỐ LƯỢNG">{{$item->step}}</td>
									<td data-column="NƠI LÀM VIỆC">{{$item->locationName}}</td>
									<td data-column="Lượt bình chọn">

                                        <span>{{\Carbon\Carbon::parse( $item->created_at)->format('d.m.Y')}}</span>

                                    </td>
								</tr>
                                @endforeach

							</tbody>
						</table>

					</div>
					<!-- <p class="pt-empty">Không có thông tin tuyển dụng</p> -->
				</div>
			</div>
		</div>
	</div>

@endsection
