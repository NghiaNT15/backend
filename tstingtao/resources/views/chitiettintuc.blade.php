@php
    $languageDisplay  = Cache::get('languageDisplay');
    if (Voyager::translatable($languageDisplay)) {
           $languageDisplay = $languageDisplay->translate(app()->getLocale(), 'vi');
    }


@endphp

@extends('layout')



@php
    $allBanner  = Cache::get('allBanner');
  $routerName = Route::currentRouteName();
    $currentBanner = null;
    if ($allBanner != null  ) {
    $currentBanner = $allBanner->first();
    }

    foreach ($allBanner as $seoItem) {
    if($seoItem->keyScreen == $routerName)
    {
    $currentBanner = $seoItem;
    break;
    }
    }

    if (Voyager::translatable($currentBanner)) {
        $currentBanner = $currentBanner->translate(app()->getLocale(), 'vi');
    }



@endphp

@php
if (Voyager::translatable($data)) {
$data = $data->translate(app()->getLocale(), 'vi');
}
@endphp

@extends('layout')

@section('headerCustom')
<title>{{$data->title}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="description" content="Tsingtao Vietnam">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="/images/favicon.png" type="image/png" sizes="any">
<link rel="stylesheet" type="text/css" href="/styles/bootstrap4/bootstrap.min.css">
<link href="/plugins/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" href="/plugins/plyr/plyr.css" />
<link rel="stylesheet" type="text/css" href="/plugins/slicknav/slicknav.min.css">
<link rel="stylesheet" type="text/css" href="/styles/main_styles.css">


<meta property="og:title" content="{{$data->titleSeo}}">
<meta property="og:description" content="{{$data->shortDescription}}">
<meta property="og:image" content="{{Voyager::image($data->imageShare)}}">
<meta property="og:url" content="{{url()->current()}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')

<!-- End / header -->
<div class="main_content">
    <div class="page-banner">
        <div class="images" style="background-image: url({{Voyager::image($currentBanner->image)}});">
            <h2>{{$currentBanner->title}}</h2>
        </div>
</div>
<!-- News Page -->
<div class="news-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-12">
                {!!$data->content!!}
            </div>
            <div class="col-sm-12 col-12 text-center">
                <div class="button news_button">
                <a href="/tin-tuc">  {{$languageDisplay->new_backtoNew}}</a>
                </div>
            </div>
        </div>
        <div class="row related-news">
            <div class="col-sm-12 col-12">
                <div class="related-news-title">
                    <h2> {{$languageDisplay->new_other}}</h2>
                </div>
            </div>

            @foreach ($dataOther as $item)

            @php
            if (Voyager::translatable($item)) {
            $item = $item->translate(app()->getLocale(), 'vi');
            }
            @endphp
            <div class="col-md-3 col-sm-6 col-12 item">
                <a href="/tin-tuc/{{$item->slug}}">
                    <div class="thumbnail-img" style="background-image: url({{Voyager::image($item->backgroundImage)}});"></div>
                    <div class="text">
                        <p>
                            {{$item->title}}
                        </p>
                    </div>
                </a>
            </div>
            @endforeach


        </div>
    </div>
</div>
@endsection





