

@php
        $languageDisplay  = Cache::get('languageDisplay');
        $contactInfo = Cache::get('contactInfo');
    if (Voyager::translatable($languageDisplay)) {
           $languageDisplay = $languageDisplay->translate(app()->getLocale(), 'vi');
    }
    if (Voyager::translatable($contactInfo)) {
           $contactInfo = $contactInfo->translate(app()->getLocale(), 'vi');
    }

@endphp
@section('css')
<link rel="stylesheet" type="text/css" href="styles/map.css">
@endsection

@php
    $allContactItemm = Cache::get('allContactItem', []);


@endphp


@php
    $allBanner  = Cache::get('allBanner');
  $routerName = Route::currentRouteName();
    $currentBanner = null;
    if ($allBanner != null  ) {
    $currentBanner = $allBanner->first();
    }

    foreach ($allBanner as $seoItem) {
    if($seoItem->keyScreen == $routerName)
    {
    $currentBanner = $seoItem;
    break;
    }
    }

    if (Voyager::translatable($currentBanner)) {
        $currentBanner = $currentBanner->translate(app()->getLocale(), 'vi');
    }



@endphp


@extends('layout')

@section('content')

<!-- End / header -->
<div class="main_content">
    <div class="page-banner">
        <div class="images" style="background-image: url({{Voyager::image($currentBanner->image)}});">
            <h2>{{$currentBanner->title}}</h2>
        </div>
    </div>

<!-- Contact -->
<div class="contact-map">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-12 col-12">
                <div class="section_title">
                <h2>{{$contactInfo->fullName}}</h2>
                </div>
                <div class="contact-info">
                    <div class="icon">
                        <i class="fas fa-home"></i>
                    </div>
                    <p>{{$contactInfo->Address}}</p>
                </div>
                <div class="contact-info">
                    <div class="icon">
                        <i class="fas fa-phone-square-alt"></i>
                    </div>
                    <p>{{$contactInfo->phone1}}</p>
                </div>
                <div class="contact-info">
                    <div class="icon">
                        <i class="fas fa-mobile-alt"></i>
                    </div>
                    <p>{{$contactInfo->phone2}}</p>
                </div>
                <div class="contact-info">
                    <div class="icon">
                        <i class="fas fa-fax"></i>
                    </div>
                    <p>{{$contactInfo->phone3}}</p>
                </div>
                <div class="contact-info">
                    <div class="icon">
                        <i class="fas fa-envelope"></i>
                    </div>
                    <p>{{$contactInfo->email}}</p>
                </div>
                <div class="contact-info">
                    <div class="icon">
                        <i class="fas fa-globe-asia"></i>
                    </div>
                    <p>{{$contactInfo->website}}</p>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 col-12">

               {!!$contactInfo->googlemap!!}
            </div>
        </div>

    </div>
</div>


<div class="contact-form">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-12 hideFormContact">
                <div class="section_title ">
                    <h2>{{$languageDisplay->contactinfo}}</h2>
                </div>
            </div>

            <div class="col-sm-12 col-12 resultRequest" style="display: none">
                <div class=" section_title">
                    <h2>{{ __('resourcesLang.contactPage.successCreateRequest') }}</h2>
                </div>
            </div>




            <div class="col-sm-12 col-12">
                <div class="contact_form_container hideFormContact">

                    <form id="contactForm" class=" contact_form needs-validation" onsubmit="return false"  novalidate data-wow-duration="1s" data-wow-delay=".5s">
                        {{ csrf_field()}}

                        <div class="input_container input_name"><input type="text" class="contact_input" id ="txtName"  placeholder="{{ __('resourcesLang.contactPage.fullName') }}" name="fullName" required="required"></div>
                        <div class="row">
                            <div class="col-md-6 input_col">
                                <div class="input_container"><input type="number"  id ="txtPhone" class="contact_input" placeholder="{{ __('resourcesLang.contactPage.PhoneNumber') }}" name="phoneNumber" required="required"></div>
                            </div>
                            <div class="col-md-6 input_col">
                                <div class="input_container"><input type="email" id  ="txtEmail"  class="contact_input" placeholder="Email" name ="email" required="required"></div>
                            </div>
                        </div>
                        <div class="input_container"><textarea class="contact_input contact_text_area" id ="txtMessage"  name="content" placeholder="{{ __('resourcesLang.contactPage.yourMessage') }}" required="required"></textarea></div>
                        <div class="form_submit text-center">
                            <button class="button contact_button"><a href="javascript:Void(0)" onclick="sendRequest()">{{ __('resourcesLang.contactPage.sendNow') }}</a></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('js')


@endsection

