@php
$allSeoObject = Cache::get("SeoPage");
$allBannerPage = Cache::get("allBanner");
$allSlideBanner = Cache::get("allSlideBanner");
$routerName = Route::currentRouteName();
$currentSeo = $allSeoObject[0];
$companyInfo = Cache::get("companyInfo");
$introlduction = Cache::get("introlduction");

foreach ($allSeoObject as $seoItem) {

 if($seoItem->keyScreen == $routerName)
  {
      $currentSeo = $seoItem;
    break;
  }
}
foreach ($allBannerPage as $bannerItem) {
if($bannerItem->keyScreen == $routerName)
 {
     $currentBanner  = $bannerItem;
   break;
 }
}
if (Voyager::translatable($currentSeo)) {
     $currentSeo = $currentSeo->translate(app()->getLocale(), 'vi');
 }
 if (Voyager::translatable($bannerItem)) {
     $bannerItem = $bannerItem->translate(app()->getLocale(), 'vi');
 }
 if (Voyager::translatable($bannerItem)) {
     $bannerItem = $bannerItem->translate(app()->getLocale(), 'vi');

}

 if (Voyager::translatable($companyInfo)) {
     $companyInfo = $companyInfo->translate(app()->getLocale(), 'vi');
 }

@endphp


{{--
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <title>{{$currentSeo->titlePage}}</title>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="CareMed demo project">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.png" type="image/png" sizes="any">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="plugins/font-awesome/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="plugins/animate/animate.css"/>
	<link rel="stylesheet" type="text/css" href="styles/menu.css">
	<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
    <link rel="stylesheet" type="text/css" href="styles/responsive.css">


    <meta property="og:title" content="{{$currentSeo->titlePage}}">
    <meta property="og:description" content="{{$currentSeo->shortDescription}}">
    <meta property="og:image" content="{{Voyager::image($currentSeo->image)}}">
    <meta property="og:url" content="{{url()->current()}}">
    <link rel="icon" type="image/svg+xml" href="/favicon.svg">
</head> --}}
<head>
	<title>Bánh Gạo Ichi</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="CareMed demo project">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.png" type="image/png" sizes="any">
	<link rel="stylesheet" type="text/css" href="/styles/bootstrap4/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/plugins/font-awesome/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="/plugins/animate/animate.css"/>
	<link rel="stylesheet" type="text/css" href="/styles/menu.css">
	<link rel="stylesheet" type="text/css" href="/styles/main_styles.css">
	<link rel="stylesheet" type="text/css" href="/styles/responsive.css">
</head>
