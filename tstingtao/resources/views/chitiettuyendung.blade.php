@php
        $languageDisplay  = Cache::get('languageDisplay');
    if (Voyager::translatable($languageDisplay)) {
           $languageDisplay = $languageDisplay->translate(app()->getLocale(), 'vi');
    }


@endphp
@extends('layout')


@php
    $allBanner  = Cache::get('allBanner');
  $routerName = Route::currentRouteName();
    $currentBanner = null;
    if ($allBanner != null  ) {
    $currentBanner = $allBanner->first();
    }

    foreach ($allBanner as $seoItem) {
    if($seoItem->keyScreen == $routerName)
    {
    $currentBanner = $seoItem;
    break;
    }
    }

    if (Voyager::translatable($currentBanner)) {
        $currentBanner = $currentBanner->translate(app()->getLocale(), 'vi');
    }


@endphp


@extends('layout')

@section('content')

<!-- End / header -->
<div class="main_content">
    <div class="page-banner">
        <div class="images" style="background-image: url({{Voyager::image($currentBanner->image)}});">
            <h2>{{$currentBanner->title}}</h2>
        </div>
    </div>
<div class="career career-detail">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-12">
                <div class="section_title">
                    <h2>{{$data->positionName}}</h2>
                </div>
            </div>
        </div>
        <div class="row career-content no-gutters">
            <div class="col-lg-4 col-sm-12 col-12">
                <div class="career-info">
                    <i class="fas fa-user circle-icon"></i>
                    <div class="text">
                    <p>{{$languageDisplay->carrer_Quatinity}}</p>
                        <span>0{{$data->quatinity}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-12">
                <div class="career-info">
                    <i class="fas fa-home circle-icon"></i>
                    <div class="text">
                        <p>{{$languageDisplay->carrer_WorkLocation}}</p>
                        <span>{{$data->locationName}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-12">
                <div class="career-info">
                    <i class="fas fa-calendar-alt circle-icon"></i>
                    <div class="text">
                        <p>{{$languageDisplay->carrer_ApplicationDealine}}</p>
                        <span>{{$data->applicationDealine}}</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-12 career-text">
                    {!!$data->content!!}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-12 text-center">
                <div class="button career_button">
                    <a href="/tuyen-dung">{{$languageDisplay->btnSeeMore}}</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
