<!DOCTYPE html>
@php
if (Voyager::translatable($data)) {
   $data = $data->translate(app()->getLocale(), 'vi');

 }



@endphp
<html lang="vi">
<head>
	<title>{{$data->title}}</title>
	<meta charset="UTF-8">
	<meta name="description" content="{{$data->shortDescription}}">
	<meta name="keywords" content="photo, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Stylesheets -->
	<link rel="stylesheet" href="/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"/>
	<link rel="stylesheet" href="/css/owl.carousel.min.css"/>

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="/sass/style.css"/>


	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta property="og:title" content="{{$data->titleSeo}}">
    <meta property="og:description" content="{{$data->shortDescription}}">
    <meta property="og:image" content="{{Voyager::image($data->imageLinkShare)}}">
    <meta property="og:url" content="{{url()->current()}}">

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	@include("menu")

	<!-- Header section -->
    @include('headerLogo')
	<!-- Header section end -->

	<!-- Hero section -->



    @include('bannerPage')
	<!-- Hero section end -->

	<!-- Career Details  section -->
	<section class="careerDetails-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-12">
					<div class="main_content fadeInUp" animation-delay="0.2">
						<div class="entry-title"><h2>{{$data->title}} </h2></div>
						<div class="post-date"><p>{{ __('resourcesLang.CarrerPage.registerDate') }} {{\Carbon\Carbon::parse( $data->created_at)->format('d.m.Y')}}</p></div>
						<div class="entry-content">
                            {!!$data->content!!}

							<div class="btn-jobs">
                                @if (App::isLocale('vi'))
                                <a href="/tuyen-dung" class="genric-btn">{{ __('resourcesLang.CarrerPage.listJob') }}</a>
                                @else
                                <a href="/career" class="genric-btn">{{ __('resourcesLang.CarrerPage.listJob') }}</a>
                                @endif

							</div>

						</div>

					</div>
				</div>

			</div>
		</div>
	</section>


	@include("footer")

	<!--====== Javascripts & Jquery ======-->
    <script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/owl.carousel.min.js"></script>
	<script src="/js/ScrollTrigger.min.js"></script>
	<script src="/js/gsap.min.js"></script>
	<script src="/js/main.js"></script>
	<script>
		$(document).ready(function() {
        TweenMax.set(".project-preview", { width: 0 });

        var tl = new TimelineLite();

        $(document).on("mouseover", ".navigation-item", function(evt) {
            tl = new TimelineLite();
            tl.to($(".project-preview"), 1, {
              width: "100%",
              ease: "expo.inOut"
            });
          }).on("mouseout", ".navigation-item", function(evt) {
            tl = new TimelineLite();
            tl.to($(".project-preview"), 0.5, {
              width: 0,
              ease: "expo.inOut"
            });
		});

		$(".navigation-link-1").hover(function() {
			$(".project-preview").css({ "background-image": "url(img/team-1.jpg)" });
		});

		$(".navigation-link-2").hover(function() {
			$(".project-preview").css({ "background-image": "url(img/team-2.jpg)" });
		});

		$(".navigation-link-3").hover(function() {
			$(".project-preview").css({ "background-image": "url(img/team-3.jpg)" });
		});

		gsap.registerPlugin(ScrollTrigger);
		gsap.fromTo('.project_image_right ',
			{
				opacity:0,
				x: 100
			},
			{
				scrollTrigger: {
					trigger: '.project_image_right',
					start: "-100px center",
					markers: false,
					toggleActions: "play"
				},
				duration: 1,
				opacity:1,
				x: 0,
			}
		),
		gsap.fromTo('.project_image_left',
			{
				opacity:0,
				x: -100
			},
			{
				scrollTrigger: {
					trigger: '.project_image_left',
					start: "-100px center",
					markers: false,
					toggleActions: "play"
				},
				duration: 1,
				opacity:1,
				x: 0,
			}
		)
		gsap.fromTo('.project_image_right .block-revealer_element',
			{
				scaleX:1,
			},
			{
				scrollTrigger: {
					trigger: '.project_image_right',
					start: "-100px center",
					markers: false,
					toggleActions: "play"
				},
				duration: 1,
				scaleX: 0,
				transformOrigin: 'right',
				delay: 1
			}
		),
		gsap.fromTo('.project_image_left .block-revealer_element',
			{
				scaleX:1,
			},
			{
				scrollTrigger: {
					trigger: '.project_image_left',
					start: "-100px center",
					markers: false,
					toggleActions: "play"
				},
				duration: 1,
				scaleX: 0,
				transformOrigin: 'left',
				delay: 1
			}
		)
		gsap.fromTo('.mission_content-overlay',
			{
				width: 0,
				scaleX:1,
			},
			{
				scrollTrigger: {
					trigger: '.mission_content-overlay',
					start: "-100px center",
					markers: false,
					toggleActions: "play"
				},
				duration: 1,
				width: '100%',
				transformOrigin: 'left',
				delay: 0.4
			}
		)

		$(".fadeInUp").each(function(i) {
			$delay = $(this) .attr('animation-delay');
			$delayVal = ($delay) ? $delay : 0;
			gsap.fromTo($(this),
				{

					y: 50
				},
				{
					scrollTrigger: {
						trigger: $(this),
						start: "-120px center",
						markers: false,
						toggleActions: "play"
					},
					duration: 1, opacity: 1, y:0, delay: $delayVal
				}
			)

		});

	  });



	</script>
	</body>
</html>
