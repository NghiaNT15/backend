<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>{{$data->title}}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="CareMed demo project">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/images/favicon.png" type="image/png" sizes="any">
        <link rel="stylesheet" type="text/css" href="/styles/bootstrap4/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/plugins/font-awesome/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="/plugins/animate/animate.css"/>
        <link rel="stylesheet" type="text/css" href="/styles/menu.css">
        <link rel="stylesheet" type="text/css" href="/styles/main_styles.css">
        <link rel="stylesheet" type="text/css" href="/styles/responsive.css">

        @php
        if (Voyager::translatable($data)) {
           $data = $data->translate(app()->getLocale(), 'vi');

         }
         @endphp

        @if ($data->linkImageShare =="")
<meta property="og:image" content="{{Voyager::image($data->backgroundImamge)}}">
@else
<meta property="og:image" content="{{Voyager::image($data->linkImageShare)}}">
@endif


@if ($data->descriptionSEO =="")
<meta property="og:description" content="{{$data->shortDescription}}">
@else
<meta property="og:description" content="{{$data->descriptionSEO}}">
@endif



@if ($data->titleSeo =="")
<meta property="og:title" content="{{$data->title}}">
@else
<meta property="og:title" content="{{$data->titleSeo}}">
@endif


<meta property="og:url" content="{{url()->current()}}">
<meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
<body>

    <div class="super_container">

        <!-- Header -->
        @include('headerLogo')

        <!-- Menu -->

        <div class="menu_container menu_mm">
            <div class="top_container"></div>
            <div class="container">
                <div class="header_container">
                    <div class="row align-items-center">
                        <div class="col">
                            <!-- Menu Brand Logo -->
                            <a class="menu_brand_container" href="index.html">
                                <div class="logo_brand">
                                    <img src="/images/logo.png" alt="">
                                </div>
                            </a>
                        </div>
                        <div class="col">
                            <!-- Menu Close Button -->
                            <div class="menu_close_container">
                                <div class="menu_close"><i class="fas fa-times"></i></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- Menu Items -->
            <div class="menu_inner menu_mm">
                <div class="menu menu_mm">
                    <ul class="menu_list menu_mm">
                        <li class="menu_item menu_mm"><a href="index.html">Trang chủ</a></li>
                        <li class="menu_item menu_mm"><a href="about_us.html">Về chúng tôi</a></li>
                        <li class="menu_item menu_mm"><a href="step.html">Quy trình sản xuất</a></li>
                        <li class="menu_item menu_mm"><a href="products.html">Sản phẩm</a></li>
                        <li class="menu_item menu_mm"><a href="career.html">Tuyển dụng</a></li>
                        <li class="menu_item menu_mm"><a href="contact.html">Liên hệ</a></li>
                        <li class="menu-item menu_mm language_mm">
                            <a href="#"><img src="/images/flag-jp.jpg" alt=""></a>
                            <a href="#"><img src="/images/flag-usa.jpg" alt=""></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @php
            $colorCode = $data->codeColor;
            if($data->codeColor =="")
            {
                $colorCode = "#e41e1f";
            }

            $classCode = $data->classCode;
        @endphp


        <div class="product_detail product_section {{$classCode}} " style="background-color: {{$colorCode}};">
            <div class="container">
                <div class="row">
                   <div class="col-sm-12 col-12">
                   <h2 class="product_title">{{$data->titleDetail}}</h2>
                   </div>
                </div>
                <div class="row relative">




                    <div class="col-lg-5">
                        <div class="product_content">
                            <div class="product_des_title">{{ __('resourcesLang.productPage.description') }}</div>
                            <div class="product_des_content">
                            {!!$data->shortDescription!!}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 product_feature">
                        @foreach ($allProductCategoryImage as $item)


                            @if ($loop->first)
                                <div class="product_feature_image image_1 wow fadeInRight">
                                    <img src="{{Voyager::image($item->backgroundImamge)}}">
                                <div class="product_weight"><span>{{$item->weight}}</span></div>
                                </div>

                                @else
                                <div class="product_feature_image image_2 wow fadeInRight" data-wow-delay=".2s">
                                <img src="{{Voyager::image($item->backgroundImamge)}}">
                                    <div class="product_weight"><span>{{$item->weight}}</span></div>
                                </div>
                             @endif
                        @endforeach


                    </div>
                </div>
            </div>
        </div>




        <div class="product_related product_section" style="background-image: url(images/bg_pattern_gray.png); background-size: contain; ">
            <div class="container">
                <h3 class="product_sub_title">{{ __('resourcesLang.productPage.khamphahuongvikhac') }}</h3>
                <div class="row product_list">
                    @foreach ($allProduct as $item)
                    <div class="col-md-4">
                        <a href="/chi-tiet-san-pham/{{$item->slug}}">
                            <div class="product_list_item wow fadeInUp" data-wow-delay="0s">
                            <img src="{{Voyager::image($item->backgroundImamge)}}">
                                <div class="product_list_item_title">
                                   {{$item->title}}
                                </div>
                            </div>
                        </a>
                    </div>
                   @endforeach





                </div>
            </div>
        </div>
        <!-- Footer -->

      @include('footer')
    </div>
    <script src="/js/jquery.min.js"></script>
<script src="/styles/bootstrap4/popper.js"></script>
<script src="/styles/bootstrap4/bootstrap.min.js"></script>
<script src="/plugins/lettering/jquery.lettering.js"></script>
<script src="/plugins/wow/wow.js"></script>
<script src="/plugins/CounterUp/jquery.counterup.js"></script>
<script src="/plugins/waypoint/jquery.waypoints.min.js"></script>

<script src="/js/map.js"></script>
<script src="/js/custom.js"></script>


</body>

</html>
