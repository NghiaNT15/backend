

@php
$companyInfo = Cache::get("footerInfo");
if (Voyager::translatable($companyInfo)) {
      $companyInfo = $companyInfo->translate(app()->getLocale(), 'vi');
 }
@endphp
<footer class="footer">
    <div class="footer_background" style="background-image: url(/images/footer-bg.jpg);"></div>
    <div class="footer_container">
        <div class="container">
            <div class="row align-items-center">
                <!-- Footer - Links -->
                <div class="col-xl-5 col-lg-7 footer_col">
                    <div class="footer_about">
                    <div class="footer_title">{{$companyInfo->shortName}}</div>
                        <ul class="footer_about_list">
                            <li>
                                <div class="footer_about_icon"><i class="fas fa-phone-alt"></i></div><span>{{$companyInfo->phone1}}</span>
                            </li>

                            <li>
                                <div class="footer_about_icon"><i class="fas fa-envelope"></i></div>
                                <span>{{$companyInfo->email}}</span>
                            </li>
                        </ul>
                        <p>{{$companyInfo->footerTextFind}}</p>
                        <ul class="footer_social">
                            <li><a href="{{$companyInfo->facebooklink}}"><i class="fab fa-facebook-square"></i></a></li>
                            <li><a href="{{$companyInfo->youtubeLink}}"><i class="fab fa-youtube"></i></a></li>
                            <li><a href="{{$companyInfo->instagramlink}}"><i class="fab fa-tiktok"></i></a></li>
                        </ul>
                    </div>
                    <div class="footer_copyright">
                        <p>{{$companyInfo->footerText}}</p>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-5 footer_col">
                    <div class="footer_image text-lg-center">
                        <img src="/images/footer-image.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
