@php
    $languageDisplay  = Cache::get('languageDisplay');
    if (Voyager::translatable($languageDisplay)) {
           $languageDisplay = $languageDisplay->translate(app()->getLocale(), 'vi');
    }





@endphp


@php
    $allBanner  = Cache::get('allBanner');
  $routerName = Route::currentRouteName();
    $currentBanner = null;
    if ($allBanner != null  ) {
    $currentBanner = $allBanner->first();
    }

    foreach ($allBanner as $seoItem) {
    if($seoItem->keyScreen == $routerName)
    {
    $currentBanner = $seoItem;
    break;
    }
    }

    if (Voyager::translatable($currentBanner)) {
        $currentBanner = $currentBanner->translate(app()->getLocale(), 'vi');
    }



@endphp


@extends('layout')

@section('content')

<!-- End / header -->
<div class="main_content">
    <div class="page-banner">
        <div class="images" style="background-image: url({{Voyager::image($currentBanner->image)}});">
            <h2>{{$currentBanner->title}}</h2>
        </div>
    </div>
    <!-- News Page -->
    <div class="news page_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-sm-12 col-12">
                    <div class="news_title">
                        <h2>
                            {{$languageDisplay->new_lasted}}
                        </h2>
                    </div>

                    <div class="list_news">
                        <div class="large_news">
                            <a href="/tin-tuc/{{$dataSpecial->slug}}">
                                <div class="large_news_image" style="background-image: url({{Voyager::image($dataSpecial->backgroundImage)}});">
                                </div>
                                <div class="large_news_text">
                                    <p> {{$dataSpecial->shortDescription}}</p>
                                </div>
                            </a>
                        </div>

                        @foreach ($dataAll as $item)
                        @php
                        if (Voyager::translatable($item)) {
                        $item = $item->translate(app()->getLocale(), 'vi');
                        }
                        @endphp

                        <div class="news_item">
                            <a href="/tin-tuc/{{$item->slug}}">
                                   <div class="news_item_image" style="background-image: url(/images/news/hinhlon.jpg);"></div>
                                   <div class="news_item_text">
                                       <div>
                                           <h2>{{$item->title}}</h2>
                                            <p>{{$item->shortDescription}} </p>
                                       </div>

                                   </div>
                               </a>
                           </div>

                        @endforeach



                    </div>

                </div>
                <div class="col-lg-3 col-sm-12 col-12">
                    <div class="news_title">
                        <h2>
                            {{$languageDisplay->new_Special}}
                        </h2>
                    </div>
                    <div class="row">

                        @foreach ($dataOther as $item)

                        <div class="small_item col-lg-12 col-sm-6 col-12">
                            <a href="/tin-tuc/{{$item->slug}}">

                                   <div class="small_item_image" style="background-image: url({{Voyager::image($item->backgroundImage)}});"></div>
                                   <div class="small_item_text">
                                       <p>
                                           {{$item->title}}

                                       </p>
                                   </div>
                               </a>
                           </div>

                        @endforeach


                    </div>


                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-12">
                    <nav aria-label="..." class="news-pagination">
                        {{-- <ul class="pagination">

                          <li class="page-item active"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item"><a class="page-link" href="#">...</a></li>
                          <li class="page-item"><a class="page-link" href="#">7</a></li>
                          <li class="page-item"><a class="page-link" href="#">8</a></li>
                          <li class="page-item"><a class="page-link" href="#">9</a></li>

                        </ul> --}}

                        {!! $dataAll->links() !!}
                      </nav>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection





