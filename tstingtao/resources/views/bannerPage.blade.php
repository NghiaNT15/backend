@php

 $allBannerPage = Cache::get("allBanner");
 $currentBanner = null;
 $routerName =  Route::currentRouteName();
 foreach ($allBannerPage as $bannerItem) {
if($bannerItem->keyScreen == $routerName)
 {
     $currentBanner  = $bannerItem;
   break;
 }
}

if (Voyager::translatable($currentBanner)) {
   $currentBanner = $currentBanner->translate(app()->getLocale(), 'vi');

 }
@endphp

<section class="hero-section">

    <div class="hero-banner">
        <div class="hero-image">
            <img src="{{Voyager::image($currentBanner->image)}}" alt="">
            <div class="hero-breabcrumb fadeInUp">
                <h2>{{ $currentBanner->title}}</h2>
            </div>
        </div>
    <div class="hero-bottom"><span>{{$currentBanner->shortDescription}}</span></div>
    </div>
</section>
