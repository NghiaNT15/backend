<ul>
    @foreach($items as $menu_item)

    @php

    $isHaveChild = count($menu_item->children)>0?true:false;
    if (Voyager::translatable($menu_item)) {
           $menu_item = $menu_item->translate(app()->getLocale(), 'vi');
           }



    @endphp
    @if ($isHaveChild )
    <li class="menu-item menu-item-has-children">
        <a href="javascript:void(0)">{{ $menu_item->title }}</a>
        <ul class="sub-menu">
        @foreach ($menu_item->children as $itemchild)

        @php
        if (Voyager::translatable($itemchild)) {
           $itemchild = $itemchild->translate(app()->getLocale(), 'vi');
           }


        @endphp
        <li class="menu-item">

            <a href="{{ $itemchild->url }}">{{$itemchild->title}}</a>
        </li>

        @endforeach

        </ul>

    </li>
    @else

    @if ($loop->index ==0)
    <li class="menu-item active  ">
        <a href="{{ $menu_item->url }}">{{ $menu_item->title }}</a>
    </li>
    @else
    <li class="menu-item  ">
        <a href="{{ $menu_item->url }}">{{ $menu_item->title }}</a>
    </li>
    @endif

    @endif



@endforeach
<li class="menu-item menu-item-has-children language_nav">
    @php
        $urlvn = "/locale/vi";
        $urljp = "/locale/jp";
        $urlusa = "/locale/en_US";
    @endphp

    @if (App::isLocale('vi'))
    <a href="javascript:void()" class="flag-item">
        <img src="/images/flag-vi.png" alt="">
    </a>
    <ul class="sub-menu">
        <li class="menu-item">
            <a href="{{$urljp}}"><img src="/images/flag-jp.jpg" alt=""></a>
        </li>
        <li class="menu-item">
            <a href="{{$urlusa}}"><img src="/images/flag-usa.jpg" alt=""></a>
        </li>
    </ul>


    @endif

    @if (App::isLocale('en_US'))


    <a href="javascript:void()"  class="flag-item"><img src="/images/flag-usa.jpg" alt=""></a>
    <ul class="sub-menu">
        <li class="menu-item">
            <a href="{{$urljp}}"><img src="/images/flag-jp.jpg" alt=""></a>
        </li>
        <li class="menu-item">
            <a href="{{$urlvn}}" class="flag-item">
                <img src="/images/flag-vi.png" alt="">
            </a>


        </li>
    </ul>


    @endif
    @if (App::isLocale('jp'))
    <a href="javascript:void()"  class="flag-item"><img src="/images/flag-jp.jpg" alt=""></a>
    <ul class="sub-menu">
        <li class="menu-item">
            <a href="{{$urlusa}}"><img src="/images/flag-usa.jpg" alt=""></a>
        </li>
        <li class="menu-item">
            <a href="{{$urlvn}}" class="flag-item">
                <img src="/images/flag-vi.png" alt="">
            </a>
        </li>
    </ul>

    @endif
</li>
</ul>

