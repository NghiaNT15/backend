@extends('layout')
@php

@endphp

@section('css')
<link rel="stylesheet" type="text/css" href="styles/map.css">
@endsection

@php
    $allContactItemm = Cache::get('allContactItem', []);


@endphp

@section('content')

<div class="product">
    <div class="product-background" style="background-image: url(images/product-bg-1.jpg);"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-12">
                <div class="section_title" id="product_title">
                    <h2>CÁC SẢN PHẨM CỦA TSINGTAO</h2>
                </div>
            </div>
        </div>
        <div class="row relative">
            <div class="col-sm-12 col-12 relative">

                <ul class="product-container" id="product-list">

                    @foreach ($allProduct as $item)

                    <li class="box" id="product_box">
                        <a href="/san-pham/{{$item->slug}}">
                            <div class="content">
                            <img src="{{Voyager::image($item->backgroundImage)}}" alt="product item" />
                                <div class="product-title">
                                    {{$item->title}}
                                </div>
                            </div>
                        </a>
                    </li>

                    @endforeach
                </ul>
                <div class="product-prev customPreviousBtn"><i class="fas fa-caret-square-left"></i></div>
                <div class="product-counter counter"></div>
                <div class="product-next customNextBtn"><i class="fas fa-caret-square-right"></i></div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')


@endsection

