@php
    $bannerHomePage = Cache::get("bannerHomePage");
    $bannerIchiPage = Cache::get("bannerIchiPage");
    $bannerSnackePage  = Cache::get("bannerSnackePage");
    $bannerichikid = Cache::get("bannerichikid");
    $bannerichiCraker = Cache::get('bannerichiCraker');
    $allProductCategory  = Cache::get('allProductCategory');

    $developerPage = Cache::get('developerPage');
    $allDeveloperPage = Cache::get('allDeveloperPage');


    if (Voyager::translatable($bannerHomePage)) {
           $bannerHomePage = $bannerHomePage->translate(app()->getLocale(), 'vi');
    }

    if (Voyager::translatable($bannerIchiPage)) {
           $bannerIchiPage = $bannerIchiPage->translate(app()->getLocale(), 'vi');
    }


    if (Voyager::translatable($bannerichikid)) {
           $bannerichikid = $bannerichikid->translate(app()->getLocale(), 'vi');
    }


    if (Voyager::translatable($bannerichiCraker)) {
           $bannerichiCraker = $bannerichiCraker->translate(app()->getLocale(), 'vi');
    }



    if (Voyager::translatable($developerPage)) {
           $developerPage = $developerPage->translate(app()->getLocale(), 'vi');
    }


@endphp




@php
    $allBanner  = Cache::get('allBanner');
  $routerName = Route::currentRouteName();
    $currentBanner = null;
    if ($allBanner != null  ) {
    $currentBanner = $allBanner->first();
    }

    foreach ($allBanner as $seoItem) {
    if($seoItem->keyScreen == $routerName)
    {
    $currentBanner = $seoItem;
    break;
    }
    }

    if (Voyager::translatable($currentBanner)) {
        $currentBanner = $currentBanner->translate(app()->getLocale(), 'vi');
    }



@endphp


@extends('layout')

@section('content')

<!-- End / header -->
<div class="main_content">
    <div class="page-banner">
        <div class="video" style="background-image: url(images/video_placeholder.jpg);">
            <div class="video_click" data-video="video/video2.mp4" type="mp4"></div>
        </div>
    </div>
<!-- Develop -->
<div class="develop first-class">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10 col-sm-12 col-12 section_content mb-0">
                <div class="section_title text-center">
                <h2>{{$developerPage->title}}</h2>
                </div>
                <div class="section_text text-center">
                    {!!$developerPage->contentDescription!!}
                </div>

            </div>

        </div>
    </div>
</div>

@foreach ($allDeveloperPage as $item)

<div class="develop">

    <div class="container">
        @if ($loop->index%2 ==0)
        <div class="row flex-row-reverse">
        @else
        <div class="row">
        @endif

            <div class="col-lg-5 col-sm-12 col-12 section_content">
                <div class="section_title"><h2>{{$item->title}}</h2></div>
                <div class="section_text">
                    {!!$item->contentDescription!!}
                </div>

            </div>
            <div class="col-lg-7 col-sm-12 col-12">
                <div class="section_image" style="background-image: url({{Voyager::image($item->imageShare)}});">


                </div>
            </div>
        </div>
    </div>
</div>

@endforeach

@endsection
<!-- Develop -->







