@php



    $allProductCategory  = Cache::get('allProductCategory');
    $allProduct = Cache::get('allProduct', []);
    $introductionPage = Cache::get('introductionPage');
    $allNew = Cache::get('allNew');
    $allbanner  = Cache::get('allbannerItem', []);
    $languageDisplay  = Cache::get('languageDisplay');

    if (Voyager::translatable($introductionPage)) {
           $introductionPage = $introductionPage->translate(app()->getLocale(), 'vi');
    }



    if (Voyager::translatable($languageDisplay)) {
           $languageDisplay = $languageDisplay->translate(app()->getLocale(), 'vi');
    }

@endphp

@extends('layout')

@section('content')

<!-- Home -->
<div class="home">
    <div class="home_background" style="background-image: url(images/bg-section-1.jpg);">

    </div>
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-sm-12 col-12">
            <p class="t_text fadeInUp">{{$languageDisplay->Homeppage_discovery}}</p>
                <div class="home_slider_container">
                    <div class="home_title text-center fadeInUp" animation-delay="0.4">
                        <img src="images/tsingtao.png" alt="">
                    </div>
                    <!-- Home Slider -->
                    <div class="owl-carousel owl-theme home_slider">

                        @foreach ($allbanner  as $item)

                        @php
                            if (Voyager::translatable($item)) {
                            $item = $item->translate(app()->getLocale(), 'vi');
                            }
                        @endphp

                         <!-- Slider Item -->
                         <div class="item">
                            <div class="home_slider_background" style="background-image:url({{Voyager::image($item->image)}})"></div>
                            <div class="home_content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12 col-12 text-center">
                                            <div class="home_text"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </div>
                    <!-- Slider Progress -->
                    <div class="home_slider_progress"></div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- About us -->

<div class="about">

    <div class="container">
        <div class="row relative">
            <div class="text_overlay"><img src="images/text_overlay.png" alt=""></div>
            <div class="col-lg-5 col-sm-12 col-12 about_content">
                <div class="about_title"><h2>{{$introductionPage->titleHOmePage}}</h2></div>
                <div class="about_text">

                        {!!$introductionPage->descriptionContentHomePage!!}

                </div>
                <div class="about_button fadeInUp">
                    <div class="button">
                        <a href="/gioi-thieu">{{$languageDisplay->btnSeeDetail}}</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-sm-12 col-12 about_image">

                <img src="{{Voyager::image($introductionPage->imageHomePage)}}" alt="">

            </div>
        </div>
    </div>
</div>

<!-- Brands -->
<div class="brands">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="section_title">
                    <h2 class="fadeInDown">{{$languageDisplay->Homepage_productsingtaoTile}}</h2>
                    <p>
                        Tsingtao có nguồn gốc từ nghề bia thủ công với công thức lưu truyền, hương vị truyền thống hàng thế kỷ trước ở Đức. Trải qua hơn 100 năm phát triển, Tsingtao là thương hiệu bia nổi tiếng toàn cầu với các dòng bia Tsingtao như Lager, Stout, Pilsner và nhiều sản phẩm thức uống thương hiệu Tsingtao khác.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-12 relative">
                <div id="brands_slider">
                    @foreach ($allProduct as $item)

                        @php
                        if (Voyager::translatable($item)) {
                        $item = $item->translate(app()->getLocale(), 'vi');
                        }
                        @endphp


                     <a href="/san-pham/{{$item->slug}}"><img src="{{Voyager::image($item->backgroundImage)}}" alt=""><div></div></a>

                    @endforeach



                </div>
                <div class="brands_slider-nav">
                    <button id="prev">Prev</button>
                    <button id="next">Next</button>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Video -->
<div class="video">
    <div class="video_background" style="background-image: url(images/bg-section-2.jpg);"></div>

    <div class="container">
        <div class="row relative">

            <div class="col-sm-12 col-12 video_content">
                <h2 class="video_title fadeInDown">{{$languageDisplay->Homepage_videoSpecial}}</h2>
                <div class="video_thumnail">
                    <video id="player" playsinline controls data-poster="/images/about-1.jpg">
                        <source src="video/video2.mp4" type="video/mp4" />
                    </video>
                </div>
                <div class="button video_button fadeInUp">
                    <a href="comingsoon.html">{{$languageDisplay->btnSeeMore}}</a>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- News -->
<div class="news">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8 col-12">
                <div class="section_title">
                    <h2 class="fadeInDown">{{$languageDisplay->Homepage_newEvent}}</h2>
                </div>
            </div>
        </div>
        <div class="row">

            @foreach ($allNew as $item)
            @php
            if (Voyager::translatable($item)) {
            $item = $item->translate(app()->getLocale(), 'vi');
            }
            @endphp


            @if ($loop->first)
            <div class="col-lg-7 col-sm-12 col-12 news_content_left">
                <div class="large_news">
                    <a href="news-detail.html">
                        <div class="large_news_image" style="background-image: url(images/news/hinhlon.jpg);">
                        </div>
                        <div class="large_news_text">
                            <p>{{$item->title}}  </p>
                        </div>
                    </a>
                </div>
            </div>

            @else
            @break
            @endif

            @endforeach

            <div class="col-lg-5 col-sm-12 col-12 news_content_right">

                @foreach ($allNew as $item)

                @php
                if (Voyager::translatable($item)) {
                $item = $item->translate(app()->getLocale(), 'vi');
                }
                @endphp

                @if ($loop->first)
                    @continue
                                    @endif

                <div class="small_news">
                    <a href="news-detail.html">
                        <div class="small_news_image" style="background-image: url({{Voyager::image($item->backgroundImage)}});"></div>
                        <div class="small_news_text">
                            <p> {{$item->title}}                                </p>
                        </div>
                    </a>

                </div>
                @endforeach



            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-12 text-center">
                <div class="button news_button fadeInUp">
                    <a href="/tin-tuc">{{$languageDisplay->btnSeeMore}}</a>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

@endsection





