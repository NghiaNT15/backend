<?php

use Illuminate\Support\Facades\Route;




Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::get('/', 'HomePageController@index')->name('homePage');
Route::get('/contact', 'HomePageController@contact')->name('contact');
Route::get('/lien-he', 'HomePageController@contact')->name('contact');

Route::get('/locale/{locale}','HomePageController@changLanuage')->name('locale');
Route::get('/resetCache', 'HomePageController@resetCache')->name('homePage');
Route::get('/gioi-thieu', 'HomePageController@vechungtoi')->name('aboutus');
Route::get('/quy-trinh-san-xuat', 'HomePageController@quytrinhsanxuat')->name('processStep');
Route::get('/tuyen-dung', 'HomePageController@tuyendung')->name('career');
Route::get('/tuyen-dung/{slug}', 'HomePageController@careerdetails')->name('career');
Route::post('/api/tuyen-dung/applyjob', 'HomePageController@applyjob');
Route::get('/danh-muc-san-pham/{slug}', 'HomePageController@productCategory')->name('product');
Route::get('/chi-tiet-san-pham/{slug}', 'HomePageController@product')->name('product');
Route::get('/san-pham', 'HomePageController@productList')->name('product');

Route::get('/san-pham/{slug}', 'HomePageController@product')->name('product');

Route::get('/danh-sach-san-pham/{slug}', 'HomePageController@productList')->name('product');
Route::post('/api/contact/createRequest', 'HomePageController@createRequest')->name('createRequest');

Route::get('/tin-tuc', 'HomePageController@new')->name('news');

Route::get('/tin-tuc/{slug}', 'HomePageController@newDetail')->name('news');

Route::get('/phat-trien-ben-vung', 'HomePageController@phattrienbenvung')->name('developer');
Route::get('/tim-kiem', 'HomePageController@search')->name('keysearch');

