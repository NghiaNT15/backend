/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Set Header
3. Init Menu
4. Init Home Slider
5. Init SVG


******************************/

$(document).ready(function()
{
	"use strict";

	/*

	1. Vars and Inits

	*/

	var header = $('.header');
	var menuActive = false;



	var target = window.location.hash;
	target = target.replace('#', '');

	window.location.hash = "";

	$(window).on('load', function() {


		if (target) {
			if (typeof window.history.replaceState == 'function') {
				history.replaceState({}, '', window.location.href.slice(0, -1));
			}
			$('html, body').animate({
				scrollTop: $("#" + target).offset().top
			}, 700, 'swing', function () {});
		}



	});

	$(".age_button a").click(function(){
		var day = $("#day_input").val();
		var month = $("#month_input").val();
		var year = $("#year_input").val();
		var age = 18;
		var mydate = new Date();
		mydate.setFullYear(year, month-1, day);

		var currdate = new Date();
		currdate.setFullYear(currdate.getFullYear() - age);
		if(!day || !month || !year){
			alert("Xin điền đầy đủ thông tin!!!");
			return false;
		}
		else if( 31 < day || day < 1){
			alert("Ngày phải nằm trong khoảng từ 1 đến 31!!!");
			return false;
		}
		else if(12 < month || month < 1){
			alert("Tháng phải nằm trong khoảng từ 1 đến 12!!!");
			return false;
		}
		else if(2019 < year || year < 1800){
			alert("Năm phải nằm trong khoảng từ 1800 đến 2019!!!");
			return false;
		}
		else if ((currdate - mydate) < 0){
			alert("Xin lỗi, chỉ những người trên " + age + " mới có thể truy cập website này!!!");
			return false;
		}

		localStorage.setItem("isProcess", true);
		return true;
	});
	$(".age_input").keyup(function (e) {
		if (this.value.length == this.maxLength) {
		  $(this).parent().next().find('.age_input').focus();
		}
		if (e.which != 8 && e.which != 0 && e.which < 48 || e.which > 57) {
			this.value = this.value.replace(/\D/g, "");
		}
	});
	setHeader();
	$(window).on('resize', function()
	{
		setHeader();
	});

	$(document).on('scroll', function()
	{
		setHeader();
	});

	$(".sliding-link").click(function(e) {
		e.preventDefault();
		var id = $(this).attr("href");
		$('html,body').animate({scrollTop: $(id).offset().top}, 500);
	});

	/**
	 * Event click search-form
	 */
	$('.search-form').each(function(index, el) {
		$('.search-form__toggle').on('click', function(event) {
			event.preventDefault();
			$(el).toggleClass('open');
		});

		$(document).on('click', function(event) {
			var $content = $(el);
			if ($.contains(el, event.target)) {
				return;
			}

			if ($(el).hasClass('open')) {
				$(el).removeClass('open');
			}
		});
	});
	// /**
	// * Dropdown
	// */
	$('[data-init="dropdown"]').each(function(index, el) {

		$(el).find('a.dropdown__toggle').on('click', function(event) {
				event.preventDefault();
				$(el).find('.dropdown__content').toggleClass('open');
				$(el).toggleClass('open');
			});

		$(document).on('click', function(event) {
				var $content = $(el).find('.dropdown__content');
				if ($.contains(el, event.target)) {
					return;
				}

			if ($(el).hasClass('open')) {
				$(el).removeClass('open');
			}

			if ($content.hasClass('open')) {
				$content.removeClass('open');
			}
		});
	});
	/**
	 * Video Banner
	 */
	$('.video_click').click(function(){
		var video = '';
		if($(this).attr('type') == 'youtube'){

			video = '<iframe width="100%" height="100%" src="'+ $(this).attr('data-video') +'"frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
		}
		else{
			video = '<video id="player" autoplay playsinline controls><source src="'+$(this).attr('data-video')+'" type="video/mp4" /></video>';
		}
		$(this).replaceWith(video);
	});
	/**
	 * Event click navbar-toggle
	 */
	$('.navbar-toggle').each(function(index, el) {
		$(el).on('click', function(event) {
			event.preventDefault();
			$(el).toggleClass('open');
			$(".offcanvas-menu-wrapper").toggleClass("active");
			$(".offcanvas-menu-overlay").toggleClass("active");

		});

		$(document).on('click', function(event) {
			var $content = $(el);
			if ($.contains(el, event.target)) {
				return;
			}

			if ($(el).hasClass('open')) {
				$(el).removeClass('open');
			}
		});
	});
	$(".offcanvas-menu-overlay, .offcanvas__close").on('click', function () {

        $(".offcanvas-menu-wrapper").removeClass("active");
        $(".offcanvas-menu-overlay").removeClass("active");
	});

	/*------------------
		Navigation
	--------------------*/
    $(".main-menu").slicknav({
        prependTo: '#mobile-menu-wrap',
        allowParentLinks: true
	});

	menuHoverDropdown();
	initHomeSlider();

	const player = new Plyr('#player');

	var width = $(window).width();
	if(width > 992){
		initGsap();
		initBrandsSlider( 1,2, 255, 8000 , 500);
	}
	if(width > 576 && width <= 992){

		initBrandsSlider( 1,1, 175, 8000 , 500);
	}
	if(width <= 576){
		var carousel3 = $("#brands_slider").waterwheelCarousel({
			startingItem:               1,   // item to place in the center of the carousel. Set to 0 for auto
			flankingItems: 1,
			speed:                      500,      // speed in milliseconds it will take to rotate from one to the next
			autoPlay:                   8000,                 // indicate the speed in milliseconds to wait before autorotating. 0 to turn off. Can be negative

		});
		$('.brands_slider-nav #prev').bind('click', function () {
			carousel3.prev();
			return false
		});

		$('.brands_slider-nav #next').bind('click', function () {
			carousel3.next();
			return false;
		});
	}


	if(width <= 992){
		$('#product-list').addClass('owl-them owl-carousel');

		var owl1 = $('#product-list');
		owl1.owlCarousel({
			smartSpeed: 250,
			margin: 10,
			loop: true,
			nav: false,
			dots: false,
			onInitialized: counter, //When the plugin has initialized.
			onTranslated: counter, //When the translation of the stage has finished.
			responsive : {
				// breakpoint from 0 up
				0 : {
					items: 1,
				},

				// breakpoint from 768 up
				768 : {
					items: 3,
				}
			}

		});
		// Custom Button
		$('.product-next').click(function () {
			owl1.trigger('next.owl.carousel');
		});
		$('.product-prev').click(function () {
			owl1.trigger('prev.owl.carousel');
		});
		function counter(event) {
			var element = event.target;         // DOM element, in this example .owl-carousel
			var itemsCount = event.item.count;     // Number of items

			var item = event.item.index - parseInt(itemsCount / 2);     // Position of the current item
			// it loop is true then reset counter from 1

			if (item > itemsCount) {
				item -= itemsCount;
			}
			if (item <= 0) {
				item += itemsCount;
			}
			$('.product-counter').html("<span>" + item + "</span> / " + "<span class='length'>" + itemsCount + "</span>")
		}
	}

	var text = $(this).find("#product_title h2");

	$( "#product_box .content" ).mouseover(function() {
		var text2 = $(this).find(".product-title");
		$( "#product_title" ).html( "<h2>"+ text2.text() +"</h2>" );
	})
	.mouseleave(function() {
		$( "#product_title" ).html( "<h2>"+ text.text() +"</h2>" );
	});
	/*

	2. Set Header

	*/

	function setHeader()
	{

		$(window).scroll(function() {
			if ($(document).scrollTop() > 100) {
				$('.header').addClass('shrink');
			} else {
				$('.header').removeClass('shrink');
			}
		});
	}

	function menuHoverDropdown(){

		$('.has-dropdown').mouseenter(function(){
			var $this = $(this);
			$this
				.find('.dropdown')
				.css('display', 'block')
				.addClass('animated-fast fadeInUpMenu');

		}).mouseleave(function(){
			var $this = $(this);

			$this
				.find('.dropdown')
				.css('display', 'none')
				.removeClass('animated-fast fadeInUpMenu');
		});
	}


	/*

	2. Init Home Slider

	*/

	function initHomeSlider()
	{
		if($('.home_slider').length)
		{
			var homeSlider = $('.home_slider');
			homeSlider.owlCarousel(
			{
				items:1,
				loop:true,
				autoplay:true,
				autoplayTimeout:8500,
				nav:true,
				dots:true,
				// mouseDrag:false,
				animateOut: 'fadeOut',
    			animateIn: 'fadeIn',
				// autoHeight: true,
				navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
				smartSpeed:1200,
				onInitialized:startProgressBar,
				onTranslate:resetProgressBar,
				onTranslated:startProgressBar
			});
			function startProgressBar()
			{
				if($('.home_slider_progress').length)
				{
					$('.home_slider_progress').css({width:"100%", transition:"width 8500ms"});
				}
			}

			function resetProgressBar()
			{
				if($('.home_slider_progress').length)
				{
					$('.home_slider_progress').css({width:"0", transition:"width 0s"});
				}
			}

		}
	}

	/*

	3. Init Activities Slider

	*/

	function initBrandsSlider( startItem,items, separation, autoPlay , speed)
	{


		$("#brands_slider").waterwheelCarousel({
			startingItem:               startItem,   // item to place in the center of the carousel. Set to 0 for auto
			flankingItems: items,
			separation:                 separation, // distance between items in carousel
			opacityMultiplier:          1, // determines how drastically the opacity of each item changes
			speed:                      speed,      // speed in milliseconds it will take to rotate from one to the next
			edgeFadeEnabled:            true,    // when true, items fade off into nothingness when reaching the edge. false to have them move behind the center image
			preloadImages:              true,  // disable/enable the image preloader.
			autoPlay:                   autoPlay,                 // indicate the speed in milliseconds to wait before autorotating. 0 to turn off. Can be negative

		});

	}

	/*

	5. Init Gsap

	*/
	function initGsap() {
		gsap.registerPlugin(ScrollTrigger);

		//About
		$(".about_image img").each(function(i) {

			gsap.from($(this), 1.2,
			{
				scrollTrigger: {
					trigger: $(this),
					start: "top center",
					markers: false,
					toggleActions: "play"
				},
				x: 300,
				opacity: 0,
				ease:Power4.easeOut,
				delay: 0
			});

		});
		//News Section
		$(".small_news").each(function(i) {

			gsap.from($(this), 1.2,
			{
				scrollTrigger: {
					trigger: ".news",
					start: "top center",
					markers: false,
					toggleActions: "play"
				},
				x: 300,
				opacity: 0,
				ease:Power4.easeOut,
				delay: 0.2 + i/5
			});

		});

		gsap.from(".large_news",
		{
			scrollTrigger: {
				trigger: ".news",
				start: "top center",
				markers: false,
				toggleActions: "play"
			},
			duration:0.5,
			x: -300,
			opacity: 0,
			ease:Power4.easeOut,
			delay: 0.2
		});
		//Invest Section

		$(".fadeInDown").each(function(i) {
			var delay = $(this) .attr('animation-delay');
			var delayVal = (delay) ? delay : 0;
			gsap.fromTo($(this),
				{

					y: -50
				},
				{
					scrollTrigger: {
						trigger: $(this),
						start: "-120px center",
						markers: false,
						toggleActions: "play"
					},
					duration: 0.5, opacity: 1, y:0, delay: delayVal
				}
			)

		});
		$(".fadeInUp").each(function(i) {
			var delay = $(this) .attr('animation-delay');
			var delayVal = (delay) ? delay : 0;
			gsap.fromTo($(this),
				{

					y: 50
				},
				{
					scrollTrigger: {
						trigger: $(this),
						start: "-120px center",
						markers: false,
						toggleActions: "play"
					},
					duration: 0.5, opacity: 1, y:0, delay: delayVal
				}
			)

		});

	}
});









function sendRequest ()
{
    if(!validateFormContact())
    {
            return;
    }
    var url = "/api/contact/createRequest";
    $.ajax({
       type: "POST",
       url: url,
       data: $("#contactForm").serialize(), // serializes the form's elements.
       success: function(data)
       {

         $(".hideFormContact").hide();
         $(".resultRequest").show();

       }
     });

}

function sendApplyForm() {


    if(!validateForm())
    {
            return;
    }

    var formData = new FormData();
    formData.append('fullName',  $("#txtName").val());
    formData.append('id',  $("#txtcode").val());
    formData.append('phoneNumber', $("#txtPhone").val());
     formData.append('email', $("#txtEmail").val());
     formData.append('attachfile',$("#upload")[0].files[0]);

    var url = "/api/tuyen-dung/applyjob";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }

       });


       $.ajax({
       type: "POST",
       contentType: false,
       processData: false,
       data        : formData,
       url: url,
       success: function(data)
       {
        //  alert("success");
         $(".hideFormContact").hide();
         $("#lableSucess").show();

       }
     });
 }

 function validateFormContact()
 {



    var txtName = document.getElementById("txtName");
    var txtPhone = document.getElementById("txtPhone");
    var txtEmail = document.getElementById("txtEmail");
    var txtMsg =  document.getElementById("txtMessage");
    var index = 0;
    if (checkIsEmpty(txtName)) {

        addError(txtName, resources.contactPage.errorRequire.fullName);
        index++
    } else {
        removeError(txtName)
    }

    if (checkIsEmpty(txtPhone)) {
        addError(txtPhone,  resources.contactPage.errorRequire.phoneNumber);
        index++
    } else {
        removeError(txtPhone)
    }
    if (checkIsEmpty(txtEmail)) {
        addError(txtEmail,  resources.contactPage.errorRequire.email);
        index++
    } else {
        removeError(txtEmail)
    }


    if (checkIsEmpty(txtMsg)) {
        addError(txtMsg,  resources.contactPage.errorRequire.yourMessage);
        index++
    } else {
        removeError(txtMsg)
    }

    // if (checkIsEmpty(txtMsg)) {
    //     addError(txtMsg,resources.contactPage.errorRequire.yourMessage);
    //     index++
    // } else {
    //     removeError(txtMsg)
    // }

    if (index > 0) {
        return false;
    }

    return true;

 }




function isNumberKey(evt) {
    var theEvent = evt || window.event;
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain')
    } else {
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key)
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = !1;
        if (theEvent.preventDefault) theEvent.preventDefault()
    }
}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email)
}

function resetContact() {
    showOrHideResultAflterSubmit(!1);

}

function addError(objectTextBox, messageError) {

    var parrentDiv = objectTextBox.parentNode;
    if (!parrentDiv)
        return;
    if(parrentDiv.childElementCount>1)
    {
        objectTextBox.parentNode.removeChild(objectTextBox.parentElement.lastElementChild);
    }
    parrentDiv.innerHTML += '<div class="invalid-feedback d-block"> ' + messageError +' </div>';
}

function removeError(objectTextBox) {
    var parrentDiv = objectTextBox.parentNode;
    if (!parrentDiv)
        return;
    if(parrentDiv.childElementCount>1)
    {
        objectTextBox.parentNode.removeChild(objectTextBox.parentElement.lastElementChild);
    }
    else
    {
        return;
    }
}

function showOrHideResultAflterSubmit(isShow) {

}

function showOrHideDataSubmit(isShow) {
    var display = document.getElementById("submitData");
    if (isShow) {
        display.style.display = "block"
    } else {
        display.style.display = "none"
    }
}

function checkIsEmpty(textBox) {
    var textBoxValue = textBox.value;
    if (!textBoxValue) {
        return !0
    }
    return textBoxValue.length < 1
}


function validateForm() {


    var txtName = document.getElementById("txtName");
    var txtPhone = document.getElementById("txtPhone");
    var txtEmail = document.getElementById("txtEmail");




    var index = 0;
    if (checkIsEmpty(txtName)) {

        addError(txtName, resources.contactPage.errorRequire.fullName);
        index++
    } else {
        removeError(txtName)
    }

    if (checkIsEmpty(txtPhone)) {
        addError(txtPhone,  resources.contactPage.errorRequire.phoneNumber);
        index++
    }
    // else  if ( checkValidPhoneNumber(txtPhone.value) ==false)
    // {
    //     addError(txtPhone,  "Số điện thoại không hợp lệ");
    //     index++
    // }

    else {
        removeError(txtPhone)
    }
    if (checkIsEmpty(txtEmail)) {

    }
    else

    {
        if ( validateEmail(txtEmail.value) ==false)
        {
            addError(txtEmail,  "Email không hợp lệ");
            index++
        }
        else
        {
            removeError(txtEmail)
        }

    }



    if( document.getElementById("upload").files.length == 0 ){

        $("#resultfile").val(resources.tuyendungPage.errorRequire.fileNotSelect);
        // addError(document.getElementById("upload"),  resources.tuyendungPage.errorRequire.fileNotSelect);
        index++;
    }
    else
    {


    }


    if (index > 0) {
        return false;
    }

    return true;

}

function openUloadFile() {
    $('#uploadfileId').trigger('click');
}

function prepareLoad(file)
{
    var fileneedUpload =  file.files[0];
     if(fileneedUpload)
     {
         document.getElementById("resultfile").innerHTML  = fileneedUpload.name;
         document.getElementById("resultfile").style.display = block;

     }


}
