<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class LanguageDisplay extends Model
{
    use Translatable;
    protected $translatable = ['Homepage_productsingtaoTile','Homepage_videoSpecial','Homepage_newEvent','btnSeeMore','btnSeeDetail','carrer_PositionName','carrer_Quatinity','carrer_WorkLocation','carrer_ApplicationDealine','new_lasted','new_Special','new_backtoNew','new_other','contactinfo'];
}
