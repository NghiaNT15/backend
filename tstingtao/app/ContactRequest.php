<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class ContactRequest extends Model
{
    public static function boot()
    {
        parent::boot();

        static::updating(function($model)
        {
            $model->isRead = 0;

        });
    }
}
