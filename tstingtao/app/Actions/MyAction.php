<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class MyAction  extends AbstractAction
{
    public function getTitle()
    {
        return 'View';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'view';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('my.route');
    }
    public function shouldActionDisplayOnDataType()
{
    return $this->dataType->slug == 'contact-requests';
}
}
