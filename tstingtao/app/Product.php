<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Product extends Model
{
    use Translatable;
    protected $translatable = ['title','slug','description','shortDescriptionContent','shortDescription','weight','slug','content','shortDescriptionTitle','titleSeo','descriptionSEO'];

}
