<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
class ProductCategory extends Model
{

    use Translatable;
    protected $translatable = ['title','titleDetail','titleHomePage','descriptionHomePage','descriptionSEO','titleSeo','titleDisplay','shortDescription','weight'];
    public function ListProduct()
    {
        return $this->hasMany('App\Product', 'categoryId', 'id')->orderBy('priority','asc');
    }


}
