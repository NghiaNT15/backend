<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
class ContactInfo extends Model
{
    use Translatable;

    protected $translatable = ['Address','fullName'];

}
