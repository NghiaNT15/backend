<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
class AboutusPage extends Model
{
    use Translatable;
    protected $translatable = ['title','content','QuatinityText','footerText'];

}
