<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class HomePageIchi extends Model
{
    use Translatable;
    protected $translatable = ['title', 'shortDescription'];
    public function ProductCategory()
    {


        return $this->hasMany('App\ProductCategory', 'id', 'categoryid');
    }
}
