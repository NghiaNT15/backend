<?php

namespace App\Http\Controllers;

use App\BannerItem;
use Illuminate\Http\Request;

class BannerItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BannerItem  $bannerItem
     * @return \Illuminate\Http\Response
     */
    public function show(BannerItem $bannerItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BannerItem  $bannerItem
     * @return \Illuminate\Http\Response
     */
    public function edit(BannerItem $bannerItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BannerItem  $bannerItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BannerItem $bannerItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BannerItem  $bannerItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(BannerItem $bannerItem)
    {
        //
    }
}
