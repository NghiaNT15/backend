<?php

namespace App\Http\Controllers;

use App\QualityLicenece;
use Illuminate\Http\Request;

class QualityLiceneceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QualityLicenece  $qualityLicenece
     * @return \Illuminate\Http\Response
     */
    public function show(QualityLicenece $qualityLicenece)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QualityLicenece  $qualityLicenece
     * @return \Illuminate\Http\Response
     */
    public function edit(QualityLicenece $qualityLicenece)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QualityLicenece  $qualityLicenece
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QualityLicenece $qualityLicenece)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QualityLicenece  $qualityLicenece
     * @return \Illuminate\Http\Response
     */
    public function destroy(QualityLicenece $qualityLicenece)
    {
        //
    }
}
