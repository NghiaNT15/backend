<?php

namespace App\Http\Controllers;

use App\AboutusPage;
use Illuminate\Http\Request;

class AboutusPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AboutusPage  $aboutusPage
     * @return \Illuminate\Http\Response
     */
    public function show(AboutusPage $aboutusPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AboutusPage  $aboutusPage
     * @return \Illuminate\Http\Response
     */
    public function edit(AboutusPage $aboutusPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AboutusPage  $aboutusPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AboutusPage $aboutusPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AboutusPage  $aboutusPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(AboutusPage $aboutusPage)
    {
        //
    }
}
