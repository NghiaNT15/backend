<?php

namespace App\Http\Controllers;

use App\NumberImpress;
use Illuminate\Http\Request;

class NumberImpressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NumberImpress  $numberImpress
     * @return \Illuminate\Http\Response
     */
    public function show(NumberImpress $numberImpress)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NumberImpress  $numberImpress
     * @return \Illuminate\Http\Response
     */
    public function edit(NumberImpress $numberImpress)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NumberImpress  $numberImpress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NumberImpress $numberImpress)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NumberImpress  $numberImpress
     * @return \Illuminate\Http\Response
     */
    public function destroy(NumberImpress $numberImpress)
    {
        //
    }
}
