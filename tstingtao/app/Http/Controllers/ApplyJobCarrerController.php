<?php

namespace App\Http\Controllers;

use App\ApplyJobCarrer;
use Illuminate\Http\Request;

class ApplyJobCarrerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ApplyJobCarrer  $applyJobCarrer
     * @return \Illuminate\Http\Response
     */
    public function show(ApplyJobCarrer $applyJobCarrer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ApplyJobCarrer  $applyJobCarrer
     * @return \Illuminate\Http\Response
     */
    public function edit(ApplyJobCarrer $applyJobCarrer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ApplyJobCarrer  $applyJobCarrer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApplyJobCarrer $applyJobCarrer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ApplyJobCarrer  $applyJobCarrer
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplyJobCarrer $applyJobCarrer)
    {
        //
    }
}
