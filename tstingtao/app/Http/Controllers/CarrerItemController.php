<?php

namespace App\Http\Controllers;

use App\CarrerItem;
use Illuminate\Http\Request;

class CarrerItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarrerItem  $carrerItem
     * @return \Illuminate\Http\Response
     */
    public function show(CarrerItem $carrerItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarrerItem  $carrerItem
     * @return \Illuminate\Http\Response
     */
    public function edit(CarrerItem $carrerItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarrerItem  $carrerItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarrerItem $carrerItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarrerItem  $carrerItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarrerItem $carrerItem)
    {
        //
    }
}
