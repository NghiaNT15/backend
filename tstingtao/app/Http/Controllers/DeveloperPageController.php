<?php

namespace App\Http\Controllers;

use App\DeveloperPage;
use Illuminate\Http\Request;

class DeveloperPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeveloperPage  $developerPage
     * @return \Illuminate\Http\Response
     */
    public function show(DeveloperPage $developerPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeveloperPage  $developerPage
     * @return \Illuminate\Http\Response
     */
    public function edit(DeveloperPage $developerPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeveloperPage  $developerPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeveloperPage $developerPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeveloperPage  $developerPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeveloperPage $developerPage)
    {
        //
    }
}
