<?php

namespace App\Http\Controllers;

use App\HomePageIchi;
use Illuminate\Http\Request;

class HomePageIchiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HomePageIchi  $homePageIchi
     * @return \Illuminate\Http\Response
     */
    public function show(HomePageIchi $homePageIchi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HomePageIchi  $homePageIchi
     * @return \Illuminate\Http\Response
     */
    public function edit(HomePageIchi $homePageIchi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HomePageIchi  $homePageIchi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HomePageIchi $homePageIchi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HomePageIchi  $homePageIchi
     * @return \Illuminate\Http\Response
     */
    public function destroy(HomePageIchi $homePageIchi)
    {
        //
    }
}
