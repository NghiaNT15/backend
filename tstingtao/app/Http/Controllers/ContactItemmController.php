<?php

namespace App\Http\Controllers;

use App\ContactItemm;
use Illuminate\Http\Request;

class ContactItemmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactItemm  $contactItemm
     * @return \Illuminate\Http\Response
     */
    public function show(ContactItemm $contactItemm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactItemm  $contactItemm
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactItemm $contactItemm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactItemm  $contactItemm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactItemm $contactItemm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactItemm  $contactItemm
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactItemm $contactItemm)
    {
        //
    }
}
