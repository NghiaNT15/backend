<?php

namespace App\Http\Controllers;

use App\StepItem;
use Illuminate\Http\Request;

class StepItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StepItem  $stepItem
     * @return \Illuminate\Http\Response
     */
    public function show(StepItem $stepItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StepItem  $stepItem
     * @return \Illuminate\Http\Response
     */
    public function edit(StepItem $stepItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StepItem  $stepItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StepItem $stepItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StepItem  $stepItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(StepItem $stepItem)
    {
        //
    }
}
