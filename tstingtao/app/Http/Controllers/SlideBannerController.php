<?php

namespace App\Http\Controllers;

use App\SlideBanner;
use Illuminate\Http\Request;

class SlideBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SlideBanner  $slideBanner
     * @return \Illuminate\Http\Response
     */
    public function show(SlideBanner $slideBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SlideBanner  $slideBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(SlideBanner $slideBanner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SlideBanner  $slideBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SlideBanner $slideBanner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SlideBanner  $slideBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(SlideBanner $slideBanner)
    {
        //
    }
}
