<?php

namespace App\Http\Controllers;

use App;
use Lang;
use Session;
use App\SeoPage;
use App\BannerPage;
use App\CarrerItem;
use App\AboutusPage;
use App\ApplyJobCarrer;
use App\ProjectItem;
use App\SlideBanner;
use App\HomePageIchi;
use App\Introduction;
use App\NumberImpress;
use App\TechologyItem;
use App\BannerHomePage;
use App\BannerItem;
use App\CarrerObjectItem;
use App\CarrerObjectPage;
use App\ContactInfo;
use App\ContactItemm;
use App\ContactRequest;
use App\DeveloperItem;
use App\DeveloperPage;
use App\FooterInfo;
use App\HomePageKidIchi;
use App\QualityLicenece;
use App\HomePageSnackIchi;
use App\InfomationCompany;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;
use App\HomePageKidIchiCranker;
use App\IntroductionObjectPage;
use App\LanguageDisplay;
use App\News;
use App\Product;
use App\ProductCategory;
use App\ProductCategoryImage;
use App\StepItem;
use Illuminate\Support\Facades\Cache;
use Larapack\Hooks\Commands\InfoCommand;
use stdClass;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->createCache();
        return view('welcome');
    }


    public function vechungtoi ()
    {
        return view("vechungtoi");
    }
    public function resetCache()
    {
        Cache::flush();
        $this->createCache();

    }



    public function tuyendung ()
    {

        $allData = CarrerObjectItem::orderby("priority","asc")->get();

        return view("career",compact("allData"));
    }

    public function productList(Request $request, $slug =null)
    {

        // $allCategory = ProductCategory::all();

        $allProduct = Product::all();
        return view("product",compact("allProduct"));
    }



    public function productCategory(Request $request,$slug)
    {
        $data = ProductCategory::whereTranslation('slug', $slug)->first();
        $allProduct = $data->ListProduct;

        $count = count($allProduct);
        $allProductCategoryImage = ProductCategoryImage::where("categoryId",$data->id)->get();

        $allProduct = Product::where("categoryId",$data->id)->get();

        if($count>0)
        {
            return view("chitetdanhmuc",compact("data","allProductCategoryImage","allProduct"));
        }
        else
        {

            return view("chitetdanhmuc2",compact("data","allProductCategoryImage","allProduct"));
        }

    }

    public function product (Request $request,$slug)
    {

        $data = Product::whereTranslation("slug",$slug)->first();
        if($data)
        {

            return view("chitietsanpham",compact("data"));
        }
        return redirect("/");


    }

    public function applyjob(Request $request)
    {
        $itemInsert=  new ApplyJobCarrer();
        $itemInsert->fullName =$request->input("fullName");
        $itemInsert->phoneNumber =$request->input("phoneNumber");
        $itemInsert->email =$request->input("email");
        $itemInsert->ref =$request->input("id");
        $itemInsert->isRead ="0";
        if($request->hasFile('attachfile')) {
            $file = $request->file('attachfile');
            $name = $file->getClientOriginalName();
            $file->move( storage_path('app/public').'/attachFile/', $name);
                $data = [
                    'download_link'=>'\\'.'attachFile'.'\\'. $name,
                      'original_name'=>$name];

            $itemInsert->linkFile= json_encode($data);
            $itemInsert->linkFile = '['.$itemInsert->linkFile.']';
         }
        $itemInsert->save();
        return ["success"=>true];
    }



    public function careerdetails ($slug)
   {

        $data = CarrerObjectItem::whereTranslation("slug",$slug)->first();
        if($data ==null)
        {
            $data = new CarrerObjectItem();
        }

       return view("chitiettuyendung",compact("data"));
   }


   public function phattrienbenvung()
   {

       return view("phattrienbenvung");
   }

    public function quytrinhsanxuat()
    {

        return view("quytrinhsanxuat");
    }
    public function createCache()
    {
        $allProductCategory = ProductCategory::orderby("priority")->get();

        Cache::forever('allProductCategory', $allProductCategory);

        $bannerHomePage =BannerHomePage::first();
        Cache::forever('bannerHomePage', $bannerHomePage);

        $introductionPage =IntroductionObjectPage::first();
        Cache::forever('introductionPage', $introductionPage);

        $developerPage =DeveloperPage::first();

        Cache::forever('developerPage', $developerPage);
        $languageDisplay =LanguageDisplay::first();

        Cache::forever('languageDisplay', $languageDisplay);
        $contactInfo =ContactInfo::first();

        Cache::forever('contactInfo', $contactInfo);

        $carrerObjectPage =CarrerObjectPage::first();

        $allProduct = Product::all();
        $allBanner = BannerItem::all();
        Cache::forever('allBanner', $allBanner);
        $allNew = News::take(5)->get();
        Cache::forever('allNew', $allNew);

        Cache::forever('allProduct', $allProduct);

        Cache::forever('carrerObjectPage', $carrerObjectPage);
        $allDeveloperPage = DeveloperItem::all();
        Cache::forever('allDeveloperPage', $allDeveloperPage);

        $companyInfo =InfomationCompany::first();
        Cache::forever('companyInfo', $companyInfo);
        $bannerIchiPage = HomePageIchi::first();
        Cache::forever('bannerIchiPage', $bannerIchiPage);
        $bannerSnackePage = HomePageSnackIchi::first();
        Cache::forever('bannerSnackePage', $bannerSnackePage);
        $allbannerItem = SlideBanner::all();
        Cache::forever('allbannerItem', $allbannerItem);

        $bannerichikid = HomePageKidIchi::first();
        Cache::forever('bannerichikid', $bannerichikid);
        $aboutUspage = AboutusPage::first();
        Cache::forever('aboutUspage', $aboutUspage);
        $allQuatinityLience = QualityLicenece::all();
        Cache::forever('allQuatinityLience', $allQuatinityLience);
        $allNumberImpress = NumberImpress::orderby("priorites")->get();
        Cache::forever('allNumberImpress', $allNumberImpress);
        $allStepItem = StepItem::orderby("step")->get();
        Cache::forever('allStepItem', $allStepItem);
        $allObjectCarrerItem = CarrerObjectItem::orderby("priority")->get();
        Cache::forever('allObjectCarrerItem', $allObjectCarrerItem);
        $allContactItem = ContactItemm::all();
        Cache::forever('allContactItem', $allContactItem );

        $SeoPage = SeoPage::all();
        Cache::forever('SeoPage', $SeoPage);

        $footerInfo = FooterInfo::first();
        Cache::forever('footerInfo', $footerInfo);


    }







    public function contact()
    {
        return view('contact');
    }

    public function career()
    {
        return view('career');
    }

    public function changLanuage($locale)
	{


		if($locale =="vi"  || $locale =="en")
		{
            \Session::put('website_language', $locale);
        }
        $this->createCache();

        return back();

    }



    public function createRequest(Request $request)
    {
        $itemInsert =new ContactRequest();
        $itemInsert->fullName =$request->input("fullName");
        $itemInsert->email =$request->input("email");
        $itemInsert->content =$request->input("content");
        $itemInsert->phone =$request->input("phoneNumber");
         $itemInsert->isRead ="0";
        $itemInsert->save();
        return ["success"=>true];
    }



//     public function new()
//     {
//         return view('news');
//     }



//     public function newDetail ($slug)
//    {



//        return view("chitiettintuc",compact("data"));
//    }


   public function new()
   {


       $dataSpecial =News::where("isActive",1)->orderby("isSpecial","desc")->orderby("priorites","desc")->orderby("created_at","desc")->first();

       $dataAll =News::where("isActive",1)
       ->whereNotIn('id', [$dataSpecial->id])
       ->paginate(8);


       $dataOther =News::where("isActive",1)->where("isSpecial",1)
       ->whereNotIn('id', [$dataSpecial->id])
       ->orderby("priorites","desc")->orderby("created_at","desc")->take(5)->get();

       return view("news",compact('dataAll','dataSpecial','dataOther'));
   }

   public function newDetail(Request $request,$slug)
   {
       $data = News::whereTranslation("slug",$slug)->where("isActive",1)->First();
       if($data)
       {

       }
       else
       {
           return redirect('/tin-tuc');
       }

       $dataOther =News::where("isActive",1)
       ->whereNotIn('id', [$data->id])
       ->orderby("priorites","desc")->orderby("created_at","desc")->take(4)->get();
       return view("chitiettintuc",compact('data','dataOther'));
   }


   public function search (Request $request)
   {



    $keysearch = $request->input("keysearch");

    $dataAll = null;

    if($keysearch!="")
    {
        $dataAll =News::where('title', 'like', '%'.$keysearch.'%')
        ->orderby("priorites","desc")->orderby("created_at","desc")
        ->paginate(8);

    }
    else
    {
        $dataAll = News::orderby("priorites","desc")->orderby("created_at","desc")
        ->paginate(8);
    }

    $dataSpecial =News::where("isActive",1)->orderby("isSpecial","desc")->orderby("priorites","desc")->orderby("created_at","desc")->first();



    $dataOther =News::where("isActive",1)->where("isSpecial",1)
    ->orderby("priorites","desc")->orderby("created_at","desc")->take(5)->get();

    return view("news",compact('dataAll','dataSpecial','dataOther'));

   }

}
