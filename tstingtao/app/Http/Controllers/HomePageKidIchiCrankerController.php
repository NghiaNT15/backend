<?php

namespace App\Http\Controllers;

use App\HomePageKidIchiCranker;
use Illuminate\Http\Request;

class HomePageKidIchiCrankerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HomePageKidIchiCranker  $homePageKidIchiCranker
     * @return \Illuminate\Http\Response
     */
    public function show(HomePageKidIchiCranker $homePageKidIchiCranker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HomePageKidIchiCranker  $homePageKidIchiCranker
     * @return \Illuminate\Http\Response
     */
    public function edit(HomePageKidIchiCranker $homePageKidIchiCranker)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HomePageKidIchiCranker  $homePageKidIchiCranker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HomePageKidIchiCranker $homePageKidIchiCranker)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HomePageKidIchiCranker  $homePageKidIchiCranker
     * @return \Illuminate\Http\Response
     */
    public function destroy(HomePageKidIchiCranker $homePageKidIchiCranker)
    {
        //
    }
}
