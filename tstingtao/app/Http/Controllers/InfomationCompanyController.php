<?php

namespace App\Http\Controllers;

use App\InfomationCompany;
use Illuminate\Http\Request;

class InfomationCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InfomationCompany  $infomationCompany
     * @return \Illuminate\Http\Response
     */
    public function show(InfomationCompany $infomationCompany)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InfomationCompany  $infomationCompany
     * @return \Illuminate\Http\Response
     */
    public function edit(InfomationCompany $infomationCompany)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InfomationCompany  $infomationCompany
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InfomationCompany $infomationCompany)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InfomationCompany  $infomationCompany
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfomationCompany $infomationCompany)
    {
        //
    }
}
