<?php

namespace App\Http\Controllers;

use App\DeveloperItem;
use Illuminate\Http\Request;

class DeveloperItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeveloperItem  $developerItem
     * @return \Illuminate\Http\Response
     */
    public function show(DeveloperItem $developerItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeveloperItem  $developerItem
     * @return \Illuminate\Http\Response
     */
    public function edit(DeveloperItem $developerItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeveloperItem  $developerItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeveloperItem $developerItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeveloperItem  $developerItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeveloperItem $developerItem)
    {
        //
    }
}
