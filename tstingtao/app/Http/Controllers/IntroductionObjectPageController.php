<?php

namespace App\Http\Controllers;

use App\IntroductionObjectPage;
use Illuminate\Http\Request;

class IntroductionObjectPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IntroductionObjectPage  $introductionObjectPage
     * @return \Illuminate\Http\Response
     */
    public function show(IntroductionObjectPage $introductionObjectPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IntroductionObjectPage  $introductionObjectPage
     * @return \Illuminate\Http\Response
     */
    public function edit(IntroductionObjectPage $introductionObjectPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IntroductionObjectPage  $introductionObjectPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IntroductionObjectPage $introductionObjectPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IntroductionObjectPage  $introductionObjectPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(IntroductionObjectPage $introductionObjectPage)
    {
        //
    }
}
