<?php

namespace App\Http\Controllers;

use App\CarrerObjectPage;
use Illuminate\Http\Request;

class CarrerObjectPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarrerObjectPage  $carrerObjectPage
     * @return \Illuminate\Http\Response
     */
    public function show(CarrerObjectPage $carrerObjectPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarrerObjectPage  $carrerObjectPage
     * @return \Illuminate\Http\Response
     */
    public function edit(CarrerObjectPage $carrerObjectPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarrerObjectPage  $carrerObjectPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarrerObjectPage $carrerObjectPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarrerObjectPage  $carrerObjectPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarrerObjectPage $carrerObjectPage)
    {
        //
    }
}
