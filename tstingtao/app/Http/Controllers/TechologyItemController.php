<?php

namespace App\Http\Controllers;

use App\TechologyItem;
use Illuminate\Http\Request;

class TechologyItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TechologyItem  $techologyItem
     * @return \Illuminate\Http\Response
     */
    public function show(TechologyItem $techologyItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TechologyItem  $techologyItem
     * @return \Illuminate\Http\Response
     */
    public function edit(TechologyItem $techologyItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TechologyItem  $techologyItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TechologyItem $techologyItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TechologyItem  $techologyItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(TechologyItem $techologyItem)
    {
        //
    }
}
