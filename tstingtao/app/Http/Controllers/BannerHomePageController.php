<?php

namespace App\Http\Controllers;

use App\BannerHomePage;
use Illuminate\Http\Request;

class BannerHomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BannerHomePage  $bannerHomePage
     * @return \Illuminate\Http\Response
     */
    public function show(BannerHomePage $bannerHomePage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BannerHomePage  $bannerHomePage
     * @return \Illuminate\Http\Response
     */
    public function edit(BannerHomePage $bannerHomePage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BannerHomePage  $bannerHomePage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BannerHomePage $bannerHomePage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BannerHomePage  $bannerHomePage
     * @return \Illuminate\Http\Response
     */
    public function destroy(BannerHomePage $bannerHomePage)
    {
        //
    }
}
