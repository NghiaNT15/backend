<?php

namespace App\Http\Controllers;

use App\SeoPage;
use Illuminate\Http\Request;

class SeoPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SeoPage  $seoPage
     * @return \Illuminate\Http\Response
     */
    public function show(SeoPage $seoPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SeoPage  $seoPage
     * @return \Illuminate\Http\Response
     */
    public function edit(SeoPage $seoPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SeoPage  $seoPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SeoPage $seoPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SeoPage  $seoPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(SeoPage $seoPage)
    {
        //
    }
}
