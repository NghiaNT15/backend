<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class BannerPage extends Model
{
    use Translatable;
    protected $translatable = ['title', 'shortDescription'];
}
