<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
class ProductCategoryImage extends Model
{
    use Translatable;
    protected $translatable = ['weight'];
}
